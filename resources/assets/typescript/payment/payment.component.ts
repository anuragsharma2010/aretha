import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { CheckoutComponent } from "../checkout/checkout.component";
import { PageService } from "../service/page.service";
import { CartService } from "../service/cart.service";
import { Router } from '@angular/router';
import { blockUI, unblockUI, toast } from '../global';
declare var jQuery: any;

@Component({
    selector: 'payment',
    template: require('./payment.html')
})

export class PaymentComponent {
    type: string = 'PAYU';
    settings: any = [];
    carts: any = [];
    cart_length: any = [];
    paytm: any = [];
    payu: any = [];
    PayUmoney: any = [];
    order: any = [];
    authData: any = [];

    wallet: any = {};

    enable_cod: number = 0;
    enable_paytm: number = 0;
    enable_payu: number = 0;
    is_wallet: number = 0;
    enable_wallet: number = 0;
    phone_number: string = '';
       number_model: any = {};
    loading = false;
    error = '';
    success = '';
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private checkoutComponent: CheckoutComponent,
        private pageService: PageService,
        private cartService: CartService,
        private router: Router,
    ) { }

    ngOnInit() {
        if (Object.keys(this.cartService.orderData).length > 0) {
            this.authData = JSON.parse(localStorage.getItem('loginUser'));





            this.checkwallet();
            

            this.pageService.getSettings().subscribe(
                settings => {
                    this.settings = settings;
                    this.enable_payu = Number(this.settings['CONFIG_ENABLE_PAYU']);
                    this.enable_paytm = Number(this.settings['CONFIG_ENABLE_PAYTM']);
                    this.enable_cod = Number(this.settings['CONFIG_ENABLE_COD']);
                    this.carts = this.checkoutComponent.order_data
                    this.cart_length = this.checkoutComponent.order_data['products'].length;
                    this.userService.getPincodeAvalability(this.checkoutComponent.order_data['address']['pin_code'])
                    .subscribe(result => {
                        if (result['status_code'] === 1) {
                             if(result['cash']=='y'){
                                this.enable_cod= 1;

                             }else{
                                this.enable_cod= 0;
                             }
                        }else{
                                this.enable_cod= 0;

                        } 


                        setTimeout(() => {
                            
                            jQuery('.collapsible').collapsible();
                        }, 1000);
                    });



                }, err => {
                    console.log(err);
                }
            );


          








        } else {

            this.router.navigate(['/']);
        }
    }
    setPaymentType(type) {

        this.type = type;

        this.checkoutComponent.type = type;
        if (type == 'COD') {

            this.enable_wallet = 0;
            this.is_wallet = 0;
        } else {
            if (this.wallet['enable_wallet'] == 1) {
                this.enable_wallet = 1;
            }

        }


    }
    paymentConfirm() {
        blockUI();

        this.checkoutComponent.order_data['type'] = this.type;
        this.checkoutComponent.order_data['user_id'] = this.authData.id;
        this.checkoutComponent.order_data['token'] = this.authData.token;
        
        if (this.is_wallet) {
            this.wallet['is_wallet'] = 1;
            this.checkoutComponent.order_data['wallet'] = this.wallet;

            if (this.wallet['after_wallet_pay'] == 0) {
                this.checkoutComponent.order_data['type'] = 'wallet';
                this.type = 'wallet';

            }

        }
        
        if(this.type=='COD'){
               
              this.pageService.checkMobileNumberVerified(this.authData.id).subscribe(
                result => {
                      if (result['status_code'] == 1) {
                            if(result['phone_verify']==1){
                                this.submitPayemntData();
                                
                            }else{
                            if(result['send_otp']==0){
                                 this.submitPayemntData();
                                
                            }else{
                             
                                this.phone_number =  result['mobile'];
                             
                                jQuery('#modal_otp_cod').modal();
                                jQuery('#modal_otp_cod').modal('open');
                                unblockUI();
                            
                            }
                                
                            }
                          
                      }else{
                              if(result['message']!=''){
                                   toast(result['message'], 'error');    

                              }
                                unblockUI();
                        
                      }

                }, err => {
                    console.log(err);
                }
            );
        }else{
            
            this.submitPayemntData();
        }
 

    }
    resendOtp() {
        blockUI();
        this.pageService.resendOtpCodNumber(this.authData.id)
        .subscribe(result => {
            if (result['status_code'] == 1) {
                unblockUI();
                toast(result['message'], 'success');
            }
            else {
                unblockUI();
                toast(result['message'], 'error');
               
            }
        });
    }
    
     close_model(id) {
       jQuery('#verified_number')[0].reset();
        jQuery('#verified_number').find('label').removeClass('active');
        this.error = '';
        this.number_model = {};
     }
    submitOtp() {
        blockUI();
        this.loading = true;
        this.number_model['address'] =this.checkoutComponent.order_data['address'];
        this.pageService.mobileNumberVerified(this.number_model,this.authData.id)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                   
                    this.error = '';
                    jQuery('#modal_otp_cod').modal('close');
                    jQuery('#verified_number')[0].reset();
                    unblockUI();
                    
                    this.loading = false;
                     toast(this.success, 'success');
                     this.submitPayemntData();
                } 
                else {
                    this.error = result['message'];
                    this.loading = false;
                    unblockUI();
                    toast(this.error, 'error');
                }
            });

    }
    
    submitPayemntData(){
        
               this.cartService.payment(this.checkoutComponent.order_data).subscribe(

            result => {
                if (result['status_code'] == 1) {
                    this.paytm = result['paytm'];
                    this.payu = result['payu'];
                    this.PayUmoney = result['PayUmoney'];
                    this.type = result['payment_type'];
                    setTimeout(() => {
                        if (this.type == "PAYU") {

                            jQuery("#payuForm").submit();
                        }

                        if (this.type == "PAYTM") {

                            jQuery("#paytmform").submit();
                        }
                        if (this.type == "PayUmoney") {
                            jQuery("#PayUmoneyForm").submit();
                        }


                    }, 1000);
                    if (this.type == "wallet" || this.type == "COD") {
                        this.cartService.getCart(this.authData.id).subscribe(
                           cartData => {
                                 this.cartService.cart = cartData['data'];

                           }, err => {
                               console.log(err);
                           }
                       );
                        toast(result['message'], 'success');
                        this.router.navigate(['./myaccount/order']);
                        unblockUI();
                    }



                } else {

                    unblockUI();
                }


            }, err => {
                console.log(err);
            }
        );
        
    }
    checkwallet() {


        this.checkoutComponent.order_data['type'] = this.type;
        this.checkoutComponent.order_data['token'] = this.authData.token;

        this.cartService.checkwalletBalance(this.authData.id, this.checkoutComponent.order_data).subscribe(
            result => {

                if (result['status_code'] == 1) {
                    this.wallet['enable_wallet'] = result['enable_wallet'];
                    this.wallet['wallet'] = result['wallet'];
                    this.wallet['after_wallet_pay_rs'] = result['after_wallet_pay_rs'];
                    this.wallet['after_wallet_pay'] = result['after_wallet_pay'];
                    this.wallet['remaining_balance'] = result['remaining_balance'];
                    this.wallet['remaining_balance_rs'] = result['remaining_balance_rs'];

                    this.enable_wallet = result['enable_wallet'];
                    if (this.enable_wallet == 1) {

                        this.is_wallet = 1;
                        this.wallet['is_wallet'] = 1;
                    } else {
                        this.is_wallet = 0;
                        this.wallet['is_wallet'] = 0;
                    }


                } else {
                    this.wallet = [];
                    this.enable_wallet = 0;
                    this.wallet['enable_wallet'] = 0;
                    this.wallet['is_wallet'] = 0;
                    this.is_wallet = 0;
                }

            }, err => {
                console.log(err);
            }

        );

    }
    

        
             
            
        
     
    

    

}
