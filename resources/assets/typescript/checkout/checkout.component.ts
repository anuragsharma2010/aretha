import { Component, OnInit } from '@angular/core';
import { UserService } from "../service/user.service";
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { CartService } from "../service/cart.service";
import { Location } from '@angular/common';
import { blockUI, unblockUI, toast } from '../global';
declare var jQuery:any;

@Component({
    selector: 'checkout',
    template: require('./checkout.html')
})

export class CheckoutComponent implements OnInit {
    cod_charge: number;
    loading = false;
    second_step = false;
    error = '';
    errors = '';
    type = '';
    success = '';
    authData: any = [];
    carts: any = [];
    cart_length: number;
    selected_address: any = [];
    order_data: any = [];
    
    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private cartService: CartService,
        private location: Location,
        private router: Router,
    ){}
    
    ngOnInit() {
          
           
        if(Object.keys(this.cartService.orderData).length > 0){
        
            if(!this.isDisabled('address')){
                    this.router.navigate(['./checkout', 'address']);
            }
        
            this.authData = JSON.parse(localStorage.getItem('loginUser'));
        
            if(this.authData && this.authData.id){

                this.carts = this.cartService.orderData;
                this.cart_length = this.cartService.orderData['products'].length;
                this.order_data['email'] = this.authData.email;
            
                this.order_data =  this.cartService.orderData;
             
            }
        }else{
           
           this.router.navigate(['/']);
        }
   
    }
    
    changeUrl(url){
        if(this.second_step){
            this.router.navigate(['./checkout', url]);
        }
        
        if(url === 'address'){
            this.second_step = false;
        }
    }
    
    isDisabled (path) {
        return this.location.path().indexOf(path) > -1;
    }
}
