import {Component, Pipe, PipeTransform} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let keys = [];
    for (let key in value) {
      keys.push({key: key, value: value[key]});
    }
    return keys;
  }
}

@Pipe({name: 'values'})
export class ValuesPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let values = [];
    for (let key in value) {
      values.push(value[key]);
    }
    return values;
  }
}

@Pipe({name: 'flatten'})
export class FlattenPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    return [].concat.apply([],value);
  }
}

@Pipe({name: 'capitalize'})
export class CapitalizePipe implements PipeTransform {
    transform(value:string) {
        if (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
        return value;
    }

}

@Pipe({name: 'ucupper'})
export class UcupperPipe implements PipeTransform {
    transform(value:string) {
        if (value) {
            return value.toUpperCase();
        }
        return value;
    }

}


@Pipe({
  name: 'sanitizeHtml'
})
export class SanitizeHtmlPipe implements PipeTransform {

  constructor(private _sanitizer:DomSanitizer) {
  }

  transform(v:string) {
    return this._sanitizer.bypassSecurityTrustHtml(v);
  }
}



@Pipe({
  name: 'safeUrl'
})
export class SafeUrlPipe implements PipeTransform {
  constructor(private domSanitizer: DomSanitizer) {}
  transform(url) {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }
}