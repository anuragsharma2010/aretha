import { Component, OnInit, Input, OnDestroy, AfterViewInit } from '@angular/core';
import { Title, DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductService } from "../service/product.service";
import { CartService } from "../service/cart.service";
import { AuthenticationService } from '../service/authentication.service';
import { blockUI, unblockUI, modalBlockUI, modalUnBlockUI, toast } from '../global';
import { PageService } from "../service/page.service";
import { CeiboShare } from 'ng2-social-share';
import { UserService } from "../service/user.service";
declare var jQuery: any;

@Component({
    selector: 'product-detail',
    template: require('./productdetail.html'),
    inputs: ['product'],


})

export class ProductDetailComponent implements OnInit {
    productDetail: any = [];
    category: any = [];
    product: any = [];
    add_product: any = {};
    images: any = [];
    thumbnail: any = [];
    settings: any = [];
    authDatas: any = [];
    select_size: string = '';
    repeat_size: string = '';
    size_error: boolean = false;
    success = '';
    error = '';
    pinerror = 0;
    add_wishlist: any = {};
    productWishlist: any = [];
    wish_flag = true;
    loading = false;
    sizes = '';
    toggalFlage = 0;
    pincode = '';
    pincode_data: any = [];
    refreshFlag = 0;
    currentSlider = 0;
    public repoUrl = '';
    public imageUrl = ''
    public site_title = '';
    public whatsapp: any = '';


    constructor(
        private productService: ProductService,
        private cartService: CartService,
        private pageService: PageService,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private titleService: Title,
        private sanitizer: DomSanitizer,
        private userService: UserService
    ) {
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
    }

    ngOnInit() {
        modalBlockUI();
        this.route.params.subscribe(params => {
            
            let slug = params['slug'];
            let userId = '';
            if (this.authenticationService.isLoggedIn()) {
                userId = this.authDatas.id;
            }
            let url = window.location.href.toString();
            this.repoUrl = url.split("?")[0];


            this.productService.getProductDetail(slug, userId).subscribe(
                products => {
                    modalBlockUI();
                    this.refreshFlag = 0;
                    this.select_size = '';
                    this.size_error = false;
                    jQuery("html, body").animate({ scrollTop: 10 }, "slow");
                    this.productDetail = products;
                    this.product = products['product'];
                    this.images = products['images'];
                    this.category = products['category'];

                    jQuery('meta[property="og:url"]').attr('content', this.repoUrl);
                    jQuery('meta[property="og:title"]').attr('content', products['product']['title']);
                    jQuery('meta[property="og:description"]').attr('content', products['product']['description']);
                    jQuery('meta[name="title"]').attr('content', products['product']['meta_title']);
                    jQuery('meta[name="description"]').attr('content', products['product']['meta_description']);
                    jQuery('meta[name="keywords"]').attr('content', products['product']['meta_keywords']);
                    if (products['no_images'] == '') {
                        jQuery('meta[property="og:image"]').attr('content', products['images'][0]['orignail_image']);
                        this.imageUrl = products['images'][0]['orignail_image'];
                    } else {
                        jQuery('meta[property="og:image"]').attr('content', products['no_images']);
                        this.imageUrl = products['no_images'];

                    }
                    
                   let wurl = 'whatsapp://send?text='+products['product']['title']+' : '+this.repoUrl;
                   
                    this.whatsapp = this.sanitize(wurl);

                    this.titleService.setTitle(products['product']['title']);

                    this.thumbnail = products['thumbnail'];
                    this.sizes = products['size'];
                    if (this.product['in_wishlist'] == 1) {
                        this.wish_flag = false;
                    }
                    jQuery(document).ready(function() {






                        if (jQuery('.slider-for').hasClass('slick-initialized')) {
                            jQuery('.slider-for').slick('unslick');;
                        }

                        if (jQuery('.slider-nav').hasClass('slick-initialized')) {
                            jQuery('.slider-nav').slick('unslick');
                        }
                        if (jQuery('.slider-for-zoom').hasClass('slick-initialized')) {
                            jQuery('.slider-for-zoom').slick('unslick');
                        }
                    });

                   jQuery(document).scroll(function() {
                       if(jQuery(window).width() < 641){
                             if (jQuery('#sticker').offset().top + jQuery('#sticker').height() >= jQuery('#description').offset().top -5) {
                                     jQuery('#sticker').removeClass('fixed_div');
                           }                         
                           if (jQuery(document).scrollTop() + window.innerHeight < jQuery('#description').offset().top) {
                               jQuery('#sticker').addClass('fixed_div');

                            }
                        }else{

                            jQuery('#sticker').removeClass('fixed_div');
                        }
                            
                    });
                       jQuery(window).resize(function() {    

                                if(jQuery(window).width() < 641){
                             if (jQuery('#sticker').offset().top + jQuery('#sticker').height() >= jQuery('#description').offset().top -5) {
                                     jQuery('#sticker').removeClass('fixed_div');
                           }                         
                           if (jQuery(document).scrollTop() + window.innerHeight < jQuery('#description').offset().top) {
                               jQuery('#sticker').addClass('fixed_div');

                            }
                        }else{

                            jQuery('#sticker').removeClass('fixed_div');
                        }
                   });








                    if (products['images']) {
                        jQuery(document).ready(function() {
                            setTimeout(() => {
                                jQuery('.slider-for').slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    lazyLoad: 'ondemand',
                                    speed: 500,
                                    arrows: true,
                                    fade: true,
                                    asNavFor: '.slider-nav',
                                    mobileFirst: true,
                                    responsive: [
                                        {
                                            breakpoint: 1024,
                                            settings: {
                                                arrows: false,
                                            }
                                        },
                                        {
                                            breakpoint: 768,
                                            settings: {
                                                arrows: false,
                                            }
                                        },
                                        {
                                            breakpoint: 767,
                                            settings: {
                                                arrows: true,
                                            }
                                        },
                                        {
                                            breakpoint: 360,
                                            settings: {
                                                arrows: true,
                                            }
                                        }
                                    ]
                                });
                                let self = this;
                                jQuery('.slider-nav').slick({
                                    slidesToShow: 5,
                                    slidesToScroll: 1,
                                    speed: 500,
                                    asNavFor: '.slider-for',
                                    dots: false,
                                    centerMode: false,
                                    focusOnSelect: true,
                                    slide: 'div',
                                    arrows: false,
                                    variableWidth: false,
                                    vertical: true,
                                    verticalSwiping: true,
                                    waitForAnimate: true,
                                    zIndex: 1000,
                                    slideWidth: '400',
                                    swipe: false,
                                    adaptiveHeight: true,

                                });

                                jQuery('.slider-for-zoom').slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    speed: 500,
                                    arrows: true,
                                    fade: true,
                                    mobileFirst: true,
                                    dots: true,
                                    /* nextArrow: '<i                                                                       prevArrow: '<i class="fa fa-arrow-left"></i>',*/

                                });
                                jQuery("#currnt_slide").val(0);
                                jQuery('.slider-for').on('afterChange', function(event, slick, currentSlide, nextSlide) {
                                    jQuery(".slider-for-zoom").slick('slickGoTo', currentSlide);
                                    jQuery("#currnt_slide").val(currentSlide);


                                });


                                if (jQuery('#modal4 table').length) {
                                    jQuery('#modal4 table').addClass("bordered centered striped responsive-table");
                                }



                            }, 1000);

                        });
                    }

                    if (this.productDetail['related_product']) {
                        jQuery(document).ready(function() {
                            if (typeof jQuery('#owl-demo').data('owlCarousel') != 'undefined') {
                                jQuery('#owl-demo').data('owlCarousel').destroy();
                                jQuery('#owl-demo').removeClass('owl-carousel');
                            }

                            setTimeout(() => {
                                jQuery("#owl-demo").owlCarousel({
                                    autoPlay: 3000, //Set AutoPlay to 3 seconds
                                    items: 4,
                                    loop: true,
                                    itemsDesktop: [1199, 3],
                                    itemsDesktopSmall: [979, 3]
                                });

                                jQuery(".next").click(function() {
                                    jQuery("#owl-demo").trigger('owl.next');
                                });

                                jQuery(".prev").click(function() {
                                    jQuery("#owl-demo").trigger('owl.prev');
                                });

                            }, 1000);
                        });
                    }
                    setTimeout(() => {
                        modalUnBlockUI();



                    }, 2000);
                }, err => {
                    console.log(err);
                }
            );
        });

        this.pageService.getSettings().subscribe(
            settings => {
                this.settings = settings;
                this.site_title = settings['CONFIG_SITE_TITLE'];
                setTimeout(() => {
                    jQuery(document).ready(function() {
                        jQuery('.tabs').tabs();
                        jQuery('.modal').modal();

                    });
                }, 1000);
            }, err => {
                console.log(err);
            }
        );
    }



    setSize(size) {
        this.select_size = size;
        this.size_error = false;
    }

    sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
}
    addToCart(choose_size) {
        if (this.authenticationService.isLoggedIn()) {
            if (this.select_size === '') {
                this.size_error = true;
            }
            else {
        
                    blockUI();
                    this.add_product['user_id'] = this.authDatas.id;
                    this.add_product['product_id'] = this.product.id;
                    this.add_product['product_size'] = this.select_size;

                    
                    this.cartService.addCart(this.add_product)
                        .subscribe(result => {
                            if (result['status_code'] === 1) {
                                this.repeat_size = choose_size;
                                this.success = result['message'];
                                unblockUI();
                                toast(this.success, 'success');
                            } else {
                                this.error = result['message'];
                                unblockUI();
                                toast(this.error, 'error');
                            }
                        });





                    if (this.toggalFlage == 1) {
                        jQuery(document).ready(function() {
                            jQuery('#zoomModal').modal('close');
                        });
                        this.toggalFlage = 0;
                    }
                
            }
        }
        else {
            jQuery(document).ready(function() {
                jQuery('#modal1').modal('open');
            });
        }
    }

    addToWishlist() {
        if (this.authenticationService.isLoggedIn()) {
            blockUI();
            this.loading = true;
            let userData = this.authenticationService.authData;
            this.add_wishlist['product_id'] = this.product.id;
            this.add_wishlist['user_id'] = userData['id'];
            this.add_wishlist['userToken'] = userData['token'];
            this.cartService.addWishlist(this.add_wishlist).subscribe(
                result => {
                    unblockUI();
                    this.loading = false;
                    if (result['status_code'] === 1) {
                        this.success = result['message'];
                        toast(this.success, 'success');
                        this.wish_flag = false;
                    } else {
                        this.error = result['message'];
                        toast(this.error, 'error');
                    }
                }
            );
        }
        else {
            jQuery('#modal1').modal('open');
        }
    }
    ngAfterViewInit() {
        setTimeout(() => {
            // modalUnBlockUI();
        }, 2000);
    }
    toggleZoom() {

        blockUI();

        jQuery('#zoomModal').modal('open');

        setTimeout(() => {

            if (this.refreshFlag == 0) {
                jQuery(document).ready(function() {
                    jQuery('.slider-for-zoom').slick('refresh');
                    jQuery(".slider-for-zoom").slick('slickGoTo', jQuery("#currnt_slide").val());
                });
                this.refreshFlag = 1;
            }
            // jQuery(max-height
            jQuery(document).ready(function() {
                jQuery("#zoomModal").mCustomScrollbar({
                    autoDraggerLength: false,

                    auto: "yes",
                    mouseWheel: {
                        scrollAmount: 128
                    }
                });
            });

            unblockUI();


        }, 1000);
        this.toggalFlage = 1;





    }

    CheckPinCode() {
           this.pincode  =  this.pincode .replace(/[^0-9]/g, '');
        if (this.pincode != '' && this.pincode.length==6) {
            this.userService.getPincodeAvalability(this.pincode)
                .subscribe(result => {
                    if (result['status_code'] === 1) {
                        this.pincode_data = result;
                        if(result['cash']=='n'){
                            this.pinerror=1;
                        }else{
                            this.pinerror=0;
                        }
                            
                    } else {

                        this.pincode_data = result;
                        this.pinerror=1;

                    }
                });

        }else{

             this.pincode_data=[];
             this.pinerror=1;
        }



    }



}
