import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { blockUI, unblockUI, toast } from '../global';
import { PagerService } from "../service/pager.service";
import { CartService } from "../service/cart.service";
import { PageService } from "../service/page.service";
declare var jQuery: any;
@Component({
    selector: 'my-order',
    template: require('./myorder.html')
})

export class MyOrderComponent {
    total: number;
    per_page: number;
    current_page: number;
    page: number = 1;
    last_page: number;
    pager: any = {};
    success: '';
    error: '';
    orders: any = [];
    authDatas: any = {};
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private pagerService: PagerService,
        private pageService: PageService,
    ) { }
    ngOnInit() {

        this.router.routerState.root.queryParams.subscribe(params => {
            if (params['page']) {
                this.page = parseInt(params['page']);
            }



            this.getList();
        });

           setTimeout(() => {
             jQuery(document).ready(function() {
                 jQuery('.tabs').tabs();
               });

            }, 100);



    }
    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service

        this.page = page;
        // get current page of items
        this.getList();
    }
    getList() {

        blockUI();
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
        this.pageService.getOrder(this.authDatas.id, this.page).subscribe(
            orderData => {
                this.orders =    orderData['data'];
                this.total =     orderData['total'];
                this.per_page =  orderData['per_page'];;
                this.pager =     this.pagerService.getPager(this.total, this.page, this.per_page);             
            }, err => {

                console.log(err);
            }
        );

        unblockUI();

    }
}
