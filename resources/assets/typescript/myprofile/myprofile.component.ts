import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { blockUI, unblockUI, toast } from '../global';
import { AuthenticationService } from '../service/authentication.service';
declare var jQuery: any;
declare var Materialize: any;

@Component({
    selector: 'my-profile',
    template: require('./myprofile.html')
})

export class MyProfileComponent implements OnInit {
    @Input() model;
    authDatas: any = [];
    userData: any = [];
    profilemodel: any = {};
    number_model: any = {};
    loading = false;
    error = '';
    success = '';
    phone_verify = '';
    phone_number = '';
    user_id = '';
    new_number = 0;
    enble_sms: number = 0;

    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.authDatas =  this.authenticationService.authData;
        this.userService.getUserDetail(this.authDatas.id, this.authDatas.token).subscribe(
            userDetail => {
                this.userData = userDetail['user_data'];
                this.profilemodel['first_name'] = this.userData['first_name'];
                this.profilemodel['last_name'] = this.userData['last_name'];
                this.profilemodel['phone'] = this.userData['phone'];
                this.phone_number = this.userData['phone'];
                this.profilemodel['email'] = this.userData['email'];
                this.profilemodel['gender'] = this.userData['gender'];
                this.phone_verify = this.userData['phone_verify'];
                this.enble_sms = userDetail['enble_sms'];

                setTimeout(() => {
                    Materialize.updateTextFields();

                }, 1000);
            }, err => {
                console.log(err);
            }
        );
        setTimeout(() => {
            jQuery(document).ready(function() {
                jQuery('.tabs').tabs();
                jQuery('.modal').modal();
            
            });

        }, 100);
    }
    phoneVerify() {

        if (this.enble_sms) {


            this.user_id = this.authDatas.id;
             blockUI();
        this.authenticationService.resendOtpPassword(this.user_id)
            .subscribe(result => {
                if (result['status_code'] == 1) {
                   jQuery('#modal_otp_myfrofile').modal('open');
                    unblockUI();
                    toast(result['message'], 'success');
                }
                else {
                    unblockUI();
                    toast(result['message'], 'error');

                }
            });
         

        }


    }
    profileSubmit() {
        blockUI();
        this.loading = true;
        this.profilemodel['id'] = this.authDatas.id;
        this.profilemodel['token'] = this.authDatas.token;

        this.userService.profileSave(this.profilemodel)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.new_number = result['new_number'];
                    this.phone_number = this.profilemodel['phone'];
                    if (result['new_number'] == 1) {
                        this.phone_number = result['mobile_number'];
                        this.user_id = this.authDatas.id;
                        jQuery('#modal_otp_myfrofile').modal('open');
                    }


                    this.error = '';
                    unblockUI();
                    this.loading = false;
                    toast(this.success, 'success');


                    let auth_data = JSON.stringify(result['user_data']);
                    if (auth_data) {
                    // set token property
                    

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('loginUser', auth_data);
                      this.authenticationService.setAuthData();

                       this.authDatas= this.authenticationService.authData;

                    } 
                }
                else {

                    this.error = result['errors'];
                    this.loading = false;
                    unblockUI();
                    toast(result['message'], 'error');
                }
            });
    }
    resendOtp() {
        blockUI();
        this.authenticationService.resendOtpPassword(this.user_id)
            .subscribe(result => {
                if (result['status_code'] == 1) {
                    unblockUI();
                    toast(result['message'], 'success');
                }
                else {
                    unblockUI();
                    toast(result['message'], 'error');

                }
            });
    }
    close_model(id) {
        jQuery('#verified_number')[0].reset();
        jQuery('#verified_number').find('label').removeClass('active');
        this.error = '';
        this.number_model = {};
    }
    submitOtp() {
        blockUI();
        this.loading = true;
        this.number_model['user'] = this.user_id;
        this.authenticationService.mobileNumberVerified(this.number_model)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.number_model = '';
                    this.error = '';
                    jQuery('#modal_otp_myfrofile').modal('close');
                    jQuery('#verified_number')[0].reset();
                    unblockUI();
                    this.phone_verify = '1';
                    this.loading = false;
                    toast(this.success, 'success');
                }
                else {
                    this.error = result['message'];
                    this.loading = false;
                    unblockUI();
                    toast(this.error, 'error');
                }
            });

    }
}
