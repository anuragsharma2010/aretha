import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { PagerService } from "../service/pager.service";
import { blockUI, unblockUI, toast } from '../global';
import { PageService } from "../service/page.service";
declare var jQuery: any;
@Component({
    selector: 'my-wallet',
    template: require('./mywallet.html')
})

export class MyWalletComponent {
    authDatas: any = {};
    walletDetail: any = [];
    total: number;
    per_page: number;
    current_page: number;
    page: number = 1;
    wallet_amount: string = '';
    last_page: number;
    pager: any = {};
    success: '';
    error: '';
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private pageService: PageService,
        private pagerService: PagerService,
    ) { }

    ngOnInit() {
        this.router.routerState.root.queryParams.subscribe(params => {
            if (params['page']) {
                this.page = parseInt(params['page']);
            }

            this.getList();
        });
        setTimeout(() => {
             jQuery(document).ready(function() {
                 jQuery('.tabs').tabs();
               });

            }, 100);

    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service

        this.page = page;
        // get current page of items
        this.getList();
    }



    getList() {

        blockUI();
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
        this.pageService.getWallet(this.authDatas.id, this.page).subscribe(
            walletData => {
             
                this.walletDetail = walletData['data'];
                this.total = walletData['total'];
                this.per_page = walletData['per_page'];;
                this.wallet_amount = walletData['wallet_amount'];;
    
                this.pager = this.pagerService.getPager(this.total, this.page, this.per_page);


            }, err => {

                console.log(err);
            }
        );

        unblockUI();

    }
}
