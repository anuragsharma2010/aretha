import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { blockUI, unblockUI, toast } from '../global';
import { AuthenticationService } from '../service/authentication.service';
declare var jQuery:any;

@Component({
    selector: 'change-password',
    template: require('./changepassword.html')
})

export class ChangePasswordComponent{
    changepasswordmodel: any = {};
    authDatas: any = [];
    loading = false;
    error = '';
    success = '';
    is_socail = 0;
    
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
         private authenticationService: AuthenticationService,
    ){}
    ngOnInit() {
       this.authDatas = this.authenticationService.authData;
        setTimeout(() => {
            jQuery(document).ready(function() {
                jQuery('.tabs').tabs();
            });

        }, 100);


      this.userService.getUserDetails(this.authDatas.id, this.authDatas.token).subscribe(    userDetail => {
                
                this.is_socail = userDetail['is_socail'];

            }, err => {
                console.log(err);
            });

    }
    changePasswordSubmit(){
        blockUI();
        this.loading = true;
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
        this.changepasswordmodel['user_id'] = this.authDatas.id;
        this.changepasswordmodel['token'] = this.authDatas.token;
        this.userService.changePasswordSave(this.changepasswordmodel)
        .subscribe(result => {
            if (result['status_code'] === 1) {
                this.success = result['message'];
                this.error = '';
                jQuery('#changePassword')[0].reset();
                jQuery('#changePassword').find('label').removeClass('active');
                unblockUI();
                this.loading = false;
                 this.is_socail = 0;
                toast(this.success, 'success');
            } else {
                this.error = result['error'];
                this.loading = false;
                unblockUI();
                toast(result['message'], 'error');
            }
        });
    }
}
