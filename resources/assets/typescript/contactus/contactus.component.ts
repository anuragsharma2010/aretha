import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PageService } from "../service/page.service";
import { DomSanitizer } from '@angular/platform-browser';
import { blockUI, unblockUI, toast } from '../global';
declare var jQuery: any;

@Component({
    selector: 'contact-us',
    template: require('./contactus.html')
})

export class ContactUsComponent {

    settings: any = [];
    contactModel: any = {};
    loading = false;
    error = '';
    success = '';
    constructor(
        private pageService: PageService,
        private route: ActivatedRoute,
        private router: Router,
        private sanitizer: DomSanitizer
    ) {

    }

    ngOnInit() {

        this.pageService.getSettings().subscribe(
            settings => {
                this.settings = settings;
                this.settings['CONFIG_GOOGLE_MAP'] = this.sanitizer.bypassSecurityTrustHtml(settings['CONFIG_GOOGLE_MAP']);

                setTimeout(() => {

                    jQuery('.map iframe').height("100%");
                    jQuery('.map iframe').width("100%");
                    setTimeout('resizeequalheight()', 250)
                    jQuery(window).resize(function() {
                        setTimeout('resizeequalheight()', 250)

                    });

                }, 1000);



            }, err => {
                console.log(err);
            }
        );
    }

    savecontactus() {
        blockUI();
        this.loading = true;
        this.pageService.contactUs(this.contactModel)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.contactModel = '';
                    this.error = '';
                    unblockUI();


                    this.router.navigate(['/']);
                    toast("Your Conatct enquery Send successfully", 'success');

                } else {
                    this.error = result['errors'];
                    toast("Required field can't empty", 'error');
                    this.loading = false;
                    unblockUI();
                }
            });
    }

}
