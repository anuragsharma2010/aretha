import {RequestOptions, RequestMethod, Headers} from '@angular/http';

     const authData = JSON.parse(localStorage.getItem('loginUser'));
export class MyOptions extends RequestOptions {

      constructor() { 
        super({ 
          method: RequestMethod.Get,
          headers: new Headers({
            'Content-Type': 'application/json',
            'user-id': (authData!=undefined)?authData['id']:'null',
            'user-token': (authData!=undefined)?authData['token']:'null',
            'X-Requested-With':'XMLHttpRequest'

          })
        });
      }
    } 