import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { blockUI, unblockUI, toast } from '../global';
declare var jQuery: any;
declare var Materialize: any;

@Component({
    selector: 'my-address',
    template: require('./myaddress.html')
})

export class MyAddressComponent implements OnInit {
    open_address: boolean = false;
    edit_address: boolean = false;
    authDatas: any = [];
    addressData: any = [];
    search: any = [];
    addressmodel: any = {};
    editAddressmodel: any = {};
    defaultAddress: any = {};
    loading = false;
    error = '';
    errors = '';
    success = '';

    constructor(
        private userService: UserService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
        this.userService.getUserAddress(this.authDatas.id, this.authDatas.token).subscribe(
            addressDetail => {
                this.addressData = addressDetail['data'];
            }, err => {
                console.log(err);
            }
        );
        setTimeout(() => {
            jQuery(document).ready(function() {
                jQuery('.tabs').tabs();
            });

        }, 100);
    }

    addressSubmit() {
        blockUI();
        this.loading = true;
        this.addressmodel['user_id'] = this.authDatas.id;
        this.addressmodel['token'] = this.authDatas.token;
        this.userService.addressSave(this.addressmodel)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.error = '';
                    jQuery('#userAddress')[0].reset();
                    jQuery('#userAddress').find('label').removeClass('active');
                    this.open_address = false;
                    unblockUI();
                    this.loading = false;
                    this.addressData.push(result['data']);
                    toast(this.success, 'success');
                } else {
                    this.error = result['error'];
                    this.errors = result['message'];
                    this.loading = false;
                    this.open_address = true;
                    unblockUI();
                    toast(this.errors, 'error');
                }
            });
    }

    getdetail(modelName, pincode) {
        this.userService.getPincodeDetail(pincode)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    if (modelName == 'addressmodel') {
                        this.addressmodel['state'] = result['data']['state'];
                        this.addressmodel['city'] = result['data']['city'];
                        
                    } else {
                        this.editAddressmodel['edit_state'] = result['data']['state'];
                        this.editAddressmodel['edit_city'] = result['data']['city'];
                        
                    }
                             setTimeout(() => {
                    Materialize.updateTextFields();

                }, 100);
                }
            });
    }

    editAddressSubmit() {
        blockUI();
        this.loading = true;
        this.editAddressmodel['user_id'] = this.authDatas.id;
        this.editAddressmodel['token'] = this.authDatas.token;
        this.edit_address = false;
        this.userService.editAddressSave(this.editAddressmodel)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.error = '';
                    jQuery('#editAddress')[0].reset();
                    jQuery('#editAddress').find('label').removeClass('active');
                    this.edit_address = false;
                    unblockUI();
                    this.loading = false;
                    this.addressData = result['data'];
                    toast(this.success, 'success');
                } else {
                    this.error = result['error'];
                    this.errors = result['message'];
                    this.loading = false;
                    this.edit_address = true;
                    unblockUI();
                    toast(this.errors, 'error');
                }
            });
    }

    showEdit(address_id) {
        this.editAddressmodel = this.addressData.filter(address1 => address1.id == address_id)[0];
        this.editAddressmodel.edit_first_name = this.editAddressmodel.first_name;
        this.editAddressmodel.edit_last_name = this.editAddressmodel.last_name;
        this.editAddressmodel.edit_address_1 = this.editAddressmodel.address_1;
        this.editAddressmodel.edit_address_2 = this.editAddressmodel.address_2;
        this.editAddressmodel.edit_mobile = this.editAddressmodel.mobile;
        this.editAddressmodel.edit_pin_code = this.editAddressmodel.pin_code;
        this.editAddressmodel.edit_city = this.editAddressmodel.city;
        this.editAddressmodel.edit_state = this.editAddressmodel.state;
        jQuery('#editAddress').find('label').addClass('active');
    }

    setDefaultAddress(address_id, is_default) {
        if (is_default == 0) {
            this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
            blockUI();
            this.defaultAddress['address_id'] = address_id;
            this.defaultAddress['user_id'] = this.authDatas.id;
            this.userService.setAddressDefault(this.defaultAddress)
                .subscribe(result => {
                    if (result['status_code'] === 1) {
                        this.addressData = result['data']
                        this.success = result['message'];
                        unblockUI();
                        toast(this.success, 'success');
                    } else {
                        this.errors = result['message'];
                        unblockUI();
                        toast(this.errors, 'error');
                    }
                });
        }
        else {
            toast('Already set default', 'error');
            return false;
        }
    }

    deleteAddress(address_id) {
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
        blockUI();
        this.userService.deleteUserAddress(address_id, this.authDatas.id)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.addressData = result['data']
                    this.success = result['message'];
                    unblockUI();
                    toast(this.success, 'success');
                } else {
                    this.errors = result['message'];
                    unblockUI();
                    toast(this.errors, 'error');
                }
            });
    }

    close_address_form(mode) {
        this.open_address = !mode;
        this.edit_address = !mode;
        jQuery('#userAddress')[0].reset();
        jQuery('#editAddress')[0].reset();
        jQuery('#userAddress').find('label').removeClass('active');
        jQuery('#editAddress').find('label').removeClass('active');
        this.userService.getUserAddress(this.authDatas.id, this.authDatas.token).subscribe(
            addressDetail => {
                this.addressData = addressDetail['data'];
            }, err => {
                console.log(err);
            }
        );
    }
}
