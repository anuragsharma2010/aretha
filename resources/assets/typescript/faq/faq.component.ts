import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PageService } from "../service/page.service";
import { Title } from '@angular/platform-browser';
declare var jQuery: any;
@Component({
    selector: 'faq',
    template: require('./faq.html')
})

export class FaqComponent implements OnInit {
    faqs: any = [];

    constructor(
        private pageService: PageService,
        private route: ActivatedRoute,
        private titleService: Title
    ) { }

    ngOnInit() {
        this.setTitle("Faq");

        this.route.params.subscribe(params => {
            this.pageService.getFaq().subscribe(
                pageDetail => {
                    this.faqs = pageDetail['data'];
                    setTimeout(() => {
                        jQuery('.tabs').tabs();
                        jQuery('.collapsible').collapsible();
                    }, 2000);
                }, err => {
                    console.log(err);
                }
            );
        });

        if (jQuery(window).width() < 640) {
            jQuery(".left_active").show();
            jQuery(".right_active").hide();
            jQuery(".back_link").click(function() {
                jQuery(".left_active").show();
                jQuery(".right_active").hide();
            });
        }else{

                    jQuery(".left_active").show();
                  jQuery(".right_active").show();
                    jQuery(".back_link").hide();
        }

        jQuery(window).resize(function() {
            if (jQuery(window).width() < 640) {
                jQuery(".left_active").show();
                jQuery(".right_active").hide();
                jQuery(".back_link").click(function() {
                    jQuery(".left_active").show();
                    jQuery(".right_active").hide();
                });
            }else{

                  jQuery(".left_active").show();
                  jQuery(".right_active").show();
                    jQuery(".back_link").hide();
            }
        });
    }

    tabClick() {
        if (jQuery(window).width() < 640) {
            jQuery(".left_active").hide();
            jQuery(".right_active").show();
            jQuery(".back_link").show();
        }
    }
    
    public setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }
}
