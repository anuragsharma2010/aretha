import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductService } from "../service/product.service";
import { PagerService } from "../service/pager.service";
import { blockUI, unblockUI, toast } from '../global';
import { Location } from '@angular/common';
import { CartService } from "../service/cart.service";


declare var jQuery: any;
import * as _ from 'underscore';
@Component({
    selector: 'product-listing',
    template: require('./search.html'),
})

export class SearchComponent implements OnInit {
    products: any = [];
    filters: any = [];
    categories: any = [];
    total: number;
    per_page: number;
    current_page: number;
    page: number = 1;
    last_page: number;
    slug: string;
    search: any = [];
    searchCat: any = [];
    pager: any = {};
    sortBy: string = 'popular';
    attributes: any = [];
    valuses: any = [];
    PostData: any = {};
    ChildCategory: any = [];


    constructor(
        private productService: ProductService,
        private route: ActivatedRoute,
        private router: Router,
        private pagerService: PagerService,
        private location: Location,
        private titleService: Title,
        private cartService: CartService,
        
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.slug = params['search_text'];
                    this.router.routerState.root.queryParams.subscribe(params => {
                           if(this.location.path().indexOf('search/') > -1){
                            if (params['page']) {
                                this.page = parseInt(params['page']);
                            }
                            if (params['sortBy']) {
                                this.sortBy = params['sortBy'];
                            }
                            if (params['filter']) {
                                let filter = params['filter'];
                                let filter_data = filter.split(";");
                                for (let attribute_data of filter_data) {
                                    let split_array = attribute_data.split("--");
                                    this.search.push({ attribute: split_array[0], data: split_array[1] });
                                    this.attributes.push(split_array[0]);
                                    let split_value = split_array[1].split(",");
                                    for (let value of split_value) {
                                        this.valuses.push(value);
                                    }
                                }
                            } else {
                                this.search = [];
                                this.attributes = [];
                                this.valuses = [];
                            }

                            if(params['category']){
                                  let category =   params['category'];
                                 this.ChildCategory = category.split(",");

                            }else{

                                 this.ChildCategory = [];
                            }

                            this.getList();
                            }
                        });

                        this.productService.getAllAttribute(this.slug).subscribe(
                            attributedata => {
                                this.filters = attributedata['filter'];



                                this.categories = attributedata['category'];
                                

                                setTimeout(() => {
                                    jQuery('.collapsible').collapsible();
                                }, 2000);
                            }, err => {

                                console.log(err);
                            }
                        );

        });


    }

    setsortBy(sort_by: string) {
        this.sortBy = sort_by;
        this.page = 1;
        jQuery('#modalSort').modal('close');
        this.getList();
    }
       setCategory(category: string) {


            let index = _.indexOf(this.ChildCategory, category);
             if (index == -1) {
                 this.ChildCategory.push(category);

             }else{

                  this.ChildCategory = _.without(this.ChildCategory, category);
             }



        this.page = 1;
        jQuery('#modalFilter').modal('close');
        this.getList();
    } 
    setfilter(attribute: string, value: string) {
        let attribute_data = this.search.filter(search => search.attribute === attribute).pop();
        if (attribute_data) {
            let data = attribute_data.data;
            let spitdata = data.split(",");
            let index = _.indexOf(spitdata, value);
            if (index == -1) {
                spitdata.push(value);
                this.valuses.push(value);
            } else {
                spitdata = _.without(spitdata, value);
                this.valuses = _.without(this.valuses, value);
            }
            if (spitdata.length > 0) {
                data = spitdata.join();
                Object.assign(attribute_data, { data: data });
            } else {
                this.search = this.search.filter(search => search.attribute !== attribute);
                this.attributes = _.without(this.attributes, attribute);
            }
        } else {

            this.search.push({ attribute: attribute, data: value });
            this.attributes.push(attribute);
            this.valuses.push(value);
        }
        this.page = 1;
        jQuery('#modalFilter').modal('close');
        this.getList();
    }




    clearFilter() {

        this.search = [];
        this.searchCat = [];
        this.attributes = [];
        this.valuses = [];
        this.getList(); 

    }
    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service

        this.page = page;
        // get current page of items
        this.getList();
    }
    getList() {
        blockUI();
         
        if(this.ChildCategory.length){
            this.PostData['category']  = this.ChildCategory;
          

        }

           this.PostData['search']  = this.search;
         
         
        this.productService.getSearchProduct(this.slug, this.page, this.sortBy, this.PostData).subscribe(
            productsdata => {

                this.products = productsdata['products'];
                this.total = productsdata['products']['total'];
                this.per_page = productsdata['products']['per_page'];;


                this.pager = this.pagerService.getPager(this.total, this.page, this.per_page);

                this.changeUrl();

                setTimeout(() => {
                    jQuery(document).ready(function() {
                        jQuery('.collapsible').collapsible();
                        jQuery('.modal').modal();
                        // jQuery('#modalFilter').modal('close');
                        //jQuery('#modalSort').modal('close');
                    });



                }, 1000);
            }, err => {

                console.log(err);
            }
        );
        unblockUI();
    }


    changeUrl() {



        let url = 'search/' + this.slug + '?page=' + this.page + '&sortBy=' + this.sortBy;

        if (this.search) {

            let filter = '';
            let i: number = 0;
            for (let entry of this.search) {
                if (i > 0) {
                    filter = filter + ';'
                }
                filter = filter + entry.attribute + "--" + entry.data;
                i = i + 1;
            }
            if (filter) {
                url = url + "&filter=" + filter;
            }
        }

        if (this.ChildCategory) {

            let catfilter = this.ChildCategory.join();
    
            if (catfilter) {
                url = url + "&category=" + catfilter;
            }
        }

        let count = 0;
        let query_string = '';
        this.location.go(url);
    }
}
