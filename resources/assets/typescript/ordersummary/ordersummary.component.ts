import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { CartService } from "../service/cart.service";
import { CheckoutComponent } from "../checkout/checkout.component";
import { Router } from '@angular/router';
@Component({
    selector: 'order-summary',
    template: require('./ordersummary.html')
})

export class OrderSummaryComponent{
    authData: any = [];
    carts: any = [];
    cart_length: number;
    
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private checkoutComponent: CheckoutComponent, 
        private router: Router,
        private cartService: CartService
    ){}
    
    ngOnInit() {
              if(Object.keys(this.cartService.orderData).length > 0){
        this.authData = JSON.parse(localStorage.getItem('loginUser'));
        if(this.authData && this.authData.id){
          
                    this.carts = this.checkoutComponent.order_data
                    this.cart_length = this.checkoutComponent.order_data['products'].length;
                
            
        }
              }else{
           
                   this.router.navigate(['/']);
        }
    }
}
