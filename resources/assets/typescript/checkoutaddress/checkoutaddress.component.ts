import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UserService } from "../service/user.service";
import { blockUI, unblockUI, toast } from '../global';
import { CheckoutComponent }  from '../checkout/checkout.component';
import { CartService } from "../service/cart.service";
declare var jQuery:any;

@Component({
    selector: 'checkout-address',
    template: require('./checkoutaddress.html')
})

export class CheckoutAddressComponent implements OnInit{
    open_address: boolean = false;
    edit_address: boolean = false;
    authDatas: any = [];
    addressData: any = [];
    search: any = [];
    addressmodel: any = {};
    editAddressmodel: any = {};
    loading = false;
    error = '';
    errors = '';
    success = '';
    
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private checkout: CheckoutComponent,
        private cartService: CartService,
    ){}
    
    ngOnInit() {
      if(Object.keys(this.cartService.orderData).length > 0){

            this.authDatas = JSON.parse(localStorage.getItem('loginUser'));

            this.userService.getUserAddress(this.authDatas.id, this.authDatas.token).subscribe(
                addressDetail => {
                    this.addressData = addressDetail['data'];
                    if (this.addressData[0]['is_default']) {
                           this.checkout.selected_address = this.addressData[0];
                            this.checkout.order_data['address'] = this.addressData[0];
                            this.checkout.cod_charge = 0;
                            this.checkout.second_step = true;
                    }
                    console.log(this.addressData[0]);
                }, err => {
                    console.log(err);
                }
            );
        }else{
          
          this.router.navigate(['/']);
        }
    }
    
    addressSubmit() {
        blockUI();
        this.loading = true;
        this.addressmodel['user_id'] = this.authDatas.id;
        this.addressmodel['token'] = this.authDatas.token;
        this.userService.addressSave(this.addressmodel)
        .subscribe(result => {
            if (result['status_code'] === 1) {
                this.success = result['message'];
                this.error = '';
                jQuery('#userAddress')[0].reset();
                jQuery('#userAddress').find('label').removeClass('active');
                this.open_address = false;
                unblockUI();
                this.loading = false;
                this.addressData.push(result['data']);
                toast(this.success, 'success');
            } else {
                this.error = result['error'];
                this.errors = result['message'];
                this.loading = false;
                this.open_address = true;
                unblockUI();
                toast(this.errors, 'error');
            }
        });
    }
     
    editAddressSubmit() {
        blockUI();
        this.loading = true;
        this.editAddressmodel['user_id'] = this.authDatas.id;
        this.editAddressmodel['token'] = this.authDatas.token;
        this.edit_address = false;
        this.userService.editAddressSave(this.editAddressmodel)
        .subscribe(result => {
            if (result['status_code'] === 1) {
                this.success = result['message'];
                this.error = '';
                jQuery('#editAddress')[0].reset();
                jQuery('#editAddress').find('label').removeClass('active');
                this.edit_address = false;
                unblockUI();
                this.loading = false;
                this.addressData = result['data'];
                toast(this.success, 'success');
            } else {
                this.error = result['error'];
                this.errors = result['message'];
                this.loading = false;
                this.edit_address = true;
                unblockUI();
                toast(this.errors, 'error');
            }
        });
    }
    
    showEdit(address_id){
        this.editAddressmodel = this.addressData.filter(address1 => address1.id == address_id)[0];
        this.editAddressmodel.edit_first_name = this.editAddressmodel.first_name;
        this.editAddressmodel.edit_last_name = this.editAddressmodel.last_name;
        this.editAddressmodel.edit_address_1 = this.editAddressmodel.address_1;
        this.editAddressmodel.edit_address_2 = this.editAddressmodel.address_2;
        this.editAddressmodel.edit_mobile = this.editAddressmodel.mobile;
        this.editAddressmodel.edit_pin_code = this.editAddressmodel.pin_code;
        this.editAddressmodel.edit_city = this.editAddressmodel.city;
        this.editAddressmodel.edit_state = this.editAddressmodel.state;
        jQuery('#editAddress').find('label').addClass('active');
    }
    
    close_address_form(mode) {
        this.open_address = !mode;
        this.edit_address = !mode;
        jQuery('#userAddress')[0].reset();
        jQuery('#editAddress')[0].reset();
        jQuery('#userAddress').find('label').removeClass('active');
        jQuery('#editAddress').find('label').removeClass('active');
        this.userService.getUserAddress(this.authDatas.id, this.authDatas.token).subscribe(
            addressDetail => {
                this.addressData = addressDetail['data'];
            }, err => {
                console.log(err);
            }
        );
    }
    
    setAddress(address){
        this.checkout.selected_address = address;
        this.checkout.order_data['address'] = address;
        this.checkout.cod_charge = 0;
        this.checkout.second_step = true;
        
        this.router.navigate(['./checkout', 'summary']);
    }
      getdetail(modelName,pincode){
        this.userService.getPincodeDetail(pincode)
        .subscribe(result => {
            if (result['status_code'] === 1) {
                if(modelName=='addressmodel'){
                    this.addressmodel['state'] = result['data']['state'];
                    this.addressmodel['city'] = result['data']['city'];
                    jQuery('#userAddress').find('input').focus();;
                }else{
                    this.editAddressmodel['edit_state'] = result['data']['state'];
                    this.editAddressmodel['edit_city'] = result['data']['city'];
                    jQuery('#editAddress').find('input').focus();;
              }
            } 
        });
    }

}
