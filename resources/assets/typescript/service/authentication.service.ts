import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    public authData:  any = [];
    private loggedIn = false;

    constructor(private http: Http) {
        // set token if saved in local storage
        this.authData = JSON.parse(localStorage.getItem('loginUser'));
        
    }

    login(email: string, password: string): Observable<boolean> {
        return this.http.post('api/login', { email: email, password: password })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let auth_data = JSON.stringify(response.json().user_data);
                if (auth_data) {
                    // set token property
                    

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('loginUser', auth_data);
                    localStorage.setItem('loginUser_id', auth_data['id']);
                    localStorage.setItem('loginUser_token', auth_data['token']);

                    this.setAuthData();
                    // return true to indicate successful login
                    return response.json();
                } else {
                    // return false to indicate failed login
                    return response.json();
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        localStorage.removeItem('loginUser');
        localStorage.removeItem('loginUser');
        localStorage.removeItem('loginUser');
    }

    isLoggedIn() {
        if(JSON.parse(localStorage.getItem('loginUser'))){
            return JSON.parse(localStorage.getItem('loginUser'));
        }
    }
    
    socialLogin(user_data: any) {
        let auth_data = JSON.stringify(user_data);
        if (auth_data) {
            // set token property
            this.authData = auth_data;

            // store username and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('loginUser', auth_data);
            this.setAuthData()
            // return true to indicate successful login
            return true;
        } else {
            // return false to indicate failed login
            return false;
        }
    }
    
    signup(modelData: any): Observable<boolean> {
        return this.http.post('api/sign-up', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    mobileNumberVerified(modelData: any): Observable<boolean> {
        return this.http.post('api/mobile-verify', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    mobileNumberSaved(modelData: any): Observable<boolean> {
        return this.http.post('api/mobile-number-update', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    socialLoginSaveData(modelData: any,login_type:string): Observable<boolean> {
        return this.http.post('api/social-signup/'+login_type, { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    forgotPassword(modelData: any): Observable<boolean> {
        return this.http.post('api/forgot-password', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }    
    resendOtpPassword(user_id: any): Observable<boolean> {
        return this.http.get('api/resend-otp-password/'+user_id)
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    setAuthData(){

         this.authData = JSON.parse(localStorage.getItem('loginUser'));
    }    

    setMobileNumberONAuth(mobile_number: any){

            this.authData = JSON.parse(localStorage.getItem('loginUser'));

             this.authData.phone =mobile_number;
                
                    // set token property
                    

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('loginUser', JSON.stringify(this.authData));
                    this.setAuthData();
    }
     getUserName(): string {
        
        return this.authData['first_name'] + " " + this.authData['last_name'];
    }
}
