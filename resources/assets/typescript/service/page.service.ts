import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions,RequestOptionsArgs } from '@angular/http';
import { Page } from '../model/page';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PageService {
    constructor(private http:Http) {}
    
    private apiUrl = 'api/get-all-cms';
    getPages() : Observable<Page[]> {


        var headers = new Headers(); 
        headers.append('Content-Type', 'application/json');
        headers.append('X-Requested-With', 'XMLHttpRequest');

        // ...using get request
        return this.http.get(this.apiUrl,{headers:headers})
                        // ...and calling .json() on the response to return data
                        .map((res:Response) => res.json())
                        //...errors if any
                        .catch(this.handleServerError);
    }
     
    private pageUrl = 'api/get-cms-page-detail/';
    getPageDetail(slug: string) : Observable<Page[]> {
       // ...using get request
       return this.http.get(`${this.pageUrl}${slug}`)
                       // ...and calling .json() on the response to return data
                       .map((res:Response) => res.json())
                       //...errors if any
                       .catch(this.handleServerError);
    }
    
    private menuUrl = 'api/get-all-category-for-menu';
    getMenus() : Observable<Page[]> {
        return this.http.get(this.menuUrl).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    
    private settingsUrl = 'api/get-all-settings';
    getSettings() : Observable<Page[]> {
        return this.http.get(this.settingsUrl).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    
    private sliderUrl = 'api/get-slider';
    getSliders() : Observable<Page[]> {
        return this.http.get(this.sliderUrl).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    private blockUrl = 'api/get-all-block';
    getBlocks() : Observable<Page[]> {
        return this.http.get(this.blockUrl).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    
    private faqUrl = 'api/get-faq';
    getFaq() : Observable<Page[]> {
        return this.http.get(this.faqUrl).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    
    contactUs(modelData: any): Observable<boolean> {
        return this.http.post('api/contact-us', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }
    
    private check_token_url = 'api/check-socail-login/';
    checkSocailLogin(token: string): Observable<Page[]> {
        // ...using get request
        return this.http.get(`${this.check_token_url}${token}`)
            // ...and calling .json() on the response to return data
            .map((res: Response) => res.json())
            //...errors if any
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getOrder(uId:string ,page: number): Observable<Page[]> {
        let url = 'api/get-order/' + uId + '?page=' + page;
        return this.http.get(url).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    
    getOrderDetail(uId:string ,order_id: string): Observable<Page[]> {
        let url = 'api/get-order-detail/' + uId + '/' + order_id;
        return this.http.get(url).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
      cancelOrder(uId:string ,order_id: string,modelData: any): Observable<Page[]> {
        let url = 'api/order-cancel/' + order_id + '/' + uId;
        return this.http.post(url, { data: modelData }).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    
    getWallet(uId:string,page: number): Observable<Page[]> {
        let url = 'api/get-wallet/' + uId+ '?page=' + page;
        return this.http.get(url).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    checkMobileNumberVerified(user_id: string): Observable<Page[]> {
        
          return this.http.get('api/check-address-number/'+user_id).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    resendOtpCodNumber(user_id: string): Observable<Page[]> {
        
          return this.http.get('api/resend-otp-cod-mobile-number/'+user_id).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
    mobileNumberVerified(data:any,user_id: string): Observable<Page[]> {
        
          return this.http.post('api/cod-mobile-verify/'+user_id, { data: data }).map((res:Response) => res.json())
                        .catch(this.handleServerError);
    }
     private handleServerError(error: Response) {
        return Observable.throw(error|| 'Server error'); // Observable.throw() is undefined at runtime using Webpack
    }
    newslatterSubscription(modelData: any): Observable<boolean> {
        return this.http.post('api/newslatter', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

}
