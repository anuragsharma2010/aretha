import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PageService } from "../service/page.service";
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { blockUI, unblockUI, toast } from '../global';
import { Location } from '@angular/common';
import { SocialUser } from '../model/socialuser';
import { CartService } from "../service/cart.service";
declare var jQuery: any;
declare var Materialize: any;

declare var FB: any;
declare var gapi: any;

@Component({
    selector: 'my-header',
    template: require('./header.html')
})

export class HeaderComponent implements OnInit, AfterViewInit {
    menus: any = [];
    userData: any = [];
    model: any = {};
    loading = false;
    error : any = [];
    sign_up_error : any = [];
    success = '';
    authData: any = [];
    signupmodel: any = {};
    forgotpasswordmodel: any = {};
    number_model: any = {};
    mobile_number = '';
    PhoneNumberModel: any = {};
    user_id = '';
    emailId = '';
    token: any;
    search_text: string;
    loged: boolean = false;
    user = { name: 'Hello' };
    auth2: any;
    settings: any = [];
    cartItem: any = [];
    FacebookAppId = '';
    GoogleAppId = '';
    constructor(
        private pageService: PageService,
        private router: Router,
        private authenticationService: AuthenticationService,
        private location: Location,
        private cartService: CartService
    ) {

    }

    ngOnInit() {
        //this.authData = JSON.parse(localStorage.getItem('loginUser'));
        this.authData = this.authenticationService.authData;
       

        this.pageService.getMenus().subscribe(
            menuData => {
                console.log(menuData['data'][0]['parent']);
                this.menus = menuData['data'][0]['parent'];
                this.settings = menuData['settings'];

//                this.FacebookAppId = menuData['settings']['CONFIG_FACEBOOK_CLIENT_ID'];
//                this.GoogleAppId = menuData['settings']['CONFIG_GOOGLE_CLIENT_ID'];
                let _self = this;
                FB.init({
                    appId: _self.FacebookAppId,
                    cookie: false,  // enable cookies to allow the server to access
                    // the session
                    xfbml: true,  // parse social plugins on this page
                    version: 'v2.5' // use graph api version 2.5
                });
             
//                setTimeout(() => {
//                    jQuery(document).ready(function() {
//                        jQuery('.dropdown-button').dropdown({
//                            inDuration: 300,
//                            outDuration: 225,
//                            constrain_width: false, // Does not change width of dropdown to that of the activator
//                            hover: true, // Activate on hover
//                            gutter: 0, // Spacing from edge
//                            belowOrigin: false, // Displays dropdown below the button
//                            alignment: 'left' // Displays dropdown with edge aligned to the left of button
//                        });
//                         jQuery('.collapsible').collapsible();
//                        jQuery("#nav-mobile .child-menu ul li a").click(function() {
//                            jQuery('.button-collapse').sideNav('hide');
//                        });
//                        jQuery("#modal2").mCustomScrollbar({
//                            autoDraggerLength: false,
//                            mouseWheel: {
//                                scrollAmount: 128
//                            }
//                        });
//                    });
//                    this.googleInit();
//                }, 2000);
            });


            if (localStorage.getItem('remember_me') == '1') {
                this.model.loginEmail = localStorage.getItem('loginEmail');
                this.model.remember_me = localStorage.getItem('remember_me');
                this.model.loginPassword = localStorage.getItem('loginPassword');
                setTimeout(() => {
                    jQuery(document).ready(function () {
                        Materialize.updateTextFields();
                    });
                }, 100);
            }
    }

    signin() {
        blockUI();
        this.loading = true;
        this.authenticationService.login(this.model.loginEmail, this.model.loginPassword)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.error = [];
                    this.authData = JSON.parse(localStorage.getItem('loginUser'));


                      
                    if(this.model.remember_me){
                        
                          localStorage.setItem('loginEmail',this.model.loginEmail);
                          localStorage.setItem('loginPassword',this.model.loginPassword);
                          localStorage.setItem('remember_me','1');
                          


                    }else{
                        
                            localStorage.removeItem('remember_me');
                            localStorage.removeItem('loginEmail');
                            localStorage.removeItem('loginPassword');

                    }

                     



                    jQuery('#modal1').modal('close');
                    jQuery('#loginForm')[0].reset();
                    jQuery('#loginForm').find('label').removeClass('active');
                    this.getCartItem();
                    unblockUI();
                    this.loading = false;

                   

                    
                    toast(this.success, 'success');
                    setTimeout(() => {
                        jQuery('.dropdown-button').dropdown({
                            inDuration: 300,
                            outDuration: 225,
                            constrain_width: false, // Does not change width of dropdown to that of the activator
                            hover: true, // Activate on hover
                            gutter: 0, // Spacing from edge
                            belowOrigin: false, // Displays dropdown below the button
                            alignment: 'left' // Displays dropdown with edge aligned to the left of button
                        });

                        jQuery("#nav-mobile .child-menu ul li a").click(function() {
                            jQuery('.button-collapse').sideNav('hide');
                        });

                    }, 2000);
                } else {
                    this.error = result['message'];
                    this.loading = false;
                    unblockUI();
                    toast(this.error, 'error');
                }
            });
    }

    logout() {
        blockUI();
        this.authenticationService.logout();
        this.authData = null;
        unblockUI();
        toast("Logout successfully", 'success');
        this.cartService.cart = [];
        this.cartItem = [];
        this.cartService.wishlist = [];
        this.cartService.wishlistArray= [];
        this.router.navigate(['/']);
        setTimeout(() => {
            jQuery("#nav-mobile .child-menu ul li a").click(function() {
                jQuery('.button-collapse').sideNav('hide');
            });
        }, 2000);
        if(localStorage.getItem('remember_me') == '1'){
                    this.model.loginEmail = localStorage.getItem('loginEmail');
                   this.model.remember_me = localStorage.getItem('remember_me');
                   this.model.loginPassword = localStorage.getItem('loginPassword');
                       setTimeout(() => {
                                jQuery(document).ready(function() {
      Materialize.updateTextFields();
    });
                }, 1000);

        }
    }

    isLoggedIn() {
        return this.authenticationService.isLoggedIn();
    }

    getUserName(): string {
        let userData = this.authenticationService.authData;
        return userData['first_name'] + " " + userData['last_name'];
    }

    getCartItem() {
        if (this.authData && this.authData.id) {
            this.cartService.getCart(this.authData.id).subscribe(
                cartData => {
                    this.cartItem = cartData['data'];
                }, err => {
                    console.log(err);
                }
            );


            this.cartService.getToWishlist(this.authData.id);
        }
    }

    getWishlistItem() {
        if (this.authData && this.authData.id) {

            this.cartService.getWishlist(this.authData.id).subscribe(
                wishData => {
                    this.cartService.wishlist = wishData['data'];
                }, err => {
                    console.log(err);
                }
            );

        }
    }

    usersignup() {
        
        blockUI();
        this.loading = true;
        this.authenticationService.signup(this.signupmodel)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.signupmodel = '';
                    this.sign_up_error = [];
                    jQuery('#modal2').modal('close');
                    jQuery('#signupform')[0].reset();
                    unblockUI();
                    this.loading = false;
                    this.router.navigate(['/']);
                    toast(this.success, 'success');
                    if (result['phone_verify'] == 1) {
                        this.mobile_number = result['mobile_number'];
                        this.user_id = result['user_id'];
                        jQuery('#modal_otp').modal('open');
                    }
                    else { }
                }
                else {
                    this.sign_up_error = result['errors'];
                    this.loading = false;
                    unblockUI();
                    toast(result['message'], 'error');
                }
            });
     
    }

    forgot_passowrd() {
        blockUI();
        this.loading = true;
        this.authenticationService.forgotPassword(this.forgotpasswordmodel)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.forgotpasswordmodel = {};
                    this.error = [];
                    jQuery('#modal3').modal('close');
                    jQuery('#forgotPassword')[0].reset();
                    unblockUI();
                    this.loading = false;
                    this.router.navigate(['/']);
                    toast(this.success, 'success');
                } else {
                    this.error = result['message'];
                    this.loading = false;
                    unblockUI();
                    toast(this.error, 'error');
                }
            });
    }

    close_model(id) {
        jQuery('#' + id)[0].reset();
        jQuery('#' + id).find('label').removeClass('active');
        this.error = [];
    }

    submitOtp() {
        blockUI();
        this.loading = true;
        this.number_model['user'] = this.user_id;
        this.authenticationService.mobileNumberVerified(this.number_model)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.number_model = '';
                    this.error = [];
                    jQuery('#modal_otp').modal('close');
                    jQuery('#verified_number')[0].reset();
                    unblockUI();
                    this.loading = false;
                    this.location.go('/');
                    this.router.navigate(['/']);
                    toast(this.success, 'success');
                }
                else {
                    this.error = result['message'];
                    this.loading = false;
                    unblockUI();
                    toast(this.error, 'error');
                }
            });

    }

    submitPhoneNumber() {
        blockUI();
        this.loading = true;
        this.PhoneNumberModel['user'] = this.user_id;
        this.authenticationService.mobileNumberSaved(this.PhoneNumberModel)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.PhoneNumberModel = '';
                    this.error = [];
                    jQuery('#PhoneModalBox').modal('close');
                    jQuery('#PhoneNumberForm')[0].reset();
                    if (result['phone_verify'] == 1) {
                        this.mobile_number = result['mobile_number'];
                        jQuery('#modal_otp').modal('open');
                    }

                    this.authenticationService.setMobileNumberONAuth(result['mobile_number']);
                    unblockUI();
                    this.loading = false;
                    toast(this.success, 'success');
                }
                else {
                    this.error = result['error'];
                    this.loading = false;
                    unblockUI();
                    toast(result['message'], 'error');
                }
            });
    }

    resendOtp() {
        blockUI();
        this.authenticationService.resendOtpPassword(this.user_id)
            .subscribe(result => {
                if (result['status_code'] == 1) {
                    unblockUI();
                    toast(result['message'], 'success');
                }
                else {
                    unblockUI();
                    toast(result['message'], 'error');
                    this.router.navigate(['/']);
                }
            });
    }

    fblogin() {
        var _self = this;
        blockUI();
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                if (response.authResponse) {

                    FB.api('/me?fields=id,email,name,first_name,last_name,gender', function(result) {
                        if (result && !result.error) {
                            _self.user = result;
                            _self.saveSocailData(result, 'facebook');
                        }
                        else {
                            console.log(result.error);
                        }
                    });
                    unblockUI();
                }
                else {
                    console.log('User cancelled login or did not fully authorize.');
                    unblockUI();
                }

            } else {



                FB.login(function(response) {
                    if (response.authResponse) {

                        FB.api('/me?fields=id,email,name,first_name,last_name,gender', function(result) {
                            if (result && !result.error) {
                                _self.user = result;
                                _self.saveSocailData(result, 'facebook');
                            }
                            else {
                                console.log(result.error);
                            }
                        });
                        unblockUI();
                    }
                    else {
                        console.log('User cancelled login or did not fully authorize.');
                        unblockUI();
                    }
                });
            }
        });
}

    saveSocailData(data: any, login_type: string) {
               blockUI();
        this.authenticationService.socialLoginSaveData(data, login_type)
            .subscribe(response => {
                if (response['status_code'] == 1) {
                    let result = this.authenticationService.socialLogin(response['user_data']);
                    if (result) {
                        
                        this.error = [];
                        this.authData = this.authenticationService.authData;
                        this.emailId = response['user_data']['email'];
                        this.user_id = this.authData.id;
                           this.getCartItem();
                        jQuery('#modal1').modal('close');
                        jQuery('#loginForm')[0].reset();
                        unblockUI();
                        setTimeout(() => {
                            jQuery('.dropdown-button').dropdown({
                                inDuration: 300,
                                outDuration: 225,
                                constrain_width: false, // Does not change width of dropdown to that of the activator
                                hover: true, // Activate on hover
                                gutter: 0, // Spacing from edge
                                belowOrigin: false, // Displays dropdown below the button
                                alignment: 'left' // Displays dropdown with edge aligned to the left of button
                            });
                        }, 2000);
                        // this.router.navigate(['/']);
                        toast(response['message'], 'success');
                        if (response['user_data']['phone'] == '' && response['user_data']['phone_verify'] == 0) {
                            jQuery('#PhoneModalBox').modal('open');
                        }
                        unblockUI();
                    }
                    else {
                        //this.router.navigate(['/']);
                        toast(response['message'], 'error');
                        unblockUI();
                    }
                }
                else {
                    this.router.navigate(['/']);
                    toast(response['message'], 'error');
                     unblockUI();
                }
            });
    }

    googleInit() {
        

        let that = this;
        gapi.load('auth2', function() {
            that.auth2 = gapi.auth2.init({
                client_id: that.GoogleAppId,
                cookiepolicy: 'single_host_origin',
                scope: 'profile email'
            });
            that.attachSignin(document.getElementById('googleBtn'));
        });
    }

    attachSignin(element) {
        let _self = this;
        this.auth2.attachClickHandler(element, {},
            function(googleUser) {
                let profile = googleUser.getBasicProfile();
                let userData = new SocialUser(profile.getId(), profile.getName(), profile.getGivenName(), profile.getFamilyName(), profile.getEmail());
                _self.saveSocailData(userData, 'google');
            }, function(error) {


                                 
            });
    }

    ngAfterViewInit() {
        //  this.googleInit();
           
    }


    isDisabled(path) {
        return this.location.path().indexOf(path) > -1;
    }
    Search(search_text){
        if(search_text!=''){

            this.router.navigate(['search',search_text]);    
        }
        
    }
    redirectback(){
        
            this.location.back();

    }

}