<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    


     <?php 
        $rote_name =  Request::route()->getName();
     ?>

    
       <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
   @foreach ($admin_menus as $admin_menu)
                      
        @if(empty($admin_menu['child_list']))


            <li class=" treeview  {{($rote_name==$admin_menu['route'])?'active':''}}">
        <a href="{{ ($admin_menu['route']!='')?route($admin_menu['route']):'#' }}">
          <i class="fa {{ $admin_menu['icon'] }}"></i> <span>{{ $admin_menu['name'] }}</span>
        </a>
      </li>
@else
  <?php 
        $active_class = '';
         foreach ($admin_menu['child_list'] as $child_list){
            if($rote_name==$child_list['route']){
                $active_class = 'active';
                break;
            }

         }

  ?>
  

      <li class="treeview {{$active_class}}">
        <a href="#">
          <i class="fa {{ $admin_menu['icon'] }}"></i>
         <span>{{ $admin_menu['name'] }}</span>
          <i class="fa fa-angle-left pull-right"></i>
              <ul class="treeview-menu">
                  
                   @foreach ($admin_menu['child_list'] as $child_list)
          
  <li class=" {{($rote_name==$child_list['route'])?'active':''}} "><a href="{{ ($child_list['route']!='')?route($child_list['route']):'#' }}"">
            <i class="fa {{ $child_list['icon'] }}"></i>{{ $child_list['name'] }} </a></li>
                    @endforeach 


    
                
            </ul>
        </a>
      </li>                                
          
@endif

      @endforeach 
     </ul>
  </section>
  <!-- /.sidebar -->
</aside>
