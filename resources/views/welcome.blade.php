<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">

        <title>{{Configure('CONFIG_SITE_TITLE')}}</title>

        <meta name="title" content="{{Configure('CONFIG_META_TITLE')}}">
        <meta name="description" content="{{Configure('CONFIG_META_DESCRIPTION')}}">
        <meta name="keywords" content="{{Configure('CONFIG_META_KEYWORDS')}}">
        <meta property="og:url" content="{{WEBSITE_URL}}" />
        <meta property="og:title" content="{{Configure('CONFIG_SITE_TITLE')}}" />
        <meta property="og:description" content="{{Configure('CONFIG_META_DESCRIPTION')}}" />
        <meta property="og:image" content="" />
        <meta property="n2s:via" content="{{Configure('CONFIG_SITE_TITLE')}}" />
        <meta property="n2s:hashtags" content="{{Configure('CONFIG_SITE_TITLE')}}"/>
        <base href="{{WEBSITE_URL}}" />
        <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">

        {!! Html::style('https://fonts.googleapis.com/icon?family=Material+Icons') !!}

        @if (App::environment('production'))
            {!! Html::style( WEBSITE_PUBLIC_URL.elixir('css/all.css')) !!}
        @else
            {!! Html::style( WEBSITE_PUBLIC_URL.elixir('css/materialize.min.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/style.css')) !!} 
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/new-custom.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/style-responsive.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/font-awesome.min.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/flexslider.css')) !!}

            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/owl.carousel.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/owl.theme.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/slick.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/slick-theme.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/jquery.mCustomScrollbar.css')) !!}
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/tracker.css')) !!} 
            {!! Html::style(WEBSITE_PUBLIC_URL.elixir('css/custom.css')) !!}
        @endif

        <script>
            function resizeequalheight() {
                equalHeight($(".makeequal"));
            }
        </script>
    </head>
    
    <body>
        <my-app>
            <div class="loading-cntant" style="text-align: center;cursor:wait;">
                <div class="preloader-wrapper small active" style=" margin: auto; position: absolute; vertical-align: middle;  top: 0;    right: 0;    bottom: 0;  left: 0;">
                    <div class="spinner-layer spinner-blue-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </my-app>
    
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
        
        @if (App::environment('production'))
            <script src="{{WEBSITE_URL}}public{{ elixir('js/all.js') }}"></script>
        @else
            <script src="{{WEBSITE_URL}}public{{ elixir('js/sdk.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/jquery.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/jquery.easing.1.3.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/owl.carousel.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/materialize.min.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/jquery.blockUI.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/slick.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/jquery.sticky.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/vendor.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/init.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/jquery.flexslider.js') }}"></script> 
            <script src="{{WEBSITE_URL}}public{{ elixir('js/app.js') }}"></script>
            <script src="{{WEBSITE_URL}}public{{ elixir('js/index.js') }}"></script>
        @endif

        <script>
                $(window).resize(function () {
                    setTimeout('resizeequalheight()', 250);
                });
                
                $(function () {
                    SyntaxHighlighter.all();
                });

                function equalHeight(group) {
                    tallest = 0;
                    group.height('');
                    group.each(function () {
                        thisHeight = $(this).height();
                        if (thisHeight > tallest) {
                            tallest = thisHeight;
                        }
                    });
                    group.height(tallest);
                }
                setTimeout('resizeequalheight()', 250);
        </script>
    </body>
</html>