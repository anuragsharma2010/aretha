<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $pageTitle; ?>
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php //  {!! Form::model($setting,['method'=>'post','route'=>'admin.settings.store','class'=>'form-horizontal']) !!}   ?>
                {!!Form::model($setting, ['route' => ['admin.settings.update', $setting->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH',  'files' => true  , 'id' => 'edit-settings']) !!}
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#global_settings" data-toggle="tab">{{ trans('admin.GLOBAL_SETTINGS') }}</a></li>
                        <li class=""><a href="#social_settings" data-toggle="tab">{{ trans('admin.SOCIAL_SETTINGS') }}</a></li>
                        <li class=""><a href="#contact_detail" data-toggle="tab">{{ trans('admin.CONTACT_DETAIL') }}</a></li>
                        <li class=""><a href="#smtp_mail_settings" data-toggle="tab">{{ trans('admin.SMTP_MAIL_SETTINGS') }}</a></li>
                        <li class=""><a href="#payment" data-toggle="tab">{{ trans('admin.PAYMENT') }}</a></li>
                        <li class=""><a href="#delhivery" data-toggle="tab">{{ trans('admin.DELHIVERY') }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Start Global Settings -->
                        <div class="tab-pane active" id="global_settings">


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.SITE_TITLE'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('site_title',null,['class'=>'form-control','placeholder'=>trans('admin.SITE_TITLE')]) !!}

                                            <div class="error">{{ $errors->first('site_title') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.ADMIN_PAGE_LIMIT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('page_limit',null,['class'=>'form-control','placeholder'=>trans('admin.ADMIN_PAGE_LIMIT')]) !!}

                                            <div class="error">{{ $errors->first('page_limit') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.row -->


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.FRONT_PAGE_LIMIT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('front_page_limit',null,['class'=>'form-control','placeholder'=>trans('admin.FRONT_PAGE_LIMIT')]) !!}

                                            <div class="error">{{ $errors->first('front_page_limit') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.STAFF_MAIL'),null,['class'=>'col-sm-4 control-label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('staff_mail',null,['class'=>'form-control','placeholder'=>trans('admin.STAFF_MAIL')]) !!}

                                            <div class="error">{{ $errors->first('staff_mail') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                </div>
                            </div><!-- /.col -->


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.FROM_NAME'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('from_name',null,['class'=>'form-control','placeholder'=>trans('admin.FROM_NAME')]) !!}

                                            <div class="error">{{ $errors->first('from_name') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.REPLY_TO_EMAIL'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('reply_to_email',null,['class'=>'form-control','placeholder'=>trans('admin.REPLY_TO_EMAIL')]) !!}

                                            <div class="error">{{ $errors->first('reply_to_email') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.col -->

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.FROMEMAIL'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('fromemail',null,['class'=>'form-control','placeholder'=>trans('admin.FROMEMAIL')]) !!}

                                            <div class="error">{{ $errors->first('fromemail') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.COPYRIGHT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('copyright',null,['class'=>'form-control','placeholder'=>trans('admin.COPYRIGHT')]) !!}

                                            <div class="error">{{ $errors->first('copyright') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->


                                </div>
                            </div><!-- /.col -->

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.META_TITLE'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('meta_title',null,['class'=>'form-control','placeholder'=>trans('admin.META_TITLE')]) !!}

                                            <div class="error">{{ $errors->first('meta_title') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.META_KEYWORDS'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('meta_keywords',null,['class'=>'form-control','placeholder'=>trans('admin.META_KEYWORDS')]) !!}

                                            <div class="error">{{ $errors->first('meta_keywords') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.col -->
                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.META_DESCRIPTION'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::textarea('meta_description',null,['class'=>'form-control','placeholder'=>trans('admin.META_DESCRIPTION')]) !!}

                                            <div class="error">{{ $errors->first('meta_description') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.EMAIL_SIGNATURE'),null,['class'=>'col-sm-4 control-label required_label    ']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::textarea('email_signature',null,['class'=>'form-control','placeholder'=>trans('admin.EMAIL_SIGNATURE')]) !!}

                                            <div class="error">{{ $errors->first('email_signature') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                </div>
                            </div><!-- /.col -->       <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.RELATED_PRODUCT_LIMIT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('related_product_limit',null,['class'=>'form-control','placeholder'=>trans('admin.RELATED_PRODUCT_LIMIT')]) !!}

                                            <div class="error">{{ $errors->first('related_product_limit') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->


                                </div>

                            </div><!-- /.col -->
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group col-md-12 ">
                                        {!! Form::label(trans('admin.RETURN_AND_DELIVERY'),null,['class'=>'col-sm-2 control-label required_label']) !!}
                                        <div class='col-sm-10'>
                                            {!! Form::textarea('return_and_delivery',null,['class'=>'form-control ckeditor','placeholder'=>trans('admin.RETURN_AND_DELIVERY')]) !!}

                                            <div class="error">{{ $errors->first('return_and_delivery') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->


                                </div>
                            </div><!-- /.row -->

                        </div><!-- /.row -->


                        <!-- END Global Settings -->
                        <!-- Start Socail Settings -->
                        <div class="tab-pane " id="social_settings">


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.FACEBOOK_LINK'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('facebook_link',null,['class'=>'form-control','placeholder'=>trans('admin.FACEBOOK_LINK')]) !!}

                                            <div class="error">{{ $errors->first('facebook_link') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.TWITTER_LINK'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('twitter_link',null,['class'=>'form-control','placeholder'=>trans('admin.TWITTER_LINK')]) !!}

                                            <div class="error">{{ $errors->first('twitter_link') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.row --> 

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.GOOGLE_PLUS_LINK'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('google_plus_link',null,['class'=>'form-control','placeholder'=>trans('admin.GOOGLE_PLUS_LINK')]) !!}

                                            <div class="error">{{ $errors->first('google_plus_link') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.PINTEREST_LINK'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('pinterest_link',null,['class'=>'form-control','placeholder'=>trans('admin.PINTEREST_LINK')]) !!}

                                            <div class="error">{{ $errors->first('pinterest_link') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.row -->   

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.ANDROID_APP_LINK'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('android_app_link',null,['class'=>'form-control','placeholder'=>trans('admin.ANDROID_APP_LINK')]) !!}

                                            <div class="error">{{ $errors->first('android_app_link') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.IOS_APP_LINK'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('ios_app_link',null,['class'=>'form-control','placeholder'=>trans('admin.IOS_APP_LINK')]) !!}

                                            <div class="error">{{ $errors->first('ios_app_link') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.row -->    


                            <h2>Socail Login</h2>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.FACEBOOK_CLIENT_ID'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('facebook_client_id',null,['class'=>'form-control','placeholder'=>trans('admin.FACEBOOK_CLIENT_ID')]) !!}

                                            <div class="error">{{ $errors->first('facebook_client_id') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.FACEBOOK_CLIENT_SECRET'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('facebook_client_secret',null,['class'=>'form-control','placeholder'=>trans('admin.FACEBOOK_CLIENT_SECRET')]) !!}

                                            <div class="error">{{ $errors->first('facebook_client_secret') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.row -->
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.GOOGLE_CLIENT_ID'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('google_client_id',null,['class'=>'form-control','placeholder'=>trans('admin.GOOGLE_CLIENT_ID')]) !!}

                                            <div class="error">{{ $errors->first('google_client_id') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.GOOGLE_CLIENT_SECRET'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('google_client_secret',null,['class'=>'form-control','placeholder'=>trans('admin.GOOGLE_CLIENT_SECRET')]) !!}

                                            <div class="error">{{ $errors->first('google_client_secret') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.row -->
                        </div><!-- /.end socail settinf -->

                        <!-- Start contact detail Settings -->
                        <div class="tab-pane " id="contact_detail">


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.PHONE_NUMBER'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('phone_number',null,['class'=>'form-control','placeholder'=>trans('admin.PHONE_NUMBER')]) !!}

                                            <div class="error">{{ $errors->first('phone_number') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        {!! Form::label(trans('admin.ADDRESS'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('address',null,['class'=>'form-control','placeholder'=>trans('admin.ADDRESS')]) !!}

                                            <div class="error">{{ $errors->first('address') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.row --> 
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.SUPPORT_MAIL'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('support_mail',null,['class'=>'form-control','placeholder'=>trans('admin.SUPPORT_MAIL')]) !!}

                                            <div class="error">{{ $errors->first('support_mail') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->


                                </div>
                            </div><!-- /.row --> 
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.GOOGLE_MAP'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::textarea('google_map',null,['class'=>'form-control','placeholder'=>trans('admin.GOOGLE_MAP')]) !!}

                                        <div class="error">{{ $errors->first('google_map') }}</div>
                                        <div>Please Use embed iframe </div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 



                        </div><!-- /.end socail settinf -->







                        <!-- /.start smtp setting -->
                        <div  class="tab-pane " id="smtp_mail_settings" >

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.MAIL_DRIVER'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            <div class="form-radio"> 

                                                <label> {!! Form::radio('mail_driver',Config::get('global.mail_driver.smtp'),1, array('class' => 'minimal mail_driver '))!!} &nbsp; {{trans('admin.SMTP')}}</label>
                                                <label> {!! Form::radio('mail_driver',Config::get('global.mail_driver.mail'),0, array('class' => 'minimal mail_driver' )) !!}  &nbsp;  {{trans('admin.MAIL')}}</label>
                                            </div>

                                            <div class="error">{{ $errors->first('mail_driver') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                </div>
                            </div><!-- /.row --> 





                            <div class="row">


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MAIL_HOST'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('mail_host',null,['class'=>'form-control','placeholder'=>trans('admin.MAIL_HOST')]) !!}

                                        <div class="error">{{ $errors->first('mail_host') }}</div>
                                    </div>
                                </div><!-- /.form-group -->  

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MAIL_PORT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('mail_port',null,['class'=>'form-control','placeholder'=>trans('admin.MAIL_PORT')]) !!}

                                        <div class="error">{{ $errors->first('mail_port') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 
                            <div class="row">


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MAIL_USERNAME'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('mail_username',null,['class'=>'form-control','placeholder'=>trans('admin.MAIL_USERNAME')]) !!}

                                        <div class="error">{{ $errors->first('mail_username') }}</div>
                                    </div>
                                </div><!-- /.form-group -->  

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MAIL_PASSWORD'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('mail_password',null,['class'=>'form-control','placeholder'=>trans('admin.MAIL_PASSWORD')]) !!}

                                        <div class="error">{{ $errors->first('mail_password') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 
                            <h1> SMS Settings</h1>

                            <div class="row">


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.ENABLE_SMS'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>

                                        {!! Form::checkbox('enable_sms',1,null, array('class' => 'minimal enable_sms '))!!}
                                        <div class="error">{{ $errors->first('enable_sms') }}</div>
                                    </div>
                                </div><!-- /.form-group -->  

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.SENDER_ID'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('sender_id',null,['class'=>'form-control','placeholder'=>trans('admin.SENDER_ID')]) !!}

                                        <div class="error">{{ $errors->first('sender_id') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 
                            <div class="row">


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.SMS_PASSWORD'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('sms_password',null,['class'=>'form-control','placeholder'=>trans('admin.SMS_PASSWORD')]) !!}

                                        <div class="error">{{ $errors->first('sms_password') }}</div>
                                    </div>
                                </div><!-- /.form-group -->  

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.SMS_USER_NAME'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('sms_user_name',null,['class'=>'form-control','placeholder'=>trans('admin.SMS_USER_NAME')]) !!}

                                        <div class="error">{{ $errors->first('sms_user_name') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 




                        </div><!-- /.End smtp setting -->


                        <!-- /.start payment setting -->

                        <div  class="tab-pane " id="payment" >
                            <h1>  Cashback  for online payment</h1>

                            <div class="row">


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.ENABLE_CASHBACK'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>

                                        {!! Form::checkbox('enable_cashback',1,null, array('class' => 'minimal enable_cashback '))!!}
                                        <div class="error">{{ $errors->first('enable_cashback') }}</div>
                                    </div>
                                </div><!-- /.form-group -->  

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.COUPON_CODE'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('coupon_code',null,['class'=>'form-control','placeholder'=>trans('admin.COUPON_CODE')]) !!}

                                        <div class="error">{{ $errors->first('coupon_code') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->     <div class="row">



                                <div class="form-group col-md-12 ">
                                    {!! Form::label(trans('admin.COUPON_DESCRIPTION'),null,['class'=>'col-sm-2 control-label required_label']) !!}
                                    <div class='col-sm-10'>
                                        {!! Form::textarea('coupon_description',null,['class'=>'ckeditor form-control','placeholder'=>trans('admin.COUPON_DESCRIPTION')]) !!}

                                        <div class="error">{{ $errors->first('coupon_description') }}</div>
                                    </div>
                                </div><!-- /.form-group -->




                            </div><!-- /.row --> 
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MIN_BILL_AMOUNT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa  {{Config::get('global.currency_icon')}}"></i>   </div>
                                            {!! Form::text('min_bill_amount',null,['class'=>'form-control','placeholder'=>trans('admin.MIN_BILL_AMOUNT')]) !!}
                                        </div>

                                        <div class="error">{{ $errors->first('min_bill_amount') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MAX_CASHBACK_AMOUNT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa  {{Config::get('global.currency_icon')}}"></i>   
                                            </div>   {!! Form::text('max_cashback_amount',null,['class'=>'form-control','placeholder'=>trans('admin.MAX_CASHBACK_AMOUNT')]) !!}
                                        </div>
                                        <div class="error">{{ $errors->first('max_cashback_amount') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->  
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.CASHBACK_TYPE'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>

                                        <?php
                                        $cashback_type = array('' => trans('admin.PLEASE_SELECT')) + Config::get('global.discount_type');
                                        ?>
                                        {!! Form::select('cashback_type', $cashback_type, null, ['id'=>'cashback_type','class' => 'select2']) !!}

                                        <div class="error">{{ $errors->first('cashback_type') }}</div>
                                    </div>
                                </div><!-- /.form-group -->



                            </div><!-- /.row -->   


                            <div class="row">
                                <div class="form-group col-md-6 fixed_div">
                                    {!! Form::label(trans('admin.AMOUNT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa  {{Config::get('global.currency_icon')}}"></i>
                                            </div>
                                            {!! Form::text('amount',null,['id'=>'amount','class'=>'form-control','placeholder'=>trans('admin.AMOUNT')]) !!}
                                        </div>

                                        <div class="error">{{ $errors->first('amount') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 percentage_div">
                                    {!! Form::label(trans('admin.PERCENTAGE'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        <div class="input-group">

                                            {!! Form::text('percentage',null,['id'=>'percentage','class'=>'form-control','placeholder'=>trans('admin.PERCENTAGE')]) !!}
                                            <div class="input-group-addon">
                                                <i class="fa   fa-percent"></i>
                                            </div>
                                        </div>

                                        <div class="error">{{ $errors->first('percentage') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->    

                            <h1>  Cash on Delivery</h1>

                            <div class="row">


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.ENABLE_COD'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>

                                        {!! Form::checkbox('enable_cod',1,null, array('class' => 'minimal enable_cod '))!!}
                                        <div class="error">{{ $errors->first('enable_cod') }}</div>
                                    </div>
                                </div><!-- /.form-group -->  

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.COD_CHARGE'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('cod_charge',null,['class'=>'form-control','placeholder'=>trans('admin.COD_CHARGE')]) !!}

                                        <div class="error">{{ $errors->first('cod_charge') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 

                                  <div class="row">



                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MIN_TOTAL_AMOUNT_FOR_COD'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('min_total_amount_for_cod',null,['class'=>'form-control','placeholder'=>trans('admin.MIN_TOTAL_AMOUNT_FOR_COD')]) !!}

                                        <div class="error">{{ $errors->first('min_total_amount_for_cod') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 
                            <div class="row">



                                <div class="form-group col-md-12 ">
                                    {!! Form::label(trans('admin.COD_DESCRIPTION'),null,['class'=>'col-sm-2 control-label required_label']) !!}
                                    <div class='col-sm-10'>
                                        {!! Form::textarea('cod_description',null,['class'=>'form-control ckeditor','placeholder'=>trans('admin.COD_DESCRIPTION')]) !!}

                                        <div class="error">{{ $errors->first('cod_description') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 
                              <h1> Shipping  Charges </h1>
                                           <div class="row">
                                                    <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.SHIPPING_CHARGES'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('shipping_charges',null,['class'=>'form-control','placeholder'=>trans('admin.SHIPPING_CHARGES')]) !!}

                                        <div class="error">{{ $errors->first('shipping_charges') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MIN_TOTAL_AMOUNT_FOR_SHIPPING'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('min_total_amount_for_shipping',null,['class'=>'form-control','placeholder'=>trans('admin.MIN_TOTAL_AMOUNT_FOR_SHIPPING')]) !!}

                                        <div class="error">{{ $errors->first('min_total_amount_for_shipping') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 



                              
                            <h1>PayTm </h1>
                            <div class="row">


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.ENABLE_PAYTM'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>

                                        {!! Form::checkbox('enable_paytm',1,null, array('class' => 'minimal enable_cod '))!!}
                                        <div class="error">{{ $errors->first('enable_cod') }}</div>
                                    </div>
                                </div><!-- /.form-group -->  

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.TEST_MODE'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::checkbox('paytm_test_mode',1,null, array('class' => 'minimal paytm_test_mode '))!!}

                                        <div class="error">{{ $errors->first('paytm_test_mode') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div><!-- /.row -->
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MID'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('mid',null,['class'=>'form-control','placeholder'=>trans('admin.MID')]) !!}

                                        <div class="error">{{ $errors->first('mid') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.WEBSITE'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('website',null,['class'=>'form-control','placeholder'=>trans('admin.WEBSITE')]) !!}

                                        <div class="error">{{ $errors->first('website') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->     
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MERCHANT_KEY'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('merchant_key',null,['class'=>'form-control','placeholder'=>trans('admin.MERCHANT_KEY')]) !!}

                                        <div class="error">{{ $errors->first('merchant_key') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.CHANNEL_ID'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('channel_id',null,['class'=>'form-control','placeholder'=>trans('admin.CHANNEL_ID')]) !!}

                                        <div class="error">{{ $errors->first('channel_id') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->      


                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.INDUSTRY_TYPE_ID'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('industry_type_id',null,['class'=>'form-control','placeholder'=>trans('admin.INDUSTRY_TYPE_ID')]) !!}

                                        <div class="error">{{ $errors->first('industry_type_id') }}</div>
                                    </div>
                                </div><!-- /.form-group -->



                            </div><!-- /.row --> 
                            
                            <div class="row">



                                <div class="form-group col-md-12 ">
                                    {!! Form::label(trans('admin.PAYTM_DESCRIPTION'),null,['class'=>'col-sm-2 control-label required_label']) !!}
                                    <div class='col-sm-10'>
                                    {!! Form::textarea('paytm_description',null,['class'=>'form-control ckeditor','placeholder'=>trans('admin.PAYTM_DESCRIPTION')]) !!}

                                        <div class="error">{{ $errors->first('paytm_description') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 
                            <h1>PayU </h1>
                            <div class="row">


                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.ENABLE_PAYU'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>

                                        {!! Form::checkbox('enable_payu',1,null, array('class' => 'minimal enable_cod '))!!}
                                        <div class="error">{{ $errors->first('enable_payu') }}</div>
                                    </div>
                                </div><!-- /.form-group -->  

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.TEST_MODE'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::checkbox('payu_test_mode',1,null, array('class' => 'minimal payu_test_mode '))!!}

                                        <div class="error">{{ $errors->first('payu_test_mode') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div><!-- /.row -->
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.MERCHANT_ID'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('merchant_id',null,['class'=>'form-control','placeholder'=>trans('admin.MERCHANT_ID')]) !!}

                                        <div class="error">{{ $errors->first('merchant_id') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.SALT'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('salt',null,['class'=>'form-control','placeholder'=>trans('admin.SALT')]) !!}

                                        <div class="error">{{ $errors->first('salt') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->   
                              <div class="row">



                                <div class="form-group col-md-12 ">
                                    {!! Form::label(trans('admin.PAYU_DESCRIPTION'),null,['class'=>'col-sm-2 control-label required_label']) !!}
                                    <div class='col-sm-10'>
                                    {!! Form::textarea('payu_description',null,['class'=>'form-control ckeditor','placeholder'=>trans('admin.PAYU_DESCRIPTION')]) !!}

                                        <div class="error">{{ $errors->first('payu_description') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 
                              <div class="row">



                                <div class="form-group col-md-12 ">
                                    {!! Form::label(trans('admin.PAYUMONEY_DESCRIPTION'),null,['class'=>'col-sm-2 control-label required_label']) !!}
                                    <div class='col-sm-10'>
                                    {!! Form::textarea('payumoney_description',null,['class'=>'form-control ckeditor','placeholder'=>trans('admin.PAYUMONEY_DESCRIPTION')]) !!}

                                        <div class="error">{{ $errors->first('payumoney_description') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 

                        </div><!-- /.End payment setting -->
                        <!-- /.start delhivery setting -->
                        <div  class="tab-pane " id="delhivery" >


                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.DELHIVERY_CLIENT'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('delhivery_client',null,['class'=>'form-control','placeholder'=>trans('admin.DELHIVERY_CLIENT')]) !!}

                                        <div class="error">{{ $errors->first('delhivery_client') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.DELHIVERY_API'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('delhivery_api',null,['class'=>'form-control','placeholder'=>trans('admin.DELHIVERY_API')]) !!}

                                        <div class="error">{{ $errors->first('delhivery_api') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->   
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.DELHIVERY_PICKUP'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('delhivery_pickup',null,['class'=>'form-control','placeholder'=>trans('admin.DELHIVERY_PICKUP')]) !!}

                                        <div class="error">{{ $errors->first('delhivery_pickup') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.DELHIVERY_TEST_MODE'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::checkbox('delhivery_test_mode',1,null, array('class' => 'minimal delhivery_test_mode '))!!}
                                        <div class="error">{{ $errors->first('delhivery_test_mode') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->   


                            <h1>Pickup Address </h1>

                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.PICKUP_ADDRESS'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('pickup_address',null,['class'=>'form-control','placeholder'=>trans('admin.PICKUP_ADDRESS')]) !!}

                                        <div class="error">{{ $errors->first('pickup_address') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.PICKUP_PINCODE'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('pickup_pincode',null,['class'=>'form-control pickup_pincode','placeholder'=>trans('admin.PICKUP_PINCODE')]) !!}
                                        <div class="error">{{ $errors->first('pickup_pincode') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row -->   


                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.PICKUP_CITY'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('pickup_city',null,['class'=>'form-control pickup_city','placeholder'=>trans('admin.PICKUP_CITY')]) !!}

                                        <div class="error">{{ $errors->first('pickup_city') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.PICKUP_STATE'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('pickup_state',null,['class'=>'form-control pickup_state','placeholder'=>trans('admin.PICKUP_STATE')]) !!}
                                        <div class="error">{{ $errors->first('PICKUP_STATE') }}</div>
                                    </div>
                                </div><!-- /.form-group -->


                            </div><!-- /.row --> 
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.PICKUP_PHONE'),null,['class'=>'col-sm-4 control-label ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('pickup_phone',null,['class'=>'form-control','placeholder'=>trans('admin.PICKUP_PHONE')]) !!}

                                        <div class="error">{{ $errors->first('pickup_phone') }}</div>
                                    </div>
                                </div><!-- /.form-group -->



                            </div><!-- /.row -->   
                        </div><!-- /.End delhivery setting -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
                <div class="box-footer">

                    {!! Html::link(route('admin.settings.index'), trans('admin.CANCEL'), ['id' => 'linkid','class' => 'btn btn-default pull-left']) !!}


                    {!! Form::submit(trans('admin.SUBMIT'),['class' => 'btn btn-info pull-right'])!!}
                </div>

                {!! Form::close() !!}

            </div><!-- /.col -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<style type="text/css">

    .select2-container{
        width: 100% !important;
    }

</style>

<script type="text/javascript">
    $(document).ready(function () {


        $('.pickup_pincode').keyup(function ()
        {

            $.ajax({
                url: WEBSITE_URL + "api/get-pincode-detail/" + $('.pickup_pincode').val(),
                dataType: "json",
                success: function (response) {
                    if (response.status_code == 1) {
                        $('.pickup_city').val(response.data.city);
                        $('.pickup_state').val(response.data.state);

                    }

                }
            });
        });



        discount_types($('#cashback_type').val());

        $('#cashback_type').change(function ()
        {
            var cashback_type = $(this).val();

            discount_types(cashback_type);





        });
    });

    function discount_types(cashback_type) {


        if (cashback_type != '') {
            if (cashback_type == 'fixed') {

                $(".fixed_div").show();
                $(".percentage_div").hide();
            } else {
                $(".fixed_div").hide();
                $(".percentage_div").show();

            }
        } else {
            $(".fixed_div").hide();
            $(".percentage_div").hide();
        }
    }

</script>
@stop
<!-- /.content-wrapper -->

