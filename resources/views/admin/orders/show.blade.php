<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

   <div class="col-md-12 col-xs-12">
          <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.orders.index',['status'=>$status]),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                </h3>
                 </div>
                 </div>
            <div class="col-md-4 col-xs-12">
               
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-title">
                                    SHIPMENT ADDRESS 
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                      <address>
                    <strong>
                            {{ucwords($order->first_name.' '.$order->last_name)}}</strong><br>
                            {{$order->address_1}}<br>
                            {{$order->address_2}}<br>
                            {{$order->city}} , {{$order->state}}  {{$order->pin_code}} <br>
                            Phone: {{$order->mobile}}
                    
                  </address>

                  
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            @if($order->is_cash_back == 1 && $order->order_status!="failed" &&  $order->order_status!="reject" &&  $order->order_status!="cancel")
                     <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-title">
                                   CASH BACK
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                      
                    <strong>
                         Cash back of  {{display_price($order->cash_back_amount)}} for Promo Code {{ strtoupper($order->coupon_code)}} has been appyied</strong>
                           
                    
                 

                  
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

         @endif 
                 @if($order->order_status == 'cancel')
                     <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-title">
                                   Cancel Reason
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                      
                    
                          {{$order->cancel_reason}} 
                           
                    
                 

                  
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

         @endif       @if($order->order_status == 'reject' || $order->order_status == 'failed' )
                     <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-title">
                                    Reason
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                      
                    
                          {{$order->reject_reason}} 
                           
                    
                 

                  
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

         @endif 



            </div><!-- /.col -->
              <div class="col-md-8 col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-title">
                            ORDER SUMMARY
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @foreach ($order->order_detail as $product)
                                     <div class="row">
                                        <div class="col-md-2 img-bx">
                                        
                                            {!! BasicFunction::showImage( PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH,PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH,$product->product->getImage->name,array('width'=>'100', 'height'=>'100','zc'=>0,'class'=>'margin')) !!}
                                        </div>
                                        <div class="col-md-10">
                                            <p><strong>{{ucfirst($product->product->title)}}</strong></p>
                                            <p > Price : {{display_price($product->price)}}</p>
                                            <div class="row">
                                                <div class="col-md-6">Size : {{$product->product_size}}</div>
                                                <div class="col-md-6">Quantity : {{$product->quantity}}</div>
                                            </div>
                                            
                                        </div>
                                    </div><hr>
                            @endforeach
                            <div class="row">
                                <div class="col-md-5"></div>
                                    <div class="col-md-7 ">
                                        <div class="col-md-12 "> 
                                            <span class="pull-left"><strong>Sub Total</strong></span>
                                            <span class="pull-right">{{display_price($order->total_price)}}</span>
                                        </div> 
                                        @if($order->shipping_charge > 0)
                                            <div class="col-md-12 "> 
                                                <span class="pull-left"><strong>Shipping</strong></span>
                                                <span class="pull-right">{{display_price($order->shipping_charge)}}</span>
                                            </div> 
                                          @endif  

                                         @if($order->total_tax > 0)

                                         <?php $tax_description = unserialize($order->tax_description); 
                                          //  dd($tax_description);
                                        ?>
                                          @foreach ($tax_description as $tax)
                                            <div class="col-md-12 "> 
                                                <span class="pull-left"><strong>{{$tax['name']}}</strong></span>
                                                <span class="pull-right">{{display_price($tax['amount'])}}</span>
                                           </div> 
                                                    @endforeach
                                          @endif

                                      
                                            @if($order->is_cod)
                                            <div class="col-md-12 "> 
                                                <span class="pull-left"><strong>Cod Charge</strong></span>
                                                <span class="pull-right">{{display_price($order->cod_money)}}</span>
                                            </div> 
                                          @endif 
                                                  @if($order->is_discount > 0)
                                            <div class="col-md-12 "> 
                                                <span class="pull-left"><strong>Discount</strong></span>
                                                <span class="pull-right">- {{display_price($order->discount)}}</span>
                                            </div> 
                                          @endif 
                                               @if($order->wallet_amount > 0)
                                            <div class="col-md-12 "> 
                                                <span class="pull-left"><strong>Wallet </strong></span>
                                                <span class="pull-right">- {{display_price($order->wallet_amount)}}</span>
                                            </div> 
                                          @endif 


                                        <hr>
                                         <div class="col-md-12 "> 
                                                <span class="pull-left"><strong>Payble Amount</strong></span>
                                                <span class="pull-right">{{display_price($order->payble_amount-$order->wallet_amount)}}</span>
                                            </div> 
                                     </div>
                            </div>
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@stop
<!-- /.content-wrapper -->
