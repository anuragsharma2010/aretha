<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
echo $pageTitle;
?>
       </h1>
        @include('includes.admin.breadcrumb')
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.products.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                </h3>
            </div>
            {!! Form::open(['route'=>'admin.products.store']) !!}  
            <div class="box-body">

                <div class="row">

                    <div class="col-md-12">
                     <div class="row">
                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.TITLE'),null,['class'=>'required_label']) !!}
                                {!! Form::text('title',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE')]) !!}
                                <div class="error">{{ $errors->first('title') }}</div>
                            </div><!-- /.form-group -->

                          <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.PRICE'),null,['class'=>'required_label']) !!}
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa  {{Config::get('global.currency_icon')}}"></i>
                              </div>
                                {!! Form::text('price',null,['class'=>'form-control','placeholder'=>trans('admin.PRICE')]) !!}
                              </div>
                                <div class="error">{{ $errors->first('price') }}</div>
                          </div><!-- /.form-group -->

                   


                        </div><!-- /.row -->      <div class="row">
                            <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.DESCRIPTION'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('description',null,['id'=>'description','class'=>'form-control ckeditor','placeholder'=>trans('admin.DESCRIPTION')]) !!}
                                <div class="error">{{ $errors->first('description') }}</div>
                            </div><!-- /.form-group -->
                                
                        </div><!-- /.row -->
                        <div class="row">

                              <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.SHORT_DESCRIPTION'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('short_description',null,['id'=>'description','class'=>'form-control','placeholder'=>trans('admin.SHORT_DESCRIPTION')]) !!}
                                <div class="error">{{ $errors->first('short_description') }}</div>
                              </div><!-- /.form-group -->

                            <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.META_TITLE'),null,['class'=>'required_label']) !!}
                                {!! Form::text('meta_title',null,['class'=>'form-control','placeholder'=>trans('admin.META_TITLE')]) !!}
                                <div class="error">{{ $errors->first('meta_title') }}</div>
                            </div><!-- /.form-group -->
                                
                        </div><!-- /.row --> 
                        <div class="row">
                            <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.META_KEYWORDS'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('meta_keywords',null,['class'=>'form-control','placeholder'=>trans('admin.META_KEYWORDS')]) !!}
                                <div class="error">{{ $errors->first('meta_keywords') }}</div>
                            </div><!-- /.form-group -->
                                
                        </div><!-- /.row -->   
                       <div class="row">
                            <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.META_DESCRIPTION'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('meta_description',null,['class'=>'form-control','placeholder'=>trans('admin.META_DESCRIPTION')]) !!}
                                <div class="error">{{ $errors->first('meta_description') }}</div>
                            </div><!-- /.form-group -->
                                
                        </div><!-- /.row -->

                         <div class="row">


                           <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.SIZECHART'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('size_chart',null,['id'=>'description','class'=>'form-control ckeditor','placeholder'=>trans('admin.SIZECHART')]) !!}
                                <div class="error">{{ $errors->first('size_chart') }}</div>
                            </div><!-- /.form-group -->

                         
                        <div class="form-group col-md-12 ">
                          {!! Form::label(trans('admin.TAGS'),null,['class'=>'']) !!}
                            <?php $tags_list = BasicFunction::getAllTagsList(); ?>
                            {!! Form::select('tags[]', $tags_list, null, ['class' => 'form-control select2','id'=>'multiselectList' ,'multiple' => 'multiple','data-placeholder'=>trans('admin.PLEASE_SELECT')]) !!}
                           <div class="error">{{ $errors->first('name') }}</div>
                        </div><!-- /.form-group -->

                     
                        <div class="form-group col-md-6 ">
                            {!! Form::label(trans('admin.CATEGORY'),null,['class'=>'required_label']) !!}
                           <?php  

                            $category_list    =   array('' => trans('admin.PLEASE_SELECT')) + BasicFunction::getAllChildCategory();
                           
                           ?>
                           {!! Form::select('category_id', $category_list, null, ['class' => 'form-control select2','id'=>'category_id']) !!}
                            <div class="error">{{ $errors->first('category_id') }}</div>
                             </div><!-- /.form-group -->
                         </div><!-- /.row -->
                          <div id='get_attribute'>
                          <?php $category_id = old('category_id')  ?>
                            @if(!empty($category_id))

                           
                           <?php   $data_array =     BasicFunction::getAttributeByCategory($category_id);
                            ?>
                              
                             @include('admin.products.get_attribute', ['data' => $data_array['data']])

                                  @endif
                          </div>

                         <div class="row">
                     
                        <div class="form-group col-md-6 ">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php
                                    $status_list = Config::get('global.status_list');
                            ?>
                           {!! Form::select('status', $status_list, null, ['class' => 'form-control select2']) !!}
                        </div><!-- /.form-group -->
                         </div><!-- /.row -->
                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.box-body -->


            <div class="box-footer">

                <div class="pull-right">
                    {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!} 
                    {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                </div>
            </div>
            <!-- /.box-footer -->

            {!! Form::close() !!}

        </div><!-- /.box -->


    </section><!-- /.content -->
</div>
    <script type="text/javascript">
      $(document).ready(function(){ 
          $('#category_id').change(function()
  {
  
    var category_id = $(this).val();
  $.ajax({
      url:WEBSITE_ADMIN_URL+"get-attribute-by-category", 
      method:'POST',
      data:{category_id:category_id},
      success:function(data) {
        $("#get_attribute").html(data);
            $(".select2").select2();
      }
   });
    
    //alert(category_id);
    
  });

      });

    </script>




@stop
