  @if(!empty($data))
  @foreach ($data as $value)
<div class="row">
                     
     <div class="form-group col-md-6 ">
                            {!! Form::label($value['name'],null,['class'=>'']) !!}
                           <?php  

                          $category_list    =   array('' => trans('admin.PLEASE_SELECT')) + BasicFunction::getAllChildCategory();
                          $AttributeValue =  array_map('ucfirst',  $value['AttributeValue']);

                          $selected =  array();
                        
                          if(old('attribute_value')){

                              $selected = old('attribute_value.'.$value["id"]);

                          }else{

                           
                              if(isset($products->attributes_value) && !empty($products->attributes_value)){

                                if(isset($products->attributes_value[$value["id"]])){

                                $selected = $products->attributes_value[$value["id"]] ;
                                }

                              }
                          }
                           ?>
                           {!! Form::select('attribute_value['.$value["id"].'][]',$AttributeValue, $selected, ['class' => 'form-control select2','multiple' => 'multiple','data-placeholder'=>'Select a '.$value['name']]) !!}
     <div class="error">{{ $errors->first('category_id') }}</div>
     </div><!-- /.form-group -->
</div><!-- /.row --> 
@endforeach

                @endif