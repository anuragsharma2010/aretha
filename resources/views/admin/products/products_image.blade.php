<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  
  {!! Html::style( asset('css/jquery.fileupload.css')) !!}


  {!! Html::script(asset('js/fileupload/vendor/jquery.ui.widget.js')) !!}
  {!! Html::script(asset('js/fileupload/tmpl.min.js')) !!}
  {!! Html::script(asset('js/fileupload/load-image.all.min.js')) !!}
  {!! Html::script(asset('js/fileupload/jquery.fileupload.js')) !!}
  {!! Html::script(asset('js/fileupload/jquery.fileupload-process.js')) !!}
  {!! Html::script(asset('js/fileupload/jquery.fileupload-image.js')) !!}
  {!! Html::script(asset('js/fileupload/jquery.fileupload-validate.js')) !!}
  {!! Html::script(asset('js/fileupload/jquery.fileupload-ui.js')) !!}
  {!! Html::script(asset('js/fileupload/main.js')) !!}

  <script type="text/javascript">
      
var id = {{$products->id}}

  </script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            
<a href="javascript:void(0)" id='add_new' class="btn btn-primary"><i class="fa  fa-plus"></i>{{trans('admin.ADD_NEW')}}</a>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">


  @include('includes.admin.file_upload_info',['filecount'=>config('global.max_file_count_upload')])


             

    <div class="image_div add_image_div " style="display:none;">
       <div class="box box-primary">
        <div class="row" >
          <div class="content" >
            <div class="col-md-9">
             {!! Form::model($products,['method'=>'post','route'=>['admin.products.update',$products->id],'id' => 'fileupload','files'=>true]) !!}
              <div class="clearfix mt25"></div>
              <div class="first_column">
                <div class="form-group">
                  <div>
                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                    <div class="row fileupload-buttonbar margin_bottom">
                      <div class="col-lg-7">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>    {{ trans('admin.ADD_FILES') }} </span>
                        <input type="file" name="files" multiple>
                        </span>&nbsp;
                        <span class="fileupload-process"></span>
                      </div>
                      <div class="col-lg-5 fileupload-progress fade margin_bottom">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active progress_bottom_margin" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                          <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <!-- The extended global progress state -->
                        <div class="progress-extended progress_bar">&nbsp;</div>
                      </div>
                    </div>
                    <!-- The table listing the files available for upload/download -->
                    <table role="presentation" id="upload_file" class="table table-striped">
                      <tbody class="files"></tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="clearfix mt25"></div>
              <div class="form-group">
                <div class="col-sm-10 ml30 fileupload-buttonbar" >
                
           

{!! Form::button(trans('admin.CLOSE'),['class' => 'btn btn-default ','id' => 'saveImage']) !!} 


                  


  {!!  Html::decode(Html::link(route('admin.products.products_image', $products->id),trans('admin.DONE'),['class'=>'btn btn-primary display_hidden','data-toggle'=>'tooltip','title'=>trans('admin.DONE'),'id' => 'submitBusiness',])) !!} 

                    
                </div>
              </div>
                     <div class="clearfix mb25"></div>
              {!! Form::close() !!}
            </div>
          </div>
    
    </div>
    </div>
    </div>


                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%">{{trans('admin.IMAGES')}}</th>
                                    
                                    
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="20%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                               <tbody>
                                @if(!$products_images->isEmpty())
                                @foreach ($products_images as $image)
                                <tr>
                                    <td>{!! BasicFunction::showImage( PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH,PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH,$image->name,array('width'=>'100', 'height'=>'100','zc'=>0)) !!}</td>
                               
                                    <td>{{ date_val($image->created_at,DATE_FORMATE ) }}</td>
                                    <td>{{ date_val($image->updated_at,DATE_FORMATE ) }}</td>
                                    <td align="center">
                                        @if($image->status == 1)

                                      
                                         @if($image->is_main == 0)
                                                 {!!  Html::decode(Html::link(route('admin.products.set_main_image',['id' => $image->id,'product_id'=>$products->id]),"<i class='fa   fa-check-circle'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.SET_MAIN_IMAGE'), "data-alert"=>trans('admin.SET_MAIN_IMAGE_ALERT')])) !!}
                                                  {!!  Html::decode(Html::link(route('admin.products.product_image_status_change',['id' => $image->id,'status'=>$image->status,'product_id'=>$products->id]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!} 
                                                      @else
                                                        {!!  Html::decode(Html::link("#","<i class='fa  fa-check'></i>",['class'=>'btn btn-success ','data-toggle'=>'tooltip','onclick'=>'return false','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!} 
                                                          
                                         @endif


                                        @else      
                                                {!!  Html::decode(Html::link(route('admin.products.product_image_status_change',['id' => $image->id,'status'=>$image->status,'product_id'=>$products->id]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        
                                        @endif
                                       @if($image->is_main == 0)

                                         {!!  Html::decode(Html::link(route('admin.products.delete_products_image',['id'=>$image->id,'product_id'=>$products->id]),"<i class='fa  fa-trash'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.DELETE'),"data-alert"=>trans('admin.DELETE_ALERT')])) !!} 
                                          @endif

                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        
                        </table>
                          {!! $products_images->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
  {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-upload fade">
          <td width="10%">
              <span class="preview"></span>
          </td>
          <td width="50%">
              <p class="name">{%=file.name%}</p>
              <strong class="error text-danger"></strong>
          </td>
          <td width="10%">
              <p class="size">Processing...</p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success progress-bar-success_style"></div></div>
          </td>
          <td width="30%" class=" text-right">
              {% if (!i && !o.options.autoUpload) { %}
                  <button class="btn btn-primary pink start" disabled>
                     <i class="fa-upload fa"></i> 
                      <span><?php //echo __('START');?></span>
                  </button>
              {% } %}
              {% if (!i) { %}
                 <button class="btn btn-warning cancel">
                     <i class="fa-times fa"></i> 
                      <span><?php //echo __('CANCEL');?></span>
                  </button>
              {% } %}
          </td>
      </tr>
  {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
  {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-download fade">
          <td width="10%">
              <span class="preview">
                  {% if (file.thumbnailUrl) { %}
                      <img src="{%=file.thumbnailUrl%}" data-lightbox="{%=file.name%}" >
                  {% } %}
              </span>
          </td>
          <td width="20%">
              <p class="name">
                  {% if (file.url) { %}
                       <span>{%=file.name%}</span>
                  {% } else { %}
                      <span>{%=file.name%}</span>
                  {% } %}
              </p>
              {% if (file.error) { %}
                  <div class='error text-danger'><strong><span class="label label-danger">Error</span> {%=file.error%}</strong></div>
              {% } %}
          </td>
          <td width="10%"> 
              <span class="size">{%=o.formatFileSize(file.size)%}</span>
          </td>
          <td width="30%" class="styled-checkbox text-right">           
              {% if (file.deleteUrl) { %}               
                  <button class="btn btn-danger pink delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                     <i class="fa-trash fa "></i> 
                      <span></span>
                  </button>                
              {% } else { %}
                    <button class="btn btn-warning cancel">
                     <i class="fa-times fa"></i> 
                      <span></span>
                  </button>
              {% } %}
            
        
          </td>
      </tr>
  {% } %}
</script>

    <script type="text/javascript">
   $(document).ready(function(){ 
    $("#add_new").click(function(){


         $(".add_image_div").show();
         
    }); 

     $("#saveImage").click(function(){


         $(".add_image_div").hide();
    });

         
  });
    
</script>


@stop
<!-- /.content-wrapper -->
