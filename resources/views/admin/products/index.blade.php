<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            {!!  Html::decode(Html::link(route('admin.products.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%">@sortablelink('title', trans('admin.TITLE'))</th>
                                    <th width="10%">@sortablelink('price', trans('admin.PRICE'))</th>
                                    
                                    <th width="20%">@sortablelink('category', trans('admin.CATEGORY'))</th>
                                    
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="20%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$productslist->isEmpty())
                                @foreach ($productslist as $product)
                                <tr>
                                    <td>{{ ucfirst($product->title) }}</td>
                                    <td>{{ display_price($product->price) }}</td>
                                     
                                    <td>{{ ucfirst($product->category->name) }}</td>
                                    
                                    <td>{{ date_val($product->created_at,DATE_FORMATE ) }}</td>
                                    <td>{{ date_val($product->updated_at,DATE_FORMATE ) }}</td>
                                    <td align="center">
                                        @if($product->status == 1)
                                        {!!  Html::decode(Html::link(route('admin.products.status_change',['id' => $product->id,'status'=>$product->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        @else
                                        {!!  Html::decode(Html::link(route('admin.products.status_change',['id' => $product->id,'status'=>$product->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        @endif
                                        {!!  Html::decode(Html::link(route('admin.products.edit', $product->id),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!} 

                                         {!!  Html::decode(Html::link(route('admin.products.products_image', $product->id),"<i class='fa  fa-image'></i>",['class'=>'btn btn-warning','data-toggle'=>'tooltip','title'=>trans('admin.IMAGES')])) !!}  

                                          {!!  Html::decode(Html::link(route('admin.products.manage_stock', ['id'=> $product->id,'from'=>'index']),"<i class='fa  fa-cart-plus'></i>",['class'=>'btn btn-info','data-toggle'=>'tooltip','title'=>trans('admin.MANAGE_STOCK')])) !!} 

                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $productslist->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
