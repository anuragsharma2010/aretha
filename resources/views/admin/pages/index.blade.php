@extends('layouts.default')

@section('content')  
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
   <section class="content-header">
        <h1>
            <?php $admin = adminUser();

             echo trans('admin.HELLO').' '.ucfirst($admin->first_name);
             ?>
        </h1>
       
    </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>{{$users_count}}</h3>
            <p>Users Registered</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="{{route('admin.users.index')}}" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>{{$product_count}}</h3>
            <p>Product  Pages</p>
          </div>
          <div class="icon">
            <i class="fa fa-product-hunt" aria-hidden="true"></i>
          </div>
          <a href="{{route('admin.products.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>{{$coupon_count}}</h3>
            <p>Coupon  Pages</p>
          </div>
          <div class="icon">
            <i class="fa fa-tag" aria-hidden="true"></i>
          </div>
          <a href="{{route('admin.coupons.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{$cms_count}}</h3>
            <p>Cms Pages</p>
          </div>
          <div class="icon">
            <i class="fa fa-file" aria-hidden="true"></i>
          </div>
          <a href="{{route('admin.cms.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

   
    </div><!-- /.row -->
    <!-- Main row -->
        <div class="row">
          <div class="col-lg-6 col-xs-6">
           <div class="row">
                  <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>{{ display_price($today_sel)}}</h3>
            <p>Today Sales</p>
          </div>
          <div class="icon">
            <i class="fa fa-inr" aria-hidden="true"></i>
          </div>
          <a href="{{route('admin.orders.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{display_price($total_sel)}}</h3>
            <p>Total Sales</p>
          </div>
          <div class="icon">
            <i class="fa fa-inr" aria-hidden="true"></i>
          </div>
          <a href="{{route('admin.orders.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>{{$newlatterCount}}</h3>
            <p>Newsletter Subscriber</p>
          </div>
          <div class="icon">
            <i class="fa fa-envelope" aria-hidden="true"></i>
          </div>
          <a href="{{route('newlatters')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>{{$total_order}}</h3>
            <p>Total Order</p>
          </div>
          <div class="icon">
            <i class="fa fa-truck" aria-hidden="true"></i>
          </div>
          <a href="{{route('admin.orders.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

 </div>
 </div>
      <div class="col-lg-6 col-xs-6">
             <?php 

              
                $options1 = [
                    

                    'height'=>400,
                    'title'=>"Today's Order"
                ];

          

                ?>
          
                {!!ChartManager::setChartType('piechart-chart')
                        ->setOptions($options1)
                        ->setCols($cols1)
                        ->setRows($rows1)
                        ->render()!!}
          

          
                  


 </div> 
          </div> 
           <div class="row">
      <div class="col-lg-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Recent Orders</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body " style="padding: 5px; ">
                <table id="example2" class="table table-bordered table-striped example2">
                            <thead>
                                <tr>
                          
                                    <th width="10%">{{trans('admin.ORDER_ID')}}</th>
                                    <th width="15%">{{trans('admin.USER')}}</th>
                                    <th width="10%">{{trans('admin.PAYMENT_BY')}}</th>
                                    <th width="10%">@sortablelink('payble_amount', trans('admin.PAYBLE_AMOUNT'))</th>
                                   
                                    <th width="10%">@sortablelink('order_status', trans('admin.ORDER_STATUS'))</th>
                              
                                      
                                    <th width="10%">@sortablelink('shipped_by', trans('admin.SHIPPED_BY'))</th>
                                      
                                    <th width="10%">@sortablelink('payment_status', trans('admin.PAYMENT_STATUS'))</th>

                                    <th width="7%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$orders_list->isEmpty())
                                @foreach ($orders_list as $orders)
                                <tr>
                                   
                                    <td> {{ $orders->order_id }} </td>
                                    <td> {{ ucwords($orders->user->first_name.' '.$orders->user->last_name) }} </td>
                                    <td> {{ ucfirst($orders->payment_by) }} </td>
                                    <td> {{ display_price($orders->payble_amount) }} </td>
                                   
                                      <td> {{ ucfirst($orders->order_status) }} </td>
                                      

                                  
                                      <td> {{ ucfirst($orders->shipped_by) }} </td>
                                    
                                    <td> {{ ucfirst($orders->payment_status) }} </td>
                                    <td> {{ date_val($orders->created_at,DATE_FORMATE ) }} </td>
                                    
                                  
                                    @endforeach
                                    @else

                                <tr><td colspan="7"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                             </div>
            <!-- /.box-body -->
   
            <!-- /.box-footer -->
          </div> 
          </div> 
          </div> 
        <div class="row">
      <div class="col-lg-12 col-xs-12">
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">All Orders</h3>

              <div class="box-tools pull-right">
                <a href="{{route('dashboard', ['type'=>'day'])}}" class="btn  btn-{{($type=='day')?'primary':'default'}}" >Day
                </a>
                 <a href="{{route('dashboard', ['type'=>'month'])}}" class="btn  btn-{{($type=='month')?'primary':'default'}}" >
                  Month
                </a>
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body " style="padding: 5px; ">
               <?php $options = [
                    'title' => 'Orders',
                    'chart' => [
                        'title' => 'Orders'
                    ],
                    'chartArea' => ['width' => '100%'],
                    'hAxis' => [
                        'title' => 'Total Population',
                        'minValue' => 0
                    ],
                    'vAxis' => [
                        'title' => 'City'
                    ],
                    'bars' => 'vertical', //required if using material chart
                    'axes' => [
                        'y' => [0 => ['side' => 'left']]
                    ],
                    'height'=>350
                ];

            
                ?>

{!!ChartManager::setChartType('bar-chart')
                        ->setOptions($options)
                        ->setCols($cols)
                        ->setRows($rows)
                     ->render()!!}
                 
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
   
            <!-- /.box-footer -->
          </div> 
          </div> 
          </div> 
          
  </section><!-- /.content -->






</div>

@stop