<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            {!!  Html::decode(Html::link(route('admin.taxs.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="15%">@sortablelink('title', trans('admin.TITLE'))</th>
                                    <th width="15%">@sortablelink('name', trans('admin.NAME'))</th>
                                    <th width="15%">@sortablelink('rate', trans('admin.TAX_RATE'))</th>
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="15%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$taxs_list->isEmpty())
                                @foreach ($taxs_list as $taxs)
                                <tr>
                                    <td> {{ ucfirst($taxs->title) }} </td>
                                    <td> {{ ucfirst($taxs->name) }} </td>
                                    <td> {{ $taxs->tax_rate }} </td>
                                      <td> {{ date_val($taxs->created_at,DATE_FORMATE ) }} </td>
                                    <td> {{ date_val($taxs->updated_at,DATE_FORMATE ) }} </td>
                                    <td align="center">
                                        @if($taxs->status == 1)
                                        {!!  Html::decode(Html::link(route('admin.taxs.status_change',['id' => $taxs->id,'status'=>$taxs->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        @else
                                        {!!  Html::decode(Html::link(route('admin.taxs.status_change',['id' => $taxs->id,'status'=>$taxs->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        @endif
                                        {!!  Html::decode(Html::link(route('admin.taxs.edit', $taxs->id),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $taxs_list->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
