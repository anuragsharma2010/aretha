<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            {!!  Html::decode(Html::link(route('admin.site_images.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%">{{trans('admin.IMAGE')}}</th>
                                    <th width="20%">@sortablelink('TITLE', trans('admin.NAME'))</th>
                                    <th width="20%">{{trans('admin.IMAGE_URL')}}</th>
                                    
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="20%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$site_images_list->isEmpty())
                                @foreach ($site_images_list as $site_images)
                                <tr>
                                    <td>{!! BasicFunction::showImage( SITE_IMAGES_UPLOAD_DIRECTROY_PATH,SITE_IMAGES_ONTHEFLY_IMAGE_PATH,$site_images->image,array('width'=>'100', 'height'=>'100','zc'=>0)) !!}</td>
                                    <td>{{ ucfirst($site_images->title) }}</td>
                                    <td>{{ SITE_IMAGES_URL.$site_images->image }}</td>
                                    
                                    <td>{{ date_val($site_images->created_at,DATE_FORMATE ) }}</td>
                                    <td>{{ date_val($site_images->updated_at,DATE_FORMATE ) }}</td>
                                    <td align="center">
                                             <a class='btn btn-danger btn-delete' data-alert="{{trans('admin.DELETE_ALERT')}}"  title="{{trans('admin.DELETE')}}" data-toggle='tooltip'> 
                                            {!! Form::open(['method'=>'delete',  'route'=>['admin.site_images.destroy',$site_images->id],'class'=>'delete_form' ]) !!}                         
                                            <i class="fa fa-remove "></i>
                                            {!! Form::close() !!}
                                        </a>

                                       

                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="6"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $site_images_list->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
