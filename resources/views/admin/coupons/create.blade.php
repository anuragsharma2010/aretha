<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
            echo $pageTitle;
            ?>
        </h1>
        @include('includes.admin.breadcrumb')
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.coupons.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                </h3>
            </div>
            {!! Form::open(['route'=>'admin.coupons.store']) !!}  
            <div class="box-body">

                <div class="row">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.TITLE'),null,['class'=>'required_label']) !!}
                                {!! Form::text('title',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE')]) !!}
                                <div class="error">{{ $errors->first('title') }}</div>
                            </div><!-- /.form-group -->
                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.COUPONS_CODE').'('.trans('admin.COUPONS_CODE_HELP') .')',null,['class'=>'required_label']) !!}

                                <div class="input-group">
                                    {!! Form::text('coupons_code',null,['id'=>'coupons_code','class'=>'form-control','placeholder'=>trans('admin.COUPONS_CODE')]) !!}
                                    <span class="input-group-addon">
                                        <input type="checkbox" id='randomlyCheckBox'>
                                    </span>
                                    <input type="hidden" id='randomly_text'  value={{ randomCouponCode(Config::get('global.coupon_code_lenth'),false,Config::get('global.coupon_code_set')) }} id='randomly'>
                                </div>


                                <div class="error">{{ $errors->first('coupons_code') }}</div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->


                        <div class="row">
                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.START_DATE'),null,['class'=>'required_label']) !!}
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa  fa-calendar"></i>
                                    </div>
                                    {!! Form::text('start_date',null,['id'=>'start_date','class'=>'form-control','placeholder'=>trans('admin.START_DATE')]) !!}
                                </div>
                                <div class="error">{{ $errors->first('start_date') }}</div>
                            </div><!-- /.form-group -->
                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.END_DATE'),null,['class'=>'required_label']) !!}
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa  fa-calendar"></i>
                                    </div>
                                    {!! Form::text('end_date',null,['id'=>'end_date','class'=>'form-control','placeholder'=>trans('admin.END_DATE')]) !!}
                                </div>
                                <div class="error">{{ $errors->first('end_date') }}</div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->    
                        <div class="row">

                            <div class="form-group col-md-12 ">
                                {!! Form::label(trans('admin.DESCRIPTION'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('description',null,['id'=>'description','class'=>'form-control ckeditor','placeholder'=>trans('admin.DESCRIPTION')]) !!}
                                <div class="error">{{ $errors->first('description') }}</div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->


                        <div class="row">
                            <div class="form-group col-md-6 ">
                                {!! Form::label(trans('admin.DISCOUNT_ON'),null,['class'=>'required_label']) !!}
                                <?php
                                $discount_on = array('' => trans('admin.PLEASE_SELECT')) + Config::get('global.discount_on');
                                ?>
                                {!! Form::select('discount_on', $discount_on, null, ['id'=>'discount_on','class' => 'form-control select2']) !!}
                                <div class="error">{{ $errors->first('discount_on') }}</div>
                            </div><!-- /.form-group -->   



                            <div class="form-group col-md-6 ">
                                {!! Form::label(trans('admin.REDEEM_COUNT'),null,['class'=>'required_label']) !!}

                                {!! Form::text('redeem_count',null,['id'=>'amount','class'=>'form-control','placeholder'=>trans('admin.REDEEM_COUNT')]) !!}
                                <div class="error">{{ $errors->first('redeem_count') }}</div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->  
                        <div id='product_div'>
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.TAGS'),null,['class'=>'']) !!}
                                    <?php $tags_list = BasicFunction::getAllTagsList(); ?>
                                    {!! Form::select('tags[]', $tags_list, null, ['class' => 'form-control select2','id'=>'multiselectList' ,'multiple' => 'multiple','data-placeholder'=>trans('admin.PLEASE_SELECT')]) !!}
                                    <div class="error">{{ $errors->first('tags') }}</div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.CATEGORY'),null,['class'=>'']) !!}
                                    <?php
                                    $category_list = BasicFunction::getAllChildCategory();
                                    ?>
                                    {!! Form::select('category_id[]', $category_list, null, ['class' => 'form-control select2','id'=>'category_id','multiple' => 'multiple','data-placeholder'=>trans('admin.PLEASE_SELECT')]) !!}
                                    <div class="error">{{ $errors->first('category_id') }}</div>

                                </div><!-- /.form-group -->

                            </div>
                            <div class="row">
                                 <div class="form-group col-md-6 ">
                                {!! Form::label(trans('admin.MAX_PRODUCT_QUANTITY'),null,['class'=>'']) !!}
                                        <?php $max_product_quantity = array('' => trans('admin.PLEASE_SELECT')) + Config::get('global.max_product_quantity'); ?>
                                {!! Form::select('max_product_quantity', $max_product_quantity, null, ['class' => 'form-control select2','id'=>'' ]) !!}
                                <div class="error">{{ $errors->first('max_product_quantity') }}</div>
                            </div><!-- /.form-group -->
                             </div>
                            
                        </div>
                        <div id="total_price_div">
                                <div class="row">

                                    
                                         <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.MIN_TOTAL_VALUE'),null,['class'=>'required_label']) !!}

                                <div class="input-group">
                            
                                    <div class="input-group-addon">
                                        <i class="fa  {{Config::get('global.currency_icon')}}"></i>
                                    </div>
                                    {!! Form::text('min_total_value',null,['id'=>'min_total_value','class'=>'form-control','placeholder'=>trans('admin.MIN_TOTAL_VALUE')]) !!}
                                
                                </div>


                                <div class="error">{{ $errors->first('min_total_value') }}</div>
                            </div><!-- /.form-group -->
                                </div>


                        </div>

                        <div class="row">





                            <div class="form-group col-md-6 ">
                                {!! Form::label(trans('admin.USERS'),null,['class'=>'']) !!}
<?php $user_list = BasicFunction::getAllUserList(); ?>
                                {!! Form::select('users[]', $user_list, null, ['class' => 'form-control select2','id'=>'multiselectList' ,'multiple' => 'multiple','data-placeholder'=>trans('admin.PLEASE_SELECT')]) !!}
                                <div class="error">{{ $errors->first('users') }}</div>
                            </div><!-- /.form-group -->
                            <div class="form-group col-md-6 ">
                                {!! Form::label(trans('admin.DISCOUNT_TYPE'),null,['class'=>'required_label']) !!}
<?php
$discount_type = array('' => trans('admin.PLEASE_SELECT')) + Config::get('global.discount_type');
?>
                                {!! Form::select('discount_type', $discount_type, null, ['id'=>'discount_type','class' => 'form-control select2']) !!}
                                <div class="error">{{ $errors->first('discount_type') }}</div>
                            </div><!-- /.form-group --> 

                        </div><!-- /.row -->     

                        <div class="row fixed_div">

                            <div class="form-group col-md-6 ">
                                {!! Form::label(trans('admin.AMOUNT'),null,['class'=>'required_label']) !!}

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa  {{Config::get('global.currency_icon')}}"></i>
                                    </div>
                                    {!! Form::text('amount',null,['id'=>'amount','class'=>'form-control','placeholder'=>trans('admin.AMOUNT')]) !!}
                                </div>
                                <div class="error">{{ $errors->first('amount') }}</div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row --><div class="row percentage_div">

                            <div class="form-group col-md-6 ">
                                {!! Form::label(trans('admin.PERCENTAGE'),null,['class'=>'required_label']) !!}

                                <div class="input-group">

                                    {!! Form::text('percentage',null,['id'=>'percentage','class'=>'form-control','placeholder'=>trans('admin.PERCENTAGE')]) !!}
                                    <div class="input-group-addon">
                                        <i class="fa   fa-percent"></i>
                                    </div>
                                </div>
                                <div class="error">{{ $errors->first('percentage') }}</div>
                            </div><!-- /.form-group -->
                                      <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.MAX_DISCOUNT'),null,['class'=>'required_label']) !!}
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa  {{Config::get('global.currency_icon')}}"></i>
                                    </div>
                                    {!! Form::text('max_discount',null,['id'=>'max_discount','class'=>'form-control','placeholder'=>trans('admin.MAX_DISCOUNT')]) !!}
                                </div>
                                <div class="error">{{ $errors->first('max_discount') }}</div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->



                        <div class="row">

                            <div class="form-group col-md-6 ">
                                {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
<?php
$status_list = Config::get('global.status_list');
?>
                                {!! Form::select('status', $status_list, null, ['class' => 'form-control select2']) !!}
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->


                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.box-body -->


            <div class="box-footer">

                <div class="pull-right">
                    {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!} 
                    {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                </div>
            </div>
            <!-- /.box-footer -->

            {!! Form::close() !!}

        </div><!-- /.box -->


    </section><!-- /.content -->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        discount_types($('#discount_type').val());
        discountOn($('#discount_on').val());
        $('#randomlyCheckBox').click(function ()
        {
            if ($(this).is(":checked")) {
                $("#coupons_code").val($("#randomly_text").val());

            } else {

                $("#coupons_code").val('');
            }





        });
        $('#discount_type').change(function ()
        {
            var discount_type = $(this).val();

            discount_types(discount_type);





        });   $('#discount_on').change(function ()
        {
            var discount_on = $(this).val();

            discountOn(discount_on);





        });
        var startDate = new Date();
        $("#start_date").datepicker({
            startDate: startDate,
            dateFormat: date_format,
            autoclose: true,
            todayHighlight: true
        }).attr('readonly', 'readonly').on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#end_date').datepicker('setStartDate', startDate);
        });
        ;


        $("#end_date").datepicker({
            startDate: startDate,
            dateFormat: date_format,
            autoclose: true,
            todayHighlight: true
        }).attr('readonly', 'readonly').on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#start_date').datepicker('setEndDate', FromEndDate);
        });
        
    });
    function discount_types(discount_type) {

        if (discount_type != '') {
            if (discount_type == 'fixed') {

                $(".fixed_div").show();
                $(".percentage_div").hide();
            } else {
                $(".fixed_div").hide();
                $(".percentage_div").show();

            }
        } else {
            $(".fixed_div").hide();
            $(".percentage_div").hide();
        }
    }    



    function discountOn(discount_on) {

        if (discount_on != '') {
            if (discount_on == 'product') {


                $("#product_div").show();
                $("#total_price_div").hide();
            } else {
                $("#product_div").hide();
                $("#total_price_div").show();

            }
        } else {
            $("#product_div").hide();
            $("#total_price_div").hide();
        }
    }

</script>
@stop

