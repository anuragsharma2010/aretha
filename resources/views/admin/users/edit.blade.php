  <!-- Content Wrapper. Contains page content -->
  @extends('layouts.default')

  @section('content')  

    <div class="content-wrapper">
     <section class="content-header">
        <h1>
            <?php echo $pageTitle; ?>
        </h1>
        @include('includes.admin.breadcrumb')
    </section>
     <!-- Main content -->
    <section class="content">
 <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.users.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                </h3>
            </div>
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-primary">
       
      	  {!! Form::model($user,['method'=>'patch','route'=>['admin.users.update',$user->id],'files'=>true]) !!}
      	 
      <div class="box-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            {!! Form::label(trans('admin.FIRST_NAME'),null,['class'=>'required_label']) !!}
                            {!! Form::text('first_name',null,['class'=>'form-control','placeholder'=>trans('admin.FIRST_NAME')]) !!}
                            <div class="error">{{ $errors->first('first_name') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.LAST_NAME'),null,['class'=>'required_label']) !!}
                            {!! Form::text('last_name',null,['class'=>'form-control','placeholder'=>trans('admin.LAST_NAME')]) !!}
                            <div class="error">{{ $errors->first('last_name') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->


        <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.EMAIL'),null,['class'=>'required_label']) !!}
                            {!! Form::label($user->email,null,['class'=>'form-control']) !!}
                            
                            
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
             
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.PHONE'),null,['class'=>'required_label']) !!}
                            {!! Form::text('phone',null,['class'=>'form-control','placeholder'=>trans('admin.PHONE')]) !!}
                            <div class="error">{{ $errors->first('phone') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                               <div class=" col-md-6">
                           <div class="form-group">
                                {!! Form::label(trans('admin.GENDER'),null,['class'=>'required_label']) !!}
                                        
                                         <div class="form-radio"> 
                            <?php $gender = Config::get('global.gender'); ?>
                            <label> {!! Form::radio('gender',Config::get('global.gender.male'),1, array('class' => 'minimal category_type '))!!} &nbsp; {{trans('admin.MALE')}}</label>
                            <label> {!! Form::radio('gender',  Config::get('global.gender.female'),0, array('class' => 'minimal category_type' )) !!}  &nbsp;  {{trans('admin.FEMALE')}}</label>
                            </div>
                            <div class="error">{{ $errors->first('gender') }}</div>
                                         
                            </div><!-- /.form-group -->


                           </div><!-- /.col --> 
                       <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php $status_list = Config::get('global.status_list'); ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'form-control select2 autocomplete']) !!}
                </div><!-- /.form-group -->
                </div><!-- /.col -->
                </div><!-- /.row -->


            
</div><!-- /.box-body -->
          <div class="box-footer">
         <div class="pull-right">

                    {!!  Html::decode(Html::link(route('admin.users.index'),trans('admin.CANCEL'),['class'=>'btn btn-default'])) !!}
                    {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                </div>
          </div>
          <!-- /.box-footer -->
         {!! Form::close() !!}
      </div><!-- /.box -->
    </section><!-- /.content -->
    </div>
  @stop