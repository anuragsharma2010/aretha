<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                        {!!  Html::decode(Html::link(route('admin.faq_category.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn  btn-primary'])) !!}
                            {!!  Html::decode(Html::link(route('admin.faqs.create',['id'=>$category_id]),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%">@sortablelink('question', trans('admin.QUESTION'))</th>
                                    <th width="20%">@sortablelink('order_key', trans('admin.ORDER_KEY'))</th>
                                    
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="20%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$faqs_list->isEmpty())
                                @foreach ($faqs_list as $faqs)
                                <tr>
                                    <td>{{ ucfirst($faqs->question) }}</td>
                                    <td>{{ $faqs->order_key }}</td>
                                    
                                    <td>{{ date_val($faqs->created_at,DATE_FORMATE ) }}</td>
                                    <td>{{ date_val($faqs->updated_at,DATE_FORMATE ) }}</td>
                                    <td align="center">
                                        @if($faqs->status == 1)
                                        {!!  Html::decode(Html::link(route('admin.faqs.status_change',['id' => $faqs->id,'status'=>$faqs->status,'category_id'=>$category_id]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        @else
                                        {!!  Html::decode(Html::link(route('admin.faqs.status_change',['id' => $faqs->id,'status'=>$faqs->status,'category_id'=>$category_id]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        @endif
                                    {!!  Html::decode(Html::link(route('admin.faqs.edit',['id'=>$faqs->id,'category_id'=>$category_id]),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $faqs_list->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
