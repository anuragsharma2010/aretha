<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $pageTitle; ?>
        </h1>
        @include('includes.admin.breadcrumb')
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.address_books.index',$user_id),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                </h3>
            </div>
            {!! Form::model($address_list,['method'=>'post','route'=>['admin.address_books.update',$address_list->id,$user_id]]) !!}

            <div class="box-body">

                <div class="row">

                    <div class="col-md-12">
                     <div class="row">

                        <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.FIRST_NAME'),null,['class'=>'required_label']) !!}
                                {!! Form::text('first_name',null,['class'=>'form-control','placeholder'=>trans('admin.FIRST_NAME')]) !!}
                                <div class="error">{{ $errors->first('first_name') }}</div>
                            </div><!-- /.form-group -->
                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.LAST_NAME'),null,['class'=>'required_label']) !!}
                                {!! Form::text('last_name',null,['class'=>'form-control','placeholder'=>trans('admin.LAST_NAME')]) !!}
                                <div class="error">{{ $errors->first('last_name') }}</div>
                            </div><!-- /.form-group -->
                                   
                      </div><!-- /.row -->   

                      <div class="row">
                            <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.ADDRESS_1'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('address_1',null,['class'=>'form-control','placeholder'=>trans('admin.ADDRESS_1')]) !!}
                              <div class="error">{{ $errors->first('address_1') }}</div>
                            </div><!-- /.form-group -->            
                      </div><!-- /.row -->

                      <div class="row">
                            <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.ADDRESS_2'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('address_2',null,['class'=>'form-control','placeholder'=>trans('admin.ADDRESS_2')]) !!}
                              <div class="error">{{ $errors->first('address_2') }}</div>
                            </div><!-- /.form-group -->            
                      </div><!-- /.row -->
                                                                   
                         <div class="row">

                             <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.PIN_CODE'),null,['class'=>'required_label']) !!}
                                {!! Form::text('pin_code',null,['class'=>'form-control','placeholder'=>trans('admin.PIN_CODE')]) !!}
                                <div class="error">{{ $errors->first('pin_code') }}</div>
                            </div><!-- /.form-group -->

                             <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.CITY'),null,['class'=>'required_label']) !!}
                                {!! Form::text('city',null,['class'=>'form-control','placeholder'=>trans('admin.CITY')]) !!}
                                <div class="error">{{ $errors->first('city') }}</div>
                            </div><!-- /.form-group -->

                             <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.STATE'),null,['class'=>'required_label']) !!}
                                {!! Form::text('state',null,['class'=>'form-control','placeholder'=>trans('admin.STATE')]) !!}
                                <div class="error">{{ $errors->first('state') }}</div>
                            </div><!-- /.form-group -->

                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.MOBILE'),null,['class'=>'required_label']) !!}
                                {!! Form::text('mobile',null,['class'=>'form-control','placeholder'=>trans('admin.MOBILE')]) !!}
                                <div class="error">{{ $errors->first('mobile') }}</div>
                            </div><!-- /.form-group -->

                            <div class="col-md-1 form-group ">
                                {!! Form::label(trans('admin.IS_DEFAULT'),null) !!}
                                {!! Form::checkbox('is_default', 1,null,['class'=>'form-control','placeholder'=>trans('admin.IS_DEFAULT')]) !!}
                                <div class="error">{{ $errors->first('is_default') }}</div>
                            </div><!-- /.form-group -->
                      
                      
                        </div><!-- /.row -->
                          

                    <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                                        <?php $status_list = Config::get('global.status_list'); ?>
                                        {!! Form::select('status', $status_list, null, ['class' => 'form-control select2 autocomplete']) !!}
                            </div><!-- /.form-group -->
                           </div><!-- /.col -->
                      
                         </div><!-- /.row -->
                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="pull-right">

                    {!!  Html::decode(Html::link(route('admin.address_books.index',$user_id),trans('admin.CANCEL'),['class'=>'btn btn-default'])) !!}
                    {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                </div>
            </div>
            <!-- /.box-footer -->
            {!! Form::close() !!}
        </div><!-- /.box -->
    </section><!-- /.content -->
</div>

@stop