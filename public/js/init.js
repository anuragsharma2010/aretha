(function($){
  $(function(){

    $('.button-collapse').sideNav();

  }); // end of document ready
})(jQuery); // end of jQuery name space
{!! Html::script('js/jquery-2.1.1.min.js') !!}
    {!! Html::script('js/materialize.min.js') !!}
    {!! Html::script('js/init.js') !!}

    <script>
     $(document).ready(function(){
       $('.slider').slider({full_width: true});
       // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
       $('.modal').modal();
     });
  
     $('.dropdown-button').dropdown({
       inDuration: 300,
       outDuration: 225,
       constrain_width: false, // Does not change width of dropdown to that of the activator
       hover: true, // Activate on hover
       gutter: 0, // Spacing from edge
       belowOrigin: false, // Displays dropdown below the button
       alignment: 'left' // Displays dropdown with edge aligned to the left of button
    })       
   </script>
