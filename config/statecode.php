<?php 
return [
"DL"=>"Delhi",
'HR'=> 'Haryana', 
'UP'=> 'Uttar Pradesh',
'TN'=> 'Tamil Nadu', 
'KA'=> 'Karnataka', 
'GJ'=> 'Gujarat', 
'MH'=> 'Maharashtra',
'AP'=> 'Andhra Pradesh', 
'AR'=> 'Andhra Pradesh', 
'CH'=> 'Chandigarh', 
'GA'=> 'Goa', 
'MP'=> 'Madhya Pradesh', 
'PB'=> 'Punjab', 
'PY'=> 'Pondicherry', 
'RJ'=> 'Rajasthan', 
'AP'=> 'Arunachal Pradesh', 
'AS'=> 'Assam', 
'BR'=> 'Bihar', 
'CG'=> 'Chhattisgarh', 
'CT'=> 'Chhattisgarh', 
'GA'=> 'Goa', 
'HP'=> 'Himachal Pradesh', 
'JK'=> 'Jammu & Kashmir',
'JH'=> 'Jharkhand',
'KL'=> 'Kerela', 
'MP'=> 'Madhya Pradesh',
'MN'=> 'Manipur',
'ML'=> 'Meghalaya', 
'MZ'=> 'Mizoram',
'NL'=> 'Nagaland',
'OR'=> 'Orissa', 
'SK'=> 'Sikkim', 
'TR'=> 'Tripura', 
'WB'=> 'West Bengal', 
'AN'=> 'Andaman & Nicobar', 
'DN'=> 'Dadra and Nagar Haveli', 
'DD'=> 'Daman & Diu', 
'LD'=> 'Lakshadweep',
'UK'=> 'Uttarakhand', 
'UT'=> 'Uttarakhand', 
];