<!-- Content Wrapper. Contains page content --> 

<?php $__env->startSection('content'); ?>  

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $pageTitle; ?>
        </h1>
        <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="pull-right">  
                    <?php echo Html::decode(Html::link(route('admin.category.index',['id'=>$parent_id]),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])); ?>

                </h3>
            </div>
            <?php echo Form::open(['method'=>'post','route'=>['admin.category.store',$parent_id], 'files'=>true]); ?>  
            <div class="box-body">

                <div class="row">

                    
                        <div class="col-md-6 form-group ">
                            <?php echo Form::label(trans('admin.NAME'),null,['class'=>'required_label']); ?>

                            <?php echo Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]); ?>

                            <div class="error"><?php echo e($errors->first('name')); ?></div>
                        </div><!-- /.form-group -->


                </div><!-- /.row -->       
                <div class="row">
                         <div class="col-md-6 form-group ">
                            <?php echo Form::label(trans('admin.CATEGORY_TYPE'),null,['class'=>'required_label']); ?>

                           
                               <div class="form-radio"> 
                            <?php $category_type = Config::get('global.category_type'); ?>
                            <label> <?php echo Form::radio('category_type',Config::get('global.category_type.main_category'),($parent_id=='')?1:0, array('class' => 'minimal category_type ')); ?> &nbsp; <?php echo e(trans('admin.MAIN_CATEGORY')); ?></label>
                            <label> <?php echo Form::radio('category_type',  Config::get('global.category_type.sub_category'),($parent_id!='')?1:0, array('class' => 'minimal category_type' )); ?>  &nbsp;  <?php echo e(trans('admin.SUB_CATEGORY')); ?></label>
                            </div>
                            <div class="error"><?php echo e($errors->first('category_type')); ?></div>
                        </div><!-- /.form-group -->
                        </div><!-- /.row -->
                <div class=" display_hidden">
                  <div class="row"> 
                <div class="col-md-6">
                    <div class="form-group    ">
                            <?php echo Form::label(trans('admin.PARENT_CATEGORY'),null,['class'=>'required_label']); ?>

                           <?php  

                            $category_list    =   array('' => trans('admin.PLEASE_SELECT')) + BasicFunction::getParentCategory();
                            
                           ?>
                            <?php echo Form::select('parent_id', $category_list,$parent_id,['class'=>'select2 form-control autocomplete']); ?>

                            <div class="error"><?php echo e($errors->first('parent_id')); ?></div>
                         
                        </div><!-- /.form-group -->
                        </div><!-- /.col-md-6 -->
                        </div><!-- /.row -->

  <div class="row"> 
           <div class="form-group col-md-6">
                            <?php echo Form::label(trans('admin.ATTRIBUTES'),null,['class'=>'required_label']); ?>

                          
                            <?php echo Form::select('attributes[]', $attribute_list,
null, ['class' => 'select2 form-control','multiple' => 'multiple','data-
placeholder'=>'Select a Attributes']); ?>                   
   <div class="error"><?php echo e($errors->first('attributes')); ?></div>
         </div><!--
/.form-group -->


        
            </div><!-- /.row -->
            </div><!-- /.display_hidden -->
            <div class="row main_category_type " >
                
                    <div class="form-group col-md-6">
                            <?php echo Form::label(trans('admin.CATEGORY_FOR'),null,['class'=>'required_label']); ?>

                            <?php $status_list = array('' => trans('admin.PLEASE_SELECT')) + array_map('ucfirst', Config::get('global.category_for')); ?>
                            <?php echo Form::select('category_for', $status_list, null, ['class' => 'select2 form-control']); ?>

                               <div class="error"><?php echo e($errors->first('category_for')); ?></div>
                    </div><!-- /.form-group -->
            </div>

                    <div class="row">
                            <div class="col-md-12 form-group ">
                                <?php echo Form::label(trans('admin.META_TITLE'),null,['class'=>'required_label']); ?>

                                <?php echo Form::text('meta_title',null,['class'=>'form-control','placeholder'=>trans('admin.META_TITLE')]); ?>

                                <div class="error"><?php echo e($errors->first('meta_title')); ?></div>
                            </div><!-- /.form-group -->
                                
                    </div><!-- /.row --> 
                    <div class="row">
                            <div class="col-md-12 form-group ">
                                <?php echo Form::label(trans('admin.META_KEYWORDS'),null,['class'=>'required_label']); ?>

                                <?php echo Form::textarea('meta_keywords',null,['class'=>'form-control','placeholder'=>trans('admin.META_KEYWORDS')]); ?>

                                <div class="error"><?php echo e($errors->first('meta_keywords')); ?></div>
                            </div><!-- /.form-group -->
                                
                    </div><!-- /.row -->   
                    <div class="row">
                            <div class="col-md-12 form-group ">
                                <?php echo Form::label(trans('admin.META_DESCRIPTION'),null,['class'=>'required_label']); ?>

                                <?php echo Form::textarea('meta_description',null,['class'=>'form-control','placeholder'=>trans('admin.META_DESCRIPTION')]); ?>

                                <div class="error"><?php echo e($errors->first('meta_description')); ?></div>
                            </div><!-- /.form-group -->
                                
                    </div><!-- /.row -->
                    
                    <div class="col-md-6 form-group ">
                                <?php echo Form::label(trans('admin.IMAGE'),null,['class'=>'required_label']); ?>

                                <?php echo Form::file('image'); ?>

                                <div class="error"><?php echo e($errors->first('image')); ?></div>
                    </div><!-- /.form-group -->

                    <div class="row">            
                        <div class="form-group col-md-6">
                            <?php echo Form::label(trans('admin.STATUS'),null,['class'=>'required_label']); ?>

                            <?php $status_list = Config::get('global.status_list'); ?>
                            <?php echo Form::select('status', $status_list, null, ['class' => 'select2 form-control']); ?>

                        </div><!-- /.form-group -->


        
            </div><!-- /.row -->


            <div class="box-footer">

                <div class="pull-right">
                    <?php echo Form::reset(trans('admin.RESET'),['class' => 'btn btn-default ']); ?> 
                    <?php echo Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info ']); ?>

                </div>
            </div>
                 </div>
            <!-- /.box-footer -->

            <?php echo Form::close(); ?>


        </div><!-- /.box -->


    </section><!-- /.content -->
</div>
<script type="text/javascript">
    
$(document).ready(function() {
 ischecedredio();
    $('.minimal').on('ifChecked', function(event){
          
            ischecedredio();

     });

});


function ischecedredio(){

 var checkedValue =    $('.category_type:checked').val();

           if(checkedValue==1){
            $('.display_hidden').show();
            $('.main_category_type').hide();

           }else{
                $('.display_hidden').hide();
                   $('.main_category_type').show();

           }
   

}
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>