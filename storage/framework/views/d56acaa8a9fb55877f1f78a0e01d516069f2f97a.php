
                   
       
                    <div style="font-family: Conv_Raleway-Regular,Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #211f1f; font-size: 13px;">
                       <table class="table-row" style="table-layout: auto; padding-right: 24px; padding-left: 24px; width: 600px; background-color: #ffffff;" bgcolor="#FFFFFF" width="600" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                          <tr>
                            <td style="color:#8e8e8e;line-height:15px;text-align:left">
                            <h1 style="color:#757575; font-size:18px">Hello <?php echo e(ucwords($user['first_name'].' '.$user['last_name'])); ?>!</h1>
                         <h3 style="color:#757575">Thank You for Shopping!</h3>
                            <p>Your Order  <span style="color:#48b5dc">#<?php echo e($order_id); ?></span> has been placed successfully!</p>
                            <p>Just to remind you a couple of things:</p>
                            </td>
                           </tr>
                             <tr style="font-family: Conv_Raleway-Regular,Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #211f1f; font-size: 13px;">
                                <td class="table-row-td" style="padding-right: 16px; padding-bottom: 12px; font-family:Conv_Raleway-Regular, Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #211f1f; font-size: 13px; font-weight: normal;" valign="top" align="left">
                              <div style="color:#8e8e8e;background-color:#f5f5f5;font-family:Montserrat,sans-serif,Helvetica,Arial;font-size:14px;margin-bottom:15px;margin-top:5px;padding-top:10px;padding-bottom:10px">
<ul>
<li>Orders within India take about 7-8 working days. We will be sending you the tracking details on your phone and email once we dispatch your order.</li>
<li>Please allow atleast 24 hours for the tracking link to be activated.</li>
</ul>

</div> 
<div style="text-align:left;color:#8e8e8e;font-family:Montserrat,sans-serif,Helvetica,Arial;font-size:14px">
<p>
For any queries, please write us at <b><a style="color:#48b5dc;" href="<?php echo e(Configure('CONFIG_FROMEMAIL')); ?>" target="_blank">info@fabivo.com</a></b> </p>
</div>  
<div style="margin-bottom:45px;margin-top:30px;margin-left:0px;text-align:center">
<a style="font-family:Montserrat,sans-serif,Helvetica,Arial;text-align:center;text-decoration:none" href="<?php echo e(WEBSITE_URL); ?>myaccount/order-detail/<?php echo e($order_id); ?>" target="_blank"><span style="text-align:center;width:200px;background:#2298c2;padding-top:7px;color:white;font-size:20px;border:1px solid #2298c2;border-radius:2px;padding-bottom:7px;margin-bottom:5px;padding-left:15px;padding-right:15px">View Order</span></a>
</div>                                  </td>
                              
                             </tr>
                             
                             <tr>
                                <td>
                                   <h3 style="color:#757575">Shipping Address:</h3>
                                    <div style="text-align:left;color:#9b9b9b;width:50%">
                                    <?php echo e(ucwords($first_name.' '.$last_name)); ?> <br> <?php echo e($address_1); ?>, <?php echo e($address_2); ?>, <?php echo e($city); ?> <br> <?php echo e($state); ?>-<?php echo e($pin_code); ?> <br> India<br>
                                    Phone:-<?php echo e($mobile); ?>

                                    </div>
                          <h3 style="color:#757575">Order Summary:</h3>
                                    <table width="100%" border="0" cellpadding="10" cellspacing="0" style="letter-spacing:0.2px;float:left;margin:0 auto;font-family:Montserrat,sans-serif,Helvetica,Arial;font-size:14px;color:#fff;padding-bottom:5px;border-bottom:1px solid #9d9d9d;">
                                    <tbody align="right" style="display:block;padding:10px 0">
                                    </tbody>
                                    <thead>
                                    <tr style="background:#444547">
                                    <th style="width:240px;padding:0 5px">Product</th>
                                    <th style="width:140px;padding:0 5px">Size</th>
                                    <th style="width:40px;padding:0 5px">Qty</th>
                                    <th style="width:140px;">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody style="color:#9b9b9b">
                                      <?php foreach($order_detail as $product): ?>
                                    <tr>
                                    <td style="width:240px;padding:0 5px;font-weight:bold;color:#a0a0a0"><?php echo e(ucfirst($product['product']->title)); ?></td>
                                    <td style="width:140px;padding:0 5px;color:#a0a0a0">
                                   <?php echo e($product['product_size']); ?>

                                  
                                 
                                    </td>
                                    <td style="width:40px;padding:0 5px;color:#a0a0a0"><?php echo e($product['quantity']); ?></td>
                                    <td style="width:140px;text-align:right;color:#a0a0a0"><?php echo e(display_price($product['price'])); ?></td>
                                    </tr>
                                     <?php endforeach; ?>
                                    </tbody>
                                    
                                    </table>
  
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-top:20px">
                                    <tbody align="right" style="display:block;padding-bottom:15px;border-bottom:1px solid #9d9d9d;color:#757575">
                                    <tr>
                                    <td style="text-align:left">Subtotal:</td>
                                    <td style="text-align:right"><?php echo e(display_price($total_price)); ?></td>
                                    </tr>
                                      <?php if($shipping_charge > 0): ?>
                                            <tr>
                                    <td style="text-align:left">Shipping Charge:</td>
                                    <td style="text-align:right"><?php echo e(display_price($shipping_charge)); ?></td>
                                    </tr>
                                      <?php endif; ?>  
                                       <?php if($total_tax > 0): ?>
                                       <?php $tax_description = unserialize($tax_description); 
                                      
                                    ?>
                                         <?php foreach($tax_description as $tax): ?>
                                                 <tr>
                                    <td style="text-align:left"><?php echo e($tax['name']); ?></td>
                                    <td style="text-align:right"><?php echo e(display_price($tax['amount'])); ?></td>
                                    </tr>

                                         <?php endforeach; ?>
                                      <?php endif; ?> 

                                       <?php if($is_cod): ?>
                                        <tr>
                                    <td style="text-align:left">Cod Charge</td>
                                    <td style="text-align:right"><?php echo e(display_price($cod_money)); ?></td>
                                    </tr>

                                        <?php endif; ?> 
                                      <?php if($is_discount > 0): ?>
                                    <tr>
                                    <td style="text-align:left">Discount:</td>
                                    <td style="text-align:right;color:#7ad321">- <?php echo e(display_price($discount)); ?></td>
                                    </tr>
                                                   <?php endif; ?>     
                                                   <?php if($wallet_amount > 0): ?>
                                    <tr>
                                    <td style="text-align:left">Wallet:</td>
                                    <td style="text-align:right;color:#7ad321">-<?php echo e(display_price($wallet_amount)); ?></td>
                                    </tr>
                                    <?php endif; ?>
                                    </tbody>
                                    </table>
                                    <h3 style="text-align:right;padding-right:10px;color:#4a4a4a">Payble Amount:<?php echo e(display_price($payble_amount-$wallet_amount)); ?></h3>
                                       <?php if($is_cash_back == 1): ?>
                                    <h4 style="text-align:left;padding-left:5px;color:#7ad321">
                                        Cash back of <?php echo e(display_price($cash_back_amount)); ?> for promo code <?php echo e(strtoupper($coupon_code)); ?> has been successfully added to your fabivo wallet on after shipped successfully. Happy Shopping!

                                    </h4>
                                             <?php endif; ?>     
                                </td>
                             </tr>
                      
                          </tbody>
                       </table>
                    </div>
                   