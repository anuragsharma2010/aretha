<!-- Content Wrapper. Contains page content -->


<?php $__env->startSection('content'); ?>  

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $pageTitle; ?>
        </h1>
        <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="pull-right">  
                    <?php echo Html::decode(Html::link(route('admin.blocks.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])); ?>

                </h3>
            </div>
            <?php echo Form::model($blocks,['method'=>'patch','route'=>['admin.blocks.update',$blocks->id],'files'=>true]); ?>


            <div class="box-body">

                <div class="row">

             <div class="col-md-12">
                     <div class="row">
                             <div class="col-md-6 form-group ">
                                <?php echo Form::label(trans('admin.POSITION'),null,['class'=>'']); ?>

                                <?php echo Form::label($blocks->position,null,['class'=>'form-control']); ?>

                                <div class="error"><?php echo e($errors->first('position')); ?></div>
                            </div><!-- /.form-group -->

                            <div class="col-md-6 form-group ">
                                <?php echo Form::label(trans('admin.TITLE'),null,['class'=>'required_label']); ?>

                                <?php echo Form::text('title',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE')]); ?>

                                <div class="error"><?php echo e($errors->first('title')); ?></div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->    
                        
                        <div class="row">
                             
                            <div class="col-md-6 form-group ">
                                <?php echo Form::label(trans('admin.TITLE2'),null,['class'=>'']); ?>

                                <?php echo Form::text('title_2',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE2')]); ?>

                                <div class="error"><?php echo e($errors->first('title2')); ?></div>
                            </div><!-- /.form-group -->
                            
                            <div class="col-md-6 form-group ">
                                <?php echo Form::label(trans('admin.TITLE3'),null,['class'=>'']); ?>

                                <?php echo Form::text('title_3',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE3')]); ?>

                                <div class="error"><?php echo e($errors->first('title3')); ?></div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->    

                           <div class="row">
                              <div class="col-md-12 form-group ">
                                <?php echo Form::label(trans('admin.DESCRIPTION'),null,['class'=>'required_label']); ?>

                                <?php echo Form::text('description',null,['class'=>'form-control','placeholder'=>trans('admin.DESCRIPTION')]); ?>

                              <div class="error"><?php echo e($errors->first('description')); ?></div>
                      </div><!-- /.form-group -->
                                
                      </div><!-- /.row -->
                    <div class="row">

                        <div class="col-md-6  ">
                          <div class="row">
                            <div class="form-group  col-md-9">
                            <?php echo Form::label(trans('admin.IMAGE'),null,['class'=>'required_label']); ?>

                            <?php echo Form::file('image'); ?>

                          <div class="error"><?php echo e($errors->first('image')); ?></div>
                          </div><!-- /.form-group -->

                          <div class=" col-md-3">
                          <?php echo BasicFunction::showImage(BLOCKS_IMAGES_UPLOAD_DIRECTROY_PATH,BLOCKS_IMAGES_ONTHEFLY_IMAGE_PATH,$blocks->image,array('width'=>'100', 'height'=>'100','zc'=>2)); ?>

                            </div><!-- /.col --> 

                            </div><!-- /.ROW-->

                        </div><!-- /.col --> 
                                          
                        <div class="form-group col-md-6 ">
                            <?php echo Form::label(trans('admin.CATEGORY'),null,['class'=>'required_label']); ?>

                           <?php  

                            $category_list    =   array('' => trans('admin.PLEASE_SELECT')) + BasicFunction::getAllChildCategory();
                           
                           ?>
                           <?php echo Form::select('category_id', $category_list, null, ['class' => 'form-control select2','id'=>'category_id']); ?>

                            <div class="error"><?php echo e($errors->first('category_id')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.row -->
                         
                        
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="pull-right">

                    <?php echo Html::decode(Html::link(route('admin.blocks.index'),trans('admin.CANCEL'),['class'=>'btn btn-default'])); ?>

                    <?php echo Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info ']); ?>

                </div>
            </div>
            <!-- /.box-footer -->
            <?php echo Form::close(); ?>

        </div><!-- /.box -->
    </section><!-- /.content -->
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>