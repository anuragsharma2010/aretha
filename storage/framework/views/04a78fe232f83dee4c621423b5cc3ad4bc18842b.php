<?php $__env->startSection('content'); ?>  
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
   <section class="content-header">
        <h1>
            <?php $admin = adminUser();

             echo trans('admin.HELLO').' '.ucfirst($admin->first_name);
             ?>
        </h1>
       
    </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?php echo e($users_count); ?></h3>
            <p>Users Registered</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo e(route('admin.users.index')); ?>" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3><?php echo e($product_count); ?></h3>
            <p>Product  Pages</p>
          </div>
          <div class="icon">
            <i class="fa fa-product-hunt" aria-hidden="true"></i>
          </div>
          <a href="<?php echo e(route('admin.products.index')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3><?php echo e($coupon_count); ?></h3>
            <p>Coupon  Pages</p>
          </div>
          <div class="icon">
            <i class="fa fa-tag" aria-hidden="true"></i>
          </div>
          <a href="<?php echo e(route('admin.coupons.index')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?php echo e($cms_count); ?></h3>
            <p>Cms Pages</p>
          </div>
          <div class="icon">
            <i class="fa fa-file" aria-hidden="true"></i>
          </div>
          <a href="<?php echo e(route('admin.cms.index')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

   
    </div><!-- /.row -->
    <!-- Main row -->
        <div class="row">
          <div class="col-lg-6 col-xs-6">
           <div class="row">
                  <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3><?php echo e(display_price($today_sel)); ?></h3>
            <p>Today Sales</p>
          </div>
          <div class="icon">
            <i class="fa fa-inr" aria-hidden="true"></i>
          </div>
          <a href="<?php echo e(route('admin.orders.index')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?php echo e(display_price($total_sel)); ?></h3>
            <p>Total Sales</p>
          </div>
          <div class="icon">
            <i class="fa fa-inr" aria-hidden="true"></i>
          </div>
          <a href="<?php echo e(route('admin.orders.index')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?php echo e($newlatterCount); ?></h3>
            <p>Newsletter Subscriber</p>
          </div>
          <div class="icon">
            <i class="fa fa-envelope" aria-hidden="true"></i>
          </div>
          <a href="<?php echo e(route('newlatters')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3><?php echo e($total_order); ?></h3>
            <p>Total Order</p>
          </div>
          <div class="icon">
            <i class="fa fa-truck" aria-hidden="true"></i>
          </div>
          <a href="<?php echo e(route('admin.orders.index')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

 </div>
 </div>
      <div class="col-lg-6 col-xs-6">
             <?php 

              
                $options1 = [
                    

                    'height'=>400,
                    'title'=>"Today's Order"
                ];

          

                ?>
          
                <?php echo ChartManager::setChartType('piechart-chart')
                        ->setOptions($options1)
                        ->setCols($cols1)
                        ->setRows($rows1)
                        ->render(); ?>

          

          
                  


 </div> 
          </div> 
           <div class="row">
      <div class="col-lg-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Recent Orders</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body " style="padding: 5px; ">
                <table id="example2" class="table table-bordered table-striped example2">
                            <thead>
                                <tr>
                          
                                    <th width="10%"><?php echo e(trans('admin.ORDER_ID')); ?></th>
                                    <th width="15%"><?php echo e(trans('admin.USER')); ?></th>
                                    <th width="10%"><?php echo e(trans('admin.PAYMENT_BY')); ?></th>
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('payble_amount', trans('admin.PAYBLE_AMOUNT')));?></th>
                                   
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('order_status', trans('admin.ORDER_STATUS')));?></th>
                              
                                      
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('shipped_by', trans('admin.SHIPPED_BY')));?></th>
                                      
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('payment_status', trans('admin.PAYMENT_STATUS')));?></th>

                                    <th width="7%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('created_at', trans('admin.CREATED_AT')));?></th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!$orders_list->isEmpty()): ?>
                                <?php foreach($orders_list as $orders): ?>
                                <tr>
                                   
                                    <td> <?php echo e($orders->order_id); ?> </td>
                                    <td> <?php echo e(ucwords($orders->user->first_name.' '.$orders->user->last_name)); ?> </td>
                                    <td> <?php echo e(ucfirst($orders->payment_by)); ?> </td>
                                    <td> <?php echo e(display_price($orders->payble_amount)); ?> </td>
                                   
                                      <td> <?php echo e(ucfirst($orders->order_status)); ?> </td>
                                      

                                  
                                      <td> <?php echo e(ucfirst($orders->shipped_by)); ?> </td>
                                    
                                    <td> <?php echo e(ucfirst($orders->payment_status)); ?> </td>
                                    <td> <?php echo e(date_val($orders->created_at,DATE_FORMATE )); ?> </td>
                                    
                                  
                                    <?php endforeach; ?>
                                    <?php else: ?>

                                <tr><td colspan="7"><div class="data_not_found"> Data Not Found </div></td></tr>


                                <?php endif; ?>

                            </tbody>
                        </table>
                             </div>
            <!-- /.box-body -->
   
            <!-- /.box-footer -->
          </div> 
          </div> 
          </div> 
        <div class="row">
      <div class="col-lg-12 col-xs-12">
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">All Orders</h3>

              <div class="box-tools pull-right">
                <a href="<?php echo e(route('dashboard', ['type'=>'day'])); ?>" class="btn  btn-<?php echo e(($type=='day')?'primary':'default'); ?>" >Day
                </a>
                 <a href="<?php echo e(route('dashboard', ['type'=>'month'])); ?>" class="btn  btn-<?php echo e(($type=='month')?'primary':'default'); ?>" >
                  Month
                </a>
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body " style="padding: 5px; ">
               <?php $options = [
                    'title' => 'Orders',
                    'chart' => [
                        'title' => 'Orders'
                    ],
                    'chartArea' => ['width' => '100%'],
                    'hAxis' => [
                        'title' => 'Total Population',
                        'minValue' => 0
                    ],
                    'vAxis' => [
                        'title' => 'City'
                    ],
                    'bars' => 'vertical', //required if using material chart
                    'axes' => [
                        'y' => [0 => ['side' => 'left']]
                    ],
                    'height'=>350
                ];

            
                ?>

<?php echo ChartManager::setChartType('bar-chart')
                        ->setOptions($options)
                        ->setCols($cols)
                        ->setRows($rows)
                     ->render(); ?>

                 
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
   
            <!-- /.box-footer -->
          </div> 
          </div> 
          </div> 
          
  </section><!-- /.content -->






</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>