<!-- Content Wrapper. Contains page content -->


<?php $__env->startSection('content'); ?>  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            <?php echo e($pageTitle); ?>

        </h1>
        <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            <?php echo Html::decode(Html::link(route('admin.products.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])); ?>


                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('title', trans('admin.TITLE')));?></th>
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('price', trans('admin.PRICE')));?></th>
                                    
                                    <th width="20%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('category', trans('admin.CATEGORY')));?></th>
                                    
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('created_at', trans('admin.CREATED_AT')));?></th>
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('updated_at', trans('admin.UPDATED_AT')));?></th>
                                    <th  width="20%" align="center"><?php echo e(trans('admin.ACTION')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!$productslist->isEmpty()): ?>
                                <?php foreach($productslist as $product): ?>
                                <tr>
                                    <td><?php echo e(ucfirst($product->title)); ?></td>
                                    <td><?php echo e(display_price($product->price)); ?></td>
                                     
                                    <td><?php echo e(ucfirst($product->category->name)); ?></td>
                                    
                                    <td><?php echo e(date_val($product->created_at,DATE_FORMATE )); ?></td>
                                    <td><?php echo e(date_val($product->updated_at,DATE_FORMATE )); ?></td>
                                    <td align="center">
                                        <?php if($product->status == 1): ?>
                                        <?php echo Html::decode(Html::link(route('admin.products.status_change',['id' => $product->id,'status'=>$product->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])); ?>

                                        <?php else: ?>
                                        <?php echo Html::decode(Html::link(route('admin.products.status_change',['id' => $product->id,'status'=>$product->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])); ?>

                                        <?php endif; ?>
                                        <?php echo Html::decode(Html::link(route('admin.products.edit', $product->id),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])); ?> 

                                         <?php echo Html::decode(Html::link(route('admin.products.products_image', $product->id),"<i class='fa  fa-image'></i>",['class'=>'btn btn-warning','data-toggle'=>'tooltip','title'=>trans('admin.IMAGES')])); ?>  

                                          <?php echo Html::decode(Html::link(route('admin.products.manage_stock', ['id'=> $product->id,'from'=>'index']),"<i class='fa  fa-cart-plus'></i>",['class'=>'btn btn-info','data-toggle'=>'tooltip','title'=>trans('admin.MANAGE_STOCK')])); ?> 

                                    </td>
                                    <?php endforeach; ?>
                                    <?php else: ?>

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>


                                <?php endif; ?>

                            </tbody>
                        </table>
                        <?php echo $productslist->appends(Input::all('page'))->render(); ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<!-- /.content-wrapper -->

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>