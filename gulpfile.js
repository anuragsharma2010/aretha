var elixir = require('laravel-elixir');
var webpack = require('webpack')
require('laravel-elixir-webpack-ex');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
   // mix.sass('app.scss');

    mix.webpack(
        {
            app: 'main.ts',
            vendor: 'vendor.ts'
        },
        {
            module: {
                loaders: [
                    {
                        test: /\.ts$/,
                        loader: 'ts-loader',
                        exclude: /node_modules/
                    },
                    {
                        test: /\.html$/,
                        loader: 'raw-loader'
                    },
                    {
                        test: /\.scss$/,
                        loaders: ["raw", "sass"]
                    }
                ]
            },
            plugins: [
                new webpack.optimize.CommonsChunkPlugin({
                    name: 'app',
                    filename: 'app.js',
                    minChunks: 5,
                    chunks: [
                        'app'
                    ]
                }),
                new webpack.optimize.CommonsChunkPlugin({
                    name: 'vendor',
                    filename: 'vendor.js',
                    minChunks: Infinity
                }),
                new webpack.ProvidePlugin({
                    '__decorate': 'typescript-decorate',
                    '__extends': 'typescript-extends',
                    '__param': 'typescript-param',
                    '__metadata': 'typescript-metadata'
                }),
                   /* new webpack.optimize.UglifyJsPlugin({
                        compress: {
                          warnings: true,
                          unused: true,
                          dead_code: true,
                          drop_console: true,
                        },
                        output: {
                          comments: false,
                        }
              })*/
                  
            ],
            resolve: {
                extensions: ['', '.js', '.ts']
            },
            debug: true,
            devtool: 'source-map'
        },
        'public/js',
        'resources/assets/typescript'
    );

    mix.version([
        'js/sdk.js',
        'js/jquery.js',
        'js/jquery.easing.1.3.js',
        'js/materialize.min.js',
        'js/jquery.blockUI.js',
        'js/owl.carousel.js',
        'js/slick.js',
        'js/jquery.mCustomScrollbar.concat.min.js',
        'js/jquery.sticky.js',
        'js/app.js',
        'js/vendor.js',
        'js/init.js',
        'js/index.js',
        'js/jquery.flexslider.js',
        'js/all.js',
        'css/all.css',
        'css/materialize.min.css',
        'css/font-awesome.min.css',
        'css/style.css',
        'css/style-responsive.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/slick.css',
        'css/slick-theme.css',
        'css/jquery.mCustomScrollbar.css',
        'css/custom.css',
        'css/tracker.css',
        'css/new-custom.css',
        'css/flexslider.css',
    ]);
    
    mix.scripts([
        'sdk.js',
        'jquery.js',
        'jquery.easing.1.3.js',
        'materialize.min.js',
        'jquery.blockUI.js',
        'owl.carousel.js',
        'slick.js',
        'jquery.mCustomScrollbar.concat.min.js',
        'jquery.sticky.js',
        'vendor.js',
        'app.js',
        'init.js',
        'index.js',
        'jquery.flexslider.js',
    ], 'public/js/all.js', 'public/js');
    
    mix.styles([
        'materialize.min.css',
        'font-awesome.min.css',
        'style.css',
        'style-responsive.css',
        'owl.carousel.css',
        'owl.theme.css',
        'slick.css',
        'slick-theme.css',
        'jquery.mCustomScrollbar.css',
        'custom.css',
        'tracker.css',
        'font-awesome.css',
        'new-custom.css',
        'flexslider.css',
    ], 'public/css/all.css', 'public/css');

    mix.browserSync({
        files: [
            "public/js/*",
            "public/css/*"
        ],
     proxy: "localhost:8050"
    });

});
