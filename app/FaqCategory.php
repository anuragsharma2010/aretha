<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class FaqCategory extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faq_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];
     
     
 /*
      * get child  category  data
      *
      * */
    public function faqs()
    {   
         $instance = $this->hasMany('App\Faq', 'category_id');
         $instance->getQuery()->where('status','=', 1)->orderBy('order_key', 'asc');
         return $instance;
    }
}
