<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class ProfileWishlist extends Model{
	use  Sortable;
	protected $table = 'profile_wishlist';

	protected $fillable = ['user_id', 'product_id', 'created_at', 'updated_at'];

	 public function product(){
        return $this->belongsTo('App\Product');
    }

}
