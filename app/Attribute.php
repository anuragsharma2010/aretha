<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Attribute extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attributes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];
     
     
  
  
/*
      * get child  category  data
      *
      * */
    public function AttributeValue()
    {
        $instance = $this->hasMany('App\AttributeValue', 'attribute_id');
       $instance->getQuery()->where('status','=', 1)->orderBy('order_key', 'asc')->select('id','slug','name','value','attribute_id');

        return $instance;
    }

}
