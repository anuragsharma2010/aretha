<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class userOtp extends Model {
    protected $table = 'user_otp_logs';
    protected $guarded = ['id'];
}
