<?php

namespace App\Providers;
use Config;

use Illuminate\Support\ServiceProvider;

//use App\Helpers\VisitorHelper;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
        Config::set('mail.driver',Config::get('settings.CONFIG_MAIL_DRIVER'));
        Config::set('mail.host',Config::get('settings.CONFIG_MAIL_HOST'));
        Config::set('mail.port',Config::get('settings.CONFIG_MAIL_PORT'));
        Config::set('mail.password',Config::get('settings.CONFIG_MAIL_PASSWORD'));
        Config::set('mail.username',Config::get('settings.CONFIG_MAIL_USERNAME'));

        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
       
    }
}
