<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Product extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];
     
      public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }  
      public function attributesValue()
    {
        return $this->hasMany('App\ProductAttributeValue', 'product_id');




    }
    
        /**
     * Get the phone record associated with the user.
     */
    public function getImage()
    {
        $instance = $this->hasOne('App\ProductImage','product_id');
        $instance->where('status','=', 1)->orderBy('is_main', 'desc')->orderBy('created_at', 'desc');
         return $instance;
    }
    public function getImages()
    {
        $instance = $this->hasMany('App\ProductImage','product_id');
        $instance->where('status','=', 1)->orderBy('is_main', 'desc')->orderBy('created_at', 'desc');
         return $instance;
    }
  

}
