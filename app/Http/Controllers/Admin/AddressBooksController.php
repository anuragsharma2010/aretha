<?php
namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AddressBook;
use App\Helpers\BasicFunction;
use App\CategoryAttribute;
use Validator;
use App\User;
use Config;
use Input;
use view;

class AddressBooksController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index($id) {
        $address_list = AddressBook::where("user_id",$id)->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));

        $pageTitle = trans('admin.ADDRESSBOOKS');
        $title = trans('admin.ADDRESSBOOKS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADDRESSBOOKS'));
        setCurrentPage('admin.address_books');
       
        return view('admin.address_books.index', compact('address_list', 'pageTitle', 'title', 'breadcrumb','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function create($id) {

        $pageTitle = trans('admin.ADDRESSBOOKS');
        $title = trans('admin.ADDRESSBOOKS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ADDRESSBOOKS')] = array('admin.address_books.index',array('id'=>$id));


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_ADDRESS_BOOKS'));
 

        return view('admin.address_books.create', compact('pageTitle', 'title', 'breadcrumb','id'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request,$id) {
        $address_booksObj = new AddressBook();
        //pr($request->all());
        //die;
        if($request->get('is_default'))
        {
        AddressBook::where('user_id', '=', $id)->update(['is_default' => 0]);
        }

        $validator = validator::make($request->all(), [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'address_1' => 'required|max:255',
                    'address_2' => 'required|max:255',
                    'pin_code' => 'required|max:6',
                    'city' => 'required|max:255',
                    'state' => 'required|max:255',
                    'mobile' => 'required|phone',
             
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\AddressBooksController@create',$id)
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
        $input['user_id'] = $id;

        $address_list = $address_booksObj->create($input);

        return redirect()->action('Admin\AddressBooksController@show', getCurrentPage('admin.address_books'))->with('alert-sucess', trans('admin.ADDRESS_BOOKS_ADD_SUCCESSFULLY'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$user_id) {                                                                                                                                                     
    if ($id == '') {
            return $this->InvalidUrl();
        }
        $address_list = AddressBook::find($id);
     
        //dd($address_list);
        if (empty($address_list)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_ADDRESSBOOK');
        $title = trans('admin.EDIT_ADDRESSBOOK');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ADDRES')] =  array('admin.address_books.index',array('id'=>$user_id));

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_ADDRESSBOOK'));

        return view('admin.address_books.edit', compact('address_list', 'pageTitle', 'title', 'breadcrumb','user_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update($id,$user_id,Request $request) {
      
      /*echo '<pre>';
      print_r($request->all());
      die;*/

        $validator = validator::make($request->all(), [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'address_1' => 'required|max:255',
                    'address_2' => 'required|max:255',
                    'pin_code' => 'required|max:6',
                    'city' => 'required|max:255',
                    'state' => 'required|max:255',
                    'mobile' => 'required|min:10',
             
        ]);

        if ($validator->fails()) {
            return redirect()->action('Admin\AddressBooksController@edit',array('id' => $id, 'user_id' => $user_id))
                            ->withErrors($validator)
                            ->withInput();
        }
        $input = $request->all();
       
        $address_list = AddressBook::findOrFail($id);    
        $address_list->fill($input)->save();
        $this->setIsDefault($id,$user_id);

        $getCurrentPage['id'] = $user_id;
        $getCurrentPage = $getCurrentPage +getCurrentPage('admin.address_books');
        return redirect()->action('Admin\AddressBooksController@index', $getCurrentPage)->with('alert-sucess', trans('admin.ADDRESS_UPDATE_SUCCESSFULLY'));
    }

     public function setIsDefault($id,$user_id){
        if(!empty($user_id) && !empty($id))
        {   
            AddressBook::where('user_id', '=', $user_id)->update(['is_default' => 0]);

            $address_books = AddressBook::where('id', '=', $id)->first();
            $address_books->is_default = 1;
            $address_books->save();

      }
    }


     public function status_change($id, $status,$user_id) {
       

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $address_books = AddressBook::where('id', '=', $id)->first();
        $address_books->status = $new_status;
        $address_books->save();

     
        $getCurrentPage['id'] = $user_id;
        $getCurrentPage = $getCurrentPage +getCurrentPage('admin.address_books');
        return redirect()->action('Admin\AddressBooksController@index',$getCurrentPage )->with('alert-sucess', trans('admin.ADDRESS_BOOK_CHANGE_STATUS_SUCCESSFULLY'));
    }
}