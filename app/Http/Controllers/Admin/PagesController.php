<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Product;
use App\Coupon;
use App\Cms;
use App\Order;
use App\Helpers\GlobalHelper;
use App\NewsletterSubscriber;
use App\Helpers\BasicFunction;
use Config;
use Input;
class PagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {    

        $users_count = User::where('role_id','=','2')->count();
        //echo $users_count; die;

        $product_count = Product::count();
        
        $coupon_count = Coupon::count();

        $cms_count = Cms::count();
        

        $pageTitle = trans('admin.DASHBOARD');
        $title = trans('admin.DASHBOARD');
        /*         * breadcrumb* */
        $current_month          =   date('m');

        $current_year           =   date('y');
        $current_day           =   date('d');
        $number_of_months       =   12;
        $j                      =   0;
        $result                 =   array();
        $labels                 =   array();


        $input  =   Input::get();
         if(isset($input['type']) && $input['type']!=''){
                  $type =$input['type'];

         }else{
             $type ='day';
         }

       
        $number_of_days         =   daysInMonth($current_year,$current_month);



        $labels[] = ucfirst($type);
        if($type =='day'){


        for ($i=1; $i <= $number_of_days ; $i++) { 
            $display_day                =   date('d M', reportsMakeTime(0, 0, 0,$current_month, $i,$current_year));
            $result[$j][]               =   $display_day;
            foreach (Config::get('global.order_status')as $Odkey => $Odvalue) {
                    $from = date(MYSQL_DATE_FORMATE,reportsMakeTime(0, 0, 0,$current_month, $i,$current_year));
                    $to = date(MYSQL_DATE_FORMATE,reportsMakeTime(23, 59, 59,$current_month, $i,$current_year));
                      $result[$j][]   = Order::where('order_status',$Odvalue)->whereBetween('created_at', array($from, $to))->count();


            }
                        $j++;

         }
           

        }


        if($type =='month'){
                   for ($i=1; $i <= $number_of_months ; $i++) { 
                    $number_of_days         =   daysInMonth($current_year,$i);
            $display_day                =   date('F', reportsMakeTime(0, 0, 0,$i, 1,$current_year));
            $result[$j][]               =   $display_day;
            foreach (Config::get('global.order_status')as $Odkey => $Odvalue) {
                    $from = date(MYSQL_DATE_FORMATE,reportsMakeTime(0, 0, 0,$i, 1,$current_year));
                    $to = date(MYSQL_DATE_FORMATE,reportsMakeTime(23, 59, 59,$i, $number_of_days,$current_year));
                      $result[$j][]   = Order::where('order_status',$Odvalue)->whereBetween('created_at', array($from, $to))->count();


            }
                        $j++;

         }

        }
$cols1 = array();
foreach (Config::get('global.order_status') as $Odkey => $Odvalue) {

             $labels[] = ucfirst($Odvalue);
            

        }
            $cols =$labels;
            $rows = $result;
                

     $cols1 = ['Order', 'Count'];

$rows1 = array();
$k= 0;
            $from = date(MYSQL_DATE_FORMATE,reportsMakeTime(0, 0, 0,$current_month, $current_day,$current_year));
               
                     $to = date(MYSQL_DATE_FORMATE,reportsMakeTime(23, 59, 59,$current_month, $current_day,$current_year));
               foreach (Config::get('global.order_status')as $Odkey => $Odvalue) {
        
                        $rows1[$k][]   =ucfirst($Odvalue);
                      $rows1[$k][]   = Order::where('order_status',$Odvalue)->whereBetween('created_at', array($from, $to))->count();

                      $k++;
            }


        $today_sel = Order::where('payment_status',Config::get('global.payment_status.success'))->whereBetween('created_at', array($from, $to))->sum('payble_amount');
        $total_sel = Order::where('payment_status',Config::get('global.payment_status.success'))->sum('payble_amount');


            $newlatterCount =  NewsletterSubscriber::count();

             $orders_list  = Order::with('user')->sortable(['created_at' => 'desc'])->limit(5)->get();
             $total_order  = Order::count();

             
        return view('admin.pages.index', compact('today_sel','total_sel','type','users_count','product_count','coupon_count','cms_count', 'pageTitle', 'title','rows','cols','rows1','cols1','newlatterCount','orders_list','total_order'));
    }


         /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function  newlatters() {

        $newlatters = NewsletterSubscriber::sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
  

        $pageTitle = trans('admin.NEWSLETTER_SUBSCRIBER');
        $title = trans('admin.NEWSLETTER_SUBSCRIBER');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.NEWSLETTER_SUBSCRIBER'));
        setCurrentPage('admin.newlatters');

        return view('admin.pages.newlatter', compact('newlatters', 'pageTitle', 'title', 'breadcrumb'));
        //

    } 

    function delete_newlatter($id) {

        $grades = NewsletterSubscriber::find($id)->delete();
        return redirect()->action('Admin\PagesController@newlatters')->with('alert-sucess', trans('admin.SUBSCRIBER_DELETED_SUCCESSFULLY'));
    }


}
