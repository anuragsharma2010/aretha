<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SiteImage;
use App\Helpers\BasicFunction;
use Validator;
use Config;
use Input;

class SiteImagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $site_images_list = SiteImage::sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        

        $pageTitle = trans('admin.SITE_IMAGES');
        $title = trans('admin.SITE_IMAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.SITE_IMAGES'));
        setCurrentPage('admin.site_images');

        return view('admin.site_images.index', compact('site_images_list', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.SITE_IMAGES');
        $title = trans('admin.SITE_IMAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.SITE_IMAGES')] = 'admin.site_images.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_SITE_IMAGES'));

        return view('admin.site_images.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $site_imagesObj = new SiteImage();


        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'image' => 'required|image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size'),
             
        ]);
        if ($validator->fails()) {


            return redirect()->action('Admin\SiteImagesController@create')
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
         if ($request->hasFile('image')) {
            $name = BasicFunction::uploadImage(Input::file('image'), SITE_IMAGES_UPLOAD_DIRECTROY_PATH, 'system_images_');
            $input['image'] = $name;
        }

        $site_images = $site_imagesObj->create($input);
        return redirect()->action('Admin\SiteImagesController@index', getCurrentPage('admin.site_images'))->with('alert-sucess', trans('admin.SITE_IMAGES_ADD_SUCCESSFULLY'));
    }
  /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $site_image = SiteImage::find($id);
 
        $image_name =   $site_image->image;

        $site_image->delete();



         @unlink(SITE_IMAGES_UPLOAD_DIRECTROY_PATH . $image_name);
        return redirect()->action('Admin\SiteImagesController@index', getCurrentPage('admin.site_images'))->with('alert-sucess', trans('admin.SITE_IMAGES_DELETED_SUCCESSFULLY'));
    }


}
