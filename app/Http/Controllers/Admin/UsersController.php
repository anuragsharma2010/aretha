<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\User;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
use DB;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

         $users = User::where('role_id', '!=', Config::get('global.role_id.admin'))->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        ;
        //print_r($users); die;
        $pageTitle = trans('admin.USERS');
        $title = trans('admin.USERS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.USERS'));
        setCurrentPage('admin.USERS');
        // print_r($users); die;
        return view('admin.users.index', compact('users', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.ADD_USER');
        $title = trans('admin.ADD_USER');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.USERS')] = 'admin.users.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_USER'));

        return view('admin.users.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
    $input = $request->all();
    $input['phone'] = validPhoneNumber($input['phone']);
    $validator = validator::make($input, [

        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'phone' => 'required|phone|unique:users',
        'password' => 'required|valid_password|same:confirm_password|min:6',
        'confirm_password' => 'required|min:6',
        'gender' => 'required',
    
        ]);
        if ($validator->fails()) {


            return redirect()->action('Admin\UsersController@create')
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
        $input['password'] = Hash::make($request->password);
        $input['role_id'] = Config::get('global.role_id.user');
     
        $input['phone'] = validPhoneNumber($input['phone']);
        $user = User::create($input);
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.USER_ADD_SUCCESSFULLY'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //$user = User::find($id);
    }


    public function view($id) {
        //die('sdfsdf');
        $users = User::find($id);
        $pageTitle = trans('admin.USERS');
        $title = trans('admin.USERS');
        /*         * breadcrumb* */

        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.USERS')] = 'admin.users.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.VIEW_USERS'));
        return view('admin.users.view', compact('users','pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        if ($id == '') {
            return $this->InvalidUrl();
        }
        $user = User::find($id);
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_USER');
        $title = trans('admin.EDIT_USER');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.USERS')] = 'admin.users.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_USER'));
        return view('admin.users.edit', compact('pageTitle', 'title', 'breadcrumb','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        if ($id == '') {
            return $this->InvalidUrl();
        }
        $user = User::findOrFail($id);
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        $input = $request->all();
        $input['phone'] = validPhoneNumber($input['phone']);
        $validator = validator::make($input, [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'phone' => 'required|phone|unique:users,phone,' . $user->id,
                    'gender' => 'required',
        ]);
        if ($validator->fails()) {


            return redirect()->action('Admin\UsersController@edit', $id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $input = $request->all();
                $input['phone'] = validPhoneNumber($input['phone']);
        $user->fill($input)->save();
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.USER_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $user = User::find($id)->delete();
        return redirect()->action('Admin\UsersController@index');
    }

    public function forgotPassword() {
        $pageTitle = trans('admin.CONFIG_MANAGEMENT');
        $title = trans('admin.CONFIG_MANAGEMENT');
        return view('admin.users.forgot_password', compact('pageTitle', 'title'));
    }

    public function sendPasswordLink(Request $request) {

        $validator = validator::make($request->all(), [

                    'email' => 'required|email|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@forgotPassword')
                            ->withErrors($validator)
                            ->withInput();
        }






        $input = $request->all();

        $email = $input['email'];
        $user = User::where('email', '=', $email)->where('role_id', '=', 1)->first();


        if (empty($user->id)) {
            return redirect()->action('Admin\UsersController@forgotPassword')->with('alert-error', 'User Not exist.Please try with another email.');
        }
        $email_token = md5(uniqid(rand(), true));


        $is_update = User::where('id', $user->id)
                ->update(['email_token' => $email_token]);

        if ($is_update) {
            User::where('email', '=', $email)->where('role_id', '=', 1)->first();
        }
        $email_template = EmailTemplate::where('slug', '=', 'admin-forgot-password')->first();
        $email_type = $email_template->email_type;
        $subject = $email_template->subject;
        $body = $email_template->body;
        $to = $user->email;
        $reset_link = URL::route('admin.reset_password', ['email_token' => $email_token]);
        $login_link = ADMIN_URL;
        $body = str_replace(array(
            '{FIRST_NAME}',
            '{LAST_NAME}',
            '{EMAIL}',
            '{LOGIN_LINK}',
            '{PASSWORD_RESET_URL}'
                ), array(
            ucfirst($user->first_name),
            ucfirst($user->last_name),
            ucfirst($user->email),
            $login_link,
            $reset_link,
                ), $body);
        $subject = str_replace(array(
            '{FIRST_NAME}',
            '{LAST_NAME}',
            '{EMAIL}',
            '{LOGIN_LINK}',
            '{PASSWORD_RESET_URL}'
                ), array(
            ucfirst($user->first_name),
            ucfirst($user->last_name),
            ucfirst($user->email),
            $login_link,
            $reset_link,
                ), $subject);


        EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);

        return redirect()->action('Admin\AuthController@getLogin')->with('alert-sucess', 'Password reset link successfully send');
    }

    function resetPassword($email_token = null) {


        if ($email_token == null) {

            return redirect()->action('Admin\AuthController@getLogin');
        }
        $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 1)->first();


        if (empty($user->id)) {
            return redirect()->action('Admin\AuthController@getLogin');
        }
        $pageTitle = trans('admin.RESET_PASSWORD');
        $title = trans('admin.RESET_PASSWORD');
        return view('admin.users.reset_password', compact('pageTitle', 'title', 'email_token'));
    }

    public function resetPasswordUpdate(Request $request, $email_token) {


        $validator = validator::make($request->all(), [
                    'password' => 'required|same:confirm_password|min:6',
                    'confirm_password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@resetPassword')
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
        $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 1)->first();
        $password = Hash::make($request->password);


        $is_update = User::where('id', $user->id)
                ->update(['email_token' => '', 'password' => $password]);



        return redirect()->action('Admin\AuthController@getLogin')->with('alert-sucess', 'Account password changed successfully');
    }

    /**
     * Update logged user (admin) profile 
     *
     */
    public function profile() {
        $admin = adminUser();
        $user = User::find($admin->id);

        $pageTitle = trans('admin.MY_PROFILE');
        $title = trans('admin.MY_PROFILE');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.MY_PROFILE'));
        return view('admin.users.profile', compact('user', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update logged user (admin) profile 
     *
     */
    public function ChangePassword() {



        $pageTitle = trans('admin.CHANGE_PASSWORD');
        $title = trans('admin.CHANGE_PASSWORD');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.MY_PROFILE')] = 'admin.profile';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.CHANGE_PASSWORD'));
        return view('admin.users.change_password', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function UpdateChangePassword(Request $request) {
        # code...
        $admin = adminUser();
        $validator = validator::make($request->all(), [
                    'old_password' => 'required|OldPasswordCheck:' . $admin->password,
                    'new_password' => 'required|ValidPassword|min:6',
                    'confirm_password' => 'required|ValidPassword|min:6',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@ChangePassword')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();

        $password = Hash::make($request->new_password);


        $is_update = User::where('id', $admin->id)
                ->update(['password' => $password]);



        return redirect()->action('Admin\AuthController@getLogout')->with('alert-sucess', trans('admin.PASSWORD_CHANGE_SUCCESS'));
    }

    /**
     * Update user profile
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request) {
        $admin = adminUser();

        $validator = validator::make($request->all(), [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users,email,' . $admin->id,
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@profile')
                            ->withErrors($validator)
                            ->withInput();
        }
        $user = User::findOrFail($admin->id);
        $input = $request->all();
        $user->fill($input)->save();
        return redirect()->action('Admin\PagesController@index')->with('alert-sucess', trans('admin.MY_PROFILE_UPDATE_SUCCESS'));
    }

    /**
     * Function To chnage Status of USer
     *
     * @param  int  $id id of user
     * @param  int  $status 1/0 (current status of cms page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $user = User::where('id', '=', $id)->first();
        $user->status = $new_status;
        $user->save();
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.USER_CHANGE_STATUS_SUCCESSFULLY'));
    }


    public function email_verify($id, $email_verify) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        
        if ($email_verify == 0) {

            $new_emailverify = 1;
        }else{
            $new_emailverify = 0;
        }
        
        $user = User::where('id', '=', $id)->first();
        $user->email_verify = $new_emailverify;

        $user->save();

        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.USER_EMAIL_VERIFIED_SUCCESSFULLY'));

    }

    /*


     */

    function sendCredentials($id) {

        if ($id == '') {
            return $this->InvalidUrl();
        }
        $user = User::findOrFail($id);
        if (empty($user)) {
            return $this->InvalidUrl();
        }
        $password = generateStrongPassword();
        $user->password = Hash::make($password);
        $user->save();

        $email_template = EmailTemplate::where('slug', 
            '=', 'send-login-credentials')->first();
        $email_type = $email_template->email_type;
        $subject = $email_template->subject;
        $body = $email_template->body;

        $to = $user->email;

        $login_link = WEBSITE_URL;
        $body = str_replace(array(
            '{FIRST_NAME}',
            '{LAST_NAME}',
            '{EMAIL_ADDRESS}',
            '{LOGIN_LINK}',
            '{PASSWORD}'
                ), array(
            ucfirst($user->first_name),
            ucfirst($user->last_name),
            $user->email,
            $login_link,
            $password,
                ), $body);


        $subject = str_replace(array(
            '{FIRST_NAME}',
            '{LAST_NAME}',
            '{EMAIL_ADDRESS}',
            '{LOGIN_LINK}',
            '{PASSWORD}'
                ), array(
            ucfirst($user->first_name),
            ucfirst($user->last_name),
            $user->email,
            $login_link,
            $password,
                ), $subject);


        EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.CREDENTIALS_SEND_SUCCESSFULLY'));
    }

}
