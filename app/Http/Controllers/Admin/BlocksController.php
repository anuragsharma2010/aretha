<?php
namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Block;
use App\Helpers\BasicFunction;
use App\CategoryAttribute;
use Validator;
use Config;
use Input;
use view;

class BlocksController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index() {
        $blockslist = Block::with('category')->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));

      //  echo "<pre>";
      //  print_r($blockslist); die;

        $pageTitle = trans('admin.BLOCKS');
        $title = trans('admin.BLOCKS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.BLOCKS'));
        setCurrentPage('admin.blocks');

        return view('admin.blocks.index', compact('blockslist', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        $pageTitle = trans('admin.BLOCKS');
        $title = trans('admin.BLOCKS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.BLOCKS')] = 'admin.blocks.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_BLOCKS'));
 

        return view('admin.blocks.create', compact('pageTitle', 'title', 'breadcrumb'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) {
        $blocks_imagesObj = new Block();

        $validator = validator::make($request->all(), [
                    'position' => 'required|max:255',
                    'title' => 'required|max:255',
                    'description' => 'required|max:255',
                    'image' => 'required|image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size'),
             
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\BlocksController@create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();

       
         if ($request->hasFile('image')) {
            $name = BasicFunction::uploadImage(Input::file('image'), BLOCKS_IMAGES_UPLOAD_DIRECTROY_PATH, 'blocks');
          
            $input['image'] = $name;
        }
        
        $blocks = $blocks_imagesObj->create($input);
        return redirect()->action('Admin\BlocksController@index', getCurrentPage('admin.blocks'))->with('alert-sucess', trans('admin.BLOCKS_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $blocks = Block::find($id);
        if (empty($blocks)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_BLOCKS');
        $title = trans('admin.EDIT_BLOCKS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.BLOCKS')] = 'admin.blocks.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_BLOCKS'));

        return view('admin.blocks.edit', compact('blocks', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id) {

        $validator = validator::make($request->all(), [
                    
                    'title' => 'required|max:255',
                    'description' => 'required|max:255',
                    'image' => 'image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size'),
             
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\BlocksController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $input = $request->all();
        
        $blocks = Block::findOrFail($id);
         //echo "<pre>";
        //print_r($blocks); die;
        if ($request->hasFile('image')) {
            $name = BasicFunction::uploadImage(Input::file('image'), BLOCKS_IMAGES_UPLOAD_DIRECTROY_PATH, 'blocks', true, $blocks->image);
            $input['image'] = $name;
        } else {

            $input['image'] = $blocks->image;
        }
       
        $blocks->fill($input)->save();
        return redirect()->action('Admin\BlocksController@index', getCurrentPage('admin.blocks'))->with('alert-sucess', trans('admin.BLOCKS_UPDATE_SUCCESSFULLY'));
    }

   

}