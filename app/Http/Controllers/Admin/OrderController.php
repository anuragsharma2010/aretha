<?php
namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\Wallet;
use App\OrderDelivery;
use App\OrderStatus;
use App\Helpers\BasicFunction;
use App\Helpers\EmailHelper;
use App\CategoryAttribute;
use Validator;
use Config;
use Input;
use view;
use DateTime;
use PDF;
use Session;
use DB;



class OrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index($status='all') {

        $search =(object) null ;
        $input  =   Input::get();
        $orders_list = Order::with('user');
        if($status!='all'){
            $orders_list = $orders_list->where('order_status',$status);

        } 
        $shipped_by = '';
         if($status==Config::get('global.order_status.inprocess')){

           if(isset($input['shipped_by']) && $input['shipped_by']!=''){

            $shipped_by = $input['shipped_by'];

           }else{ 

             $shipped_by = Config::get('global.shipped_by.delhivery');
           }
            

        }

        if($shipped_by!=''){
          $orders_list = $orders_list->where('shipped_by',$shipped_by );
        }





        if(isset($input['order_id']) && $input['order_id']!=''){
            $order_id = $input['order_id'];
            $orders_list = $orders_list->where('order_id',$order_id);
            $search->order_id = $order_id;
        }    
        if(isset($input['payment_by']) && $input['payment_by']!=''){
            $payment_by = $input['payment_by'];
            $orders_list = $orders_list->where('payment_by',$payment_by);
            $search->payment_by = $payment_by;
        } 
        if(isset($input['payment_status']) && $input['payment_status']!=''){
            $payment_status = $input['payment_status'];
            $orders_list = $orders_list->where('payment_status',$payment_status);
            $search->payment_status = $payment_status;
        }   
        $from = '';
        $to = '';
        if(isset($input['from']) && $input['from']!=''){
            $from =date(MYSQL_DATE_FORMATE,makeTimeDatePicker($input['from']));
            $search->from = $input['from'];
           
        }     
        if(isset($input['to']) && $input['to']!=''){
            $to = date(MYSQL_DATE_FORMATE,makeTimeDatePicker($input['to'],'23:59:59'));
            $search->to = $input['to'];
           
        }    
        if($from!='' && $to!=''){

           $orders_list = $orders_list->whereBetween('created_at', array($from, $to));
        }else if($from!=''){

               $orders_list = $orders_list->where('created_at','>=',$from);
        }
        else if($to!=''){
              $orders_list = $orders_list->where('created_at','<=',$to);
            
        }

        if(isset($input['first_name']) && $input['first_name']!=''){
            $first_name = $input['first_name'];
            $user_ids = User::orWhere('first_name','like','%'.$first_name.'%')->orWhere('last_name','like','%'.$first_name.'%')->lists('id','id')->toArray();
            $orders_list = $orders_list->whereIn('user_id',$user_ids);
            $search->first_name = $first_name;
        }



        $orders_list  = $orders_list->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        
      //  echo "<pre>";
     

        $pageTitle = trans('admin.ORDERS');
        $title = trans('admin.ORDERS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ORDERS'));
        setCurrentPage('admin.ORDERS');

        return view('admin.orders.index', compact('orders_list', 'pageTitle', 'title', 'breadcrumb','status','search','shipped_by'));
    }
    function show($id,$status){
        if (empty($id)) {
            return $this->InvalidUrl();
        }
        $order = \App\Order::with('user','order_detail')->where('id', $id)->first();
        if (empty($order)) {
            return $this->InvalidUrl();
        }
       

        $pageTitle = trans('admin.ORDER').'-'.$order->order_id;
        $title = trans('admin.ORDER').'-'.$order->order_id;
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ORDERS')] = array('admin.orders.index', array('status' => $status));;
        $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
        $order_details= array();
             foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
                    }
                  
                    $order->order_detail = $order_details;
                  
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ORDER').'-'.$order->order_id);
        return view('admin.orders.show', compact('order', 'pageTitle', 'title', 'id', 'status', 'breadcrumb','status'));

    }

   

    function process($id,$status){

        $getCurrentPage['status'] = $status;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.ORDERS');
        $order = \App\Order::with('user','order_detail')->where('id', $id)->first();
         $check_data = \App\Postal::where('pin', '=', $order->pin_code)->first();

         if($order->is_prepaid && strtolower($check_data->pre_paid)=='n'){
           return redirect()->action('Admin\OrderController@index',$getCurrentPage)->with('alert-error', trans('admin.ORDERS_STATUS_INPROCESS_FAILED_BY_PINCODE'));

         }else{
           $token = Configure('CONFIG_DELHIVERY_API'); // replace this with your token key


        if(Configure('CONFIG_DELHIVERY_TEST_MODE')){

              $get_url =          TEST_FETCH_WAYBILL_URL."?token=" . $token . "&count=1";    
        }else{
              $get_url =          PROD_FETCH_WAYBILL_URL."?token=" . $token . "&count=1";    
 
        }
        
        $resultr = file_get_contents($get_url);
        
        $waybill = json_decode($resultr);
        
        if(Configure('CONFIG_DELHIVERY_TEST_MODE')){
            $url = TEST_CREATE_PACKAGE_URL."?token=" . $token;
        }else{

            $url = PROD_CREATE_PACKAGE_URL."?token=" . $token;
        }
        $params  = array(); // this will contain request meta and the package feed
        $package_data = array(); // package data feed
        $shipments = array();
        $pickup_location = array();
/////////////start: building the package feed/////////////////////
        $shipment = array();
        $shipment['waybill'] = $waybill; // waybill number
        $shipment['name'] = ucwords($order->first_name.' '.$order->last_name); // consignee name
        $shipment['order'] = $order->order_id; // client order number
        $shipment['products_desc'] =Configure('CONFIG_SITE_TITLE');
        $shipment['order_date'] =  date_val($order->created_at,DateTime::ISO8601); // ISO Format
        $shipment['payment_mode'] =($order->is_prepaid)? 'Pre­paid':'COD';
        $shipment['total_amount'] = $order->payble_amount; // in INR
        $shipment['cod_amount'] = ($order->is_cod)? $order->payble_amount:'0'; // amount to be collected, required for COD
        $shipment['add'] = $order->address_1.','.$order->address_2; // consignee address
        $shipment['city'] = $order->city;
        $shipment['state'] = $order->state;
        $shipment['country'] = 'India';
        $shipment['phone'] = $order->mobile;
        $shipment['pin'] =  $order->pin_code;
        $shipment['quantity'] = 1; // quanitity of quantity
        // pickup location information //
        $pickup_location['add'] = Configure('CONFIG_PICKUP_ADDRESS');
        $pickup_location['city'] = Configure('CONFIG_PICKUP_CITY');
        $pickup_location['country'] = 'India';
        $pickup_location['name'] =  Configure('CONFIG_DELHIVERY_PICKUP');  // Use client warehouse name
        $pickup_location['phone'] =  Configure('CONFIG_PICKUP_PHONE');
        $pickup_location['pin'] =  Configure('CONFIG_PICKUP_PINCODE');
        $pickup_location['state'] = Configure('CONFIG_PICKUP_STATE');
        $shipments = array($shipment);
        $package_data['shipments'] = $shipments;
        $package_data['pickup_location'] = $pickup_location;
        $params['format'] = 'json';
        $params['data'] = json_encode($package_data);


        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', "Authorization: Token " . $token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  

        $result = curl_exec($ch);
          curl_close($ch);

      

        $data_result = json_decode($result, true);
        
       
        $upload_wbn =  $data_result['upload_wbn'];
        $refnum =  $data_result['packages'][0]['refnum'];
       
        $input['order_id'] = $id;
        $input['waybill'] = $waybill;
        $input['upload_wbn'] =$upload_wbn;
        $input['refnum'] = $refnum;
        $input['manifest_id'] =generateStrongPassword(10, '', 'ud');
         
        $flag= 0;
        if($data_result['success']){
            $input['status'] ='Manifested';
            $flag= 1;

        }else{
                $input['status'] ='Failed';
                $flag= 0;

        }
        
        $input['payment_mode'] =($order->is_prepaid)? 'Pre­paid':'COD';
        $input['StatusType'] ='UD';
        $input['response'] =serialize($data_result);


          $order_delivery =   OrderDelivery::where('order_id', $id)->first();
          if(!empty($order_delivery)){
            $order_delivery->fill($input)->save();
          }else{

             OrderDelivery::create($input);   
          }
         
        if($flag==1){
            $order->order_status =Config::get('global.order_status.inprocess');
            $order->shipped_by =Config::get('global.shipped_by.delhivery');
            $order->awb_number =$waybill;
           
            $order->post_by =Config::get('global.shipped_by.delhivery');



            $order->save();
              $ordData['order_id'] = $order->id;

              $ordData['status'] =  Config::get('global.order_status.inprocess');
              OrderStatus::create($ordData);



        }
      
        if($flag==1){
               return redirect()->action('Admin\OrderController@index',$getCurrentPage)->with('alert-sucess', trans('admin.ORDERS_STATUS_INPROCESS_SUCCESSFULLY'));


        }else{
               return redirect()->action('Admin\OrderController@index',$getCurrentPage)->with('alert-error', trans('admin.ORDERS_STATUS_INPROCESS_FAILED'));

        }
      }
     
    }

    function packageSlipPrint($order_id){
       
        $data =   OrderDelivery::where('order_id', $order_id)->first();
        if($data->packages_slip_data==''){
              $waybill  =  $data->waybill;
        
        
       if(Configure('CONFIG_DELHIVERY_TEST_MODE')){
             $url = TEST_PACKING_SLIP_URL."?wbns=".$waybill;
        }else{
             $url = PROD_PACKING_SLIP_URL."?wbns=".$waybill;

        }
        $token =  Configure('CONFIG_DELHIVERY_API'); 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', "Authorization: Token " . $token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  

        $result = curl_exec($ch);
          curl_close($ch);
           $data_result = json_decode($result, true);
         if($data_result['packages_found']==1){

             $input['packages_slip_data'] = serialize($data_result['packages'][0]);
             $data->fill($input)->save();  


         }else{

                  return redirect()->action('Admin\OrderController@index',getCurrentPage('admin.ORDERS'))->with('alert-error', trans('admin.ORDERS_PACKAGE_NOT_FOUND_FAILED'));
         }

        }
       // pr(unserialize($data->packages_slip_data));
        $packages_slip_data = unserialize($data->packages_slip_data);

         $order = \App\Order::with('user','order_detail')->where('id', $order_id)->first();

             foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                       
                        $order_details[$orderKey]['product'] = \App\Product::where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
                    }
                  
                    $order->order_detail = $order_details;
          return view('admin.orders.package_slip_print', compact('data','packages_slip_data','order'));
      
    }   

     function packageSlipDownload($order_id){
       
        $data =   OrderDelivery::where('order_id', $order_id)->first();
        if($data->packages_slip_data==''){
              $waybill  =  $data->waybill;
        
        
       if(Configure('CONFIG_DELHIVERY_TEST_MODE')){
             $url = TEST_PACKING_SLIP_URL."?wbns=".$waybill;
        }else{
             $url = PROD_PACKING_SLIP_URL."?wbns=".$waybill;

        }
        $token =  Configure('CONFIG_DELHIVERY_API'); 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', "Authorization: Token " . $token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  

        $result = curl_exec($ch);
          curl_close($ch);
           $data_result = json_decode($result, true);
         if($data_result['packages_found']==1){

             $input['packages_slip_data'] = serialize($data_result['packages'][0]);
             $data->fill($input)->save();  


         }else{

                  return redirect()->action('Admin\OrderController@index',getCurrentPage('admin.ORDERS'))->with('alert-error', trans('admin.ORDERS_PACKAGE_NOT_FOUND_FAILED'));
         }

        }
       
        $packages_slip_data = unserialize($data->packages_slip_data);

         $order = \App\Order::with('user','order_detail')->where('id', $order_id)->first();

             foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                       
                        $order_details[$orderKey]['product'] = \App\Product::where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
                    }
                  
                    $order->order_detail = $order_details;
                    

                    $html =  view('admin.orders.package_slip_pdf', compact('data','packages_slip_data','order'))->render();
                
                   $html =  mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
                   //echo $html;die;
                  
                   $pdf = PDF::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save(SLIP_UPLOAD_DIRECTROY_PATH.$order->order_id.'.pdf');
                
             return $pdf->download($order->order_id.'.pdf');
       
      
    }

 

  function orderStatus($order_id){

      $order_delivery =   OrderDelivery::where('order_id', $order_id)->first();
      $waybill  =  $order_delivery->waybill;
      $token = Configure('CONFIG_DELHIVERY_API'); 

      if(Configure('CONFIG_DELHIVERY_TEST_MODE')){
        $get_url = TEST_PACKAGES_URL."/?token=" . $token . "&waybill=".$waybill;
      }else{
        $get_url = PROD_PACKAGES_URL."/?token=" . $token . "&waybill=".$waybill;

      }
       
        
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $get_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Token " . $token));
      $result = curl_exec($ch);
      curl_close($ch);
      $data_result = json_decode($result, true);
      $track_responce = array();
      if(isset($data_result['ShipmentData'][0]['Shipment'])){

        $track_responce = $data_result['ShipmentData'][0]['Shipment'];  
      }
      
      
      if(!empty($track_responce)){

          $input['track_responce'] = serialize($track_responce);
          $input['status'] = $track_responce['Status']['Status'];
          $input['StatusType'] = $track_responce['Status']['StatusType'];
          $input['last_track_time'] = getCurrentTime();
          $order_delivery->fill($input)->save();  
      }
  
      return true;
  }

     /**
     * Function To chnage Status of tags pages
     *
     * @param  int  $id id of tags pages
     * @param  int  $status 1/0 (current status of tags page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function shipped($id, $status) {
   

        if (empty($id)) {
            return $this->InvalidUrl();
        }
   
        $Orders = Order::with('user')->where('id', '=', $id)->first();
        $orderObj = $Orders;
        $Orders->order_status = Config::get('global.order_status.shipped');
        $Orders->save();
          $this->sendShippedMail($id);
             $ordData['order_id'] = $id;
              $ordData['status'] =  Config::get('global.order_status.shipped');
              OrderStatus::create($ordData);
        if(Configure('CONFIG_ENABLE_SMS')){

              EmailHelper::sendOrderSms($Orders->user->phone,$Orders->user->id,$Orders->order_id,2,$Orders); 
          }


         if($orderObj->is_cash_back==1 && $orderObj->payment_by!='cod'){


             $wallet = array();
                    $wallet['user_id'] = $Orders->user_id;
                    $wallet['flag'] =Config::get('global.wallet_flag.cr');
                    $wallet['cash'] =$Orders->cash_back_amount;
                    
                    $wallet['description'] ="Cashback recieved from Order #".$Orders->order_id.'(Coupon Code :- '.$Orders->coupon_code.')';
                    $wallet['order_id'] =$Orders->id;
                    Wallet::create($wallet);
                    $this->sendCashbackMail($orderObj);
                  $orderObj->is_cash_back_transfer = 1;
                  $orderObj->save();
                if(Configure('CONFIG_ENABLE_SMS')){

                  EmailHelper::sendOrderSms($Orders->user->phone,$Orders->user->id,$Orders->order_id,5,$Orders); 
                }

        }  
                 
        
        $getCurrentPage['status'] = $status;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.ORDERS');
        return redirect()->action('Admin\OrderController@index',$getCurrentPage)->with('alert-sucess', trans('admin.ORDERS_SHIPPED_SUCCESSFULLY'));
    }    

    public function delivered($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
   
   
        $Orders = Order::with('user')->where('id', '=', $id)->first();
        $orderdata = $Orders;
        $Orders->order_status =Config::get('global.order_status.delivered');
        if($Orders->payment_by=='cod'){
            $Orders->payment_status = Config::get('global.payment_status.success');
        }
        $Orders->delivery_date =getCurrentTime();
        $Orders->save();


           if(Configure('CONFIG_ENABLE_SMS')){

                EmailHelper::sendOrderSms($orderdata->user->phone,$orderdata->user->id,$orderdata->order_id,6); 
            } 


        $this->sendDeliverMail($id);
             $ordData['order_id'] = $id;
              $ordData['status'] =  Config::get('global.order_status.delivered');
              OrderStatus::create($ordData);



        $getCurrentPage['status'] = $status;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.ORDERS');
        return redirect()->action('Admin\OrderController@index',$getCurrentPage)->with('alert-sucess', trans('admin.ORDERS_DELIVERED_SUCCESSFULLY'));
    }

    public function shippedByPostal($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
   
        $Orders = Order::with('user')->where('id', '=', $id)->first();
        $Orders->order_status =Config::get('global.order_status.inprocess');
        $Orders->shipped_by = Config::get('global.shipped_by.postal');
        $Orders->save();
     
              $ordData   = array();
              $ordData['order_id'] = $id;
              $ordData['status'] =  Config::get('global.order_status.inprocess');
              OrderStatus::create($ordData);
        $getCurrentPage['status'] = $status;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.ORDERS');
        return redirect()->action('Admin\OrderController@index',$getCurrentPage)->with('alert-sucess', trans('admin.ORDERS_PROCESS_VIA_POSTAL_SUCCESSFULLY'));
    }    

  public function saveShippedByPostal($id, $status,Request $request) {
    $input = $request->all();


        $validator = Validator::make($request->all(), [
                    'awb_number' => 'required',
                    'post_by' => 'required',
                        ]
        );

     if ($validator->errors()->all()) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['errors'] = $validator->errors();
            $data['message'] = "Please Enter Valid Input";
        } else {

   
        $Orders = Order::with('user')->where('id', '=', $id)->first();
        $orderObj = $Orders;
        $Orders->order_status =Config::get('global.order_status.shipped');
        $Orders->shipped_by = Config::get('global.shipped_by.postal');
        $Orders->awb_number = $input['awb_number'];
        $Orders->post_by = $input['post_by'];
        $Orders->save();
       
       $ordData   = array();
              $ordData['order_id'] = $id;
              $ordData['status'] =  Config::get('global.order_status.shipped');
              OrderStatus::create($ordData);
        $this->sendShippedMail($id);



      if($orderObj->is_cash_back==1 && $orderObj->payment_by!='cod'){


             $wallet = array();
                    $wallet['user_id'] = $Orders->user_id;
                    $wallet['flag'] =Config::get('global.wallet_flag.cr');
                    $wallet['cash'] =$Orders->cash_back_amount;
                    
                    $wallet['description'] ="Cashback recieved from Order #".$Orders->order_id.'(Coupon Code :- '.$Orders->coupon_code.')';
                    $wallet['order_id'] =$Orders->id;
                    Wallet::create($wallet);
                    $this->sendCashbackMail($orderObj);
                  $orderObj->is_cash_back_transfer = 1;
                  $orderObj->save();


        }  

            if(Configure('CONFIG_ENABLE_SMS')){

              EmailHelper::sendOrderSms($Orders->user->phone,$Orders->user->id,$Orders->order_id,2,$Orders); 
            }


        $data['status_code'] = 1;
        $data['status_text'] = 'Success';
        Session::flash('alert-sucess',trans('admin.ORDERS_SHIPPED_SUCCESSFULLY'));
       
      }
        return response()->json($data);
    }

        /**
     * Function To chnage Status of tags pages
     *
     * @param  int  $id id of tags pages
     * @param  int  $status 1/0 (current status of tags page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function reject($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        
   
        $Orders = Order::where('id', '=', $id)->first();
        $Orders->order_status =Config::get('global.order_status.reject');
        $Orders->reject_reason ='Admin reject order';
        $Orders->save();

       
              $ordData['order_id'] = $id;
              $ordData['status'] =  Config::get('global.order_status.reject');
              OrderStatus::create($ordData);

               if($Orders->payment_by != Config::get('global.payment_by.cod') && $Orders->payment_status==Config::get('global.payment_status.success')) {

                    $wallet = array();
                    $wallet['user_id'] = $Orders->user_id;
                    $wallet['flag'] =Config::get('global.wallet_flag.cr');
                    $wallet['cash'] =$Orders->payble_amount;
                    
                    $wallet['description'] ="Admin reject order Refund from Order #".$Orders->order_id;
                    $wallet['order_id'] =$Orders->id;

                    Wallet::create($wallet);
                       $Orders->is_refund =1;
                       $Orders->save();


                }

        $getCurrentPage['status'] = $status;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.ORDERS');
        return redirect()->action('Admin\OrderController@index',$getCurrentPage)->with('alert-sucess', trans('admin.ORDERS_REJECT_SUCCESSFULLY'));
    }


    function orderTrack($status='All'){

     
       

          $orders_list  = OrderDelivery::with('order')->sortable(['updated_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));

         
        
      //  echo "<pre>";
     

        $pageTitle = trans('admin.ORDERS_TRACK');
        $title = trans('admin.ORDERS_TRACK');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ORDERS_TRACK'));
        setCurrentPage('admin.ORDERS_TRACK');

        return view('admin.orders.orders_track', compact('orders_list', 'pageTitle', 'title', 'breadcrumb','status'));
    }
    function Track($id){


    if (empty($id)) {
            return $this->InvalidUrl();
        }


         $order_delivery  = OrderDelivery::where('id', $id)->first();
        $order = \App\Order::with('user')->where('id', $order_delivery->order_id)->first();
        if (empty($order)) {
            return $this->InvalidUrl();
        }
        if($order_delivery->last_track_time < getCurrentTime()-1800){
          $this->orderStatus($order_delivery->order_id);
           $order_delivery  = OrderDelivery::where('id', $id)->first();

        }
        
       
        $pageTitle = trans('admin.ORDER').'-'.$order->order_id;
        $title = trans('admin.ORDER').'-'.$order->order_id;
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ORDERS_TRACK')] = 'admin.orders.track_orders';
        
                  
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.TRACK').'-'.$order->order_id);
        return view('admin.orders.track', compact('order', 'pageTitle', 'title', 'id', 'order_delivery', 'breadcrumb'));

    }

    function sendShippedMail($id){

         $order = \App\Order::with('user','order_detail')->where('id', $id)->first();
           $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
                   $order_details= array();
            foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
            $order->order_detail = $order_details;
            
            $subject= 'Order Shipped - Your order #'.$order->order_id.' has been shipped';
            $email_type= 'Order Shipped';

            EmailHelper::sendOrderMail($order->user->email, '',  $subject, 'shipped_order', $order->toArray(), $email_type);
         
        return true;
    }
        function sendDeliverMail($id){

         $order = \App\Order::with('user','order_detail')->where('id', $id)->first();
           $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
                   $order_details= array();
            foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
            $order->order_detail = $order_details;
            
            $subject= 'Order Shipped - Your order #'.$order->order_id.' has been delivered';
            $email_type= 'Order Delivered';

            EmailHelper::sendOrderMail($order->user->email, '',  $subject, 'delivered_order', $order->toArray(), $email_type);
         
        return true;
    }
   function sendCashbackMail($orderdata){

            $email_template = \App\EmailTemplate::where('slug', '=', 'cashback-received')->first();
            $email_type = $email_template->email_type;
            $subject = $email_template->subject;
            $body = $email_template->body;
            $user = $orderdata->user;
            $to = $user->email;
           
            $balance = $this->getWallateBalance($user->id);
            $body = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
                '{COUPON_CODE}',
                '{AMOUNT}',
                '{BALANCE}'
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $orderdata->order_id,
                $orderdata->coupon_code,
                display_price($orderdata->cash_back_amount),
                display_price($balance)
                
          
                    ), $body);    

                          $subject = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
                '{COUPON_CODE}',
                '{AMOUNT}',
                '{BALANCE}'
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $orderdata->order_id,
                $orderdata->coupon_code,
                display_price($orderdata->cash_back_amount),
                display_price($balance),
          
                    ), $subject);

    

             EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);

    }
     function getWallateBalance($user_id) {

        $wallet = DB::table('wallet')
                ->select(DB::raw('SUM(cash) as cash'), 'flag')
                ->where('user_id', '=', $user_id)
                ->groupBy('flag')
                ->get();
        $data = array();


        foreach ($wallet as $key => $value) {
            $data[$value->flag] = $value->cash;
        }

        $cr = 0;
        $dr = 0;
        if (isset($data['cr'])) {

            $cr = $data['cr'];
        }
        if (isset($data['dr'])) {

            $dr = $data['dr'];
        }

        return $cr - $dr;
    }


    function downloadManifest(Request $request){
      set_time_limit(0);
      

          $input = $request->all();
           $order_id = $input['order_ids'];
           
              $orders = \App\Order::with('user','order_detail','order_delivery')->whereIn('id', $order_id)->get()->toArray();
                $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
        $order_details= array();
              foreach ($orders as $key => $order) {
                # code...
              
                $order_details = array();
             foreach ($order['order_detail']  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                       
                        $order_details[$orderKey]['product'] = \App\Product::where('id', '=', $product_id)->select('id', 'slug', 'title')->first()->toArray();
                         $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                       
                        
                    }
                  
                    $orders[$key]['order_detail'] = $order_details;
                    
            }
            

               $html =  view('admin.orders.manifest_pdf', compact('orders'))->render();
              
                
              
              
                  $file_name = 'manifest'.date('d_m_Y'). '_' . rand(0, 999999999);
                   $pdf = PDF::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save(SLIP_UPLOAD_DIRECTROY_PATH.$file_name.'.pdf');
           return $pdf->download($file_name.'.pdf');

    }
    function invoice($id,$status){


       if (empty($id)) {
            return $this->InvalidUrl();
        }
       
         $order = \App\Order::with('user','order_detail')->where('id', $id)->first();

   if (empty($order)) {
            return $this->InvalidUrl();
        }
     
        $orderObj = $order;

        $pageTitle = trans('admin.ORDER').'-'.$order->order_id;
        $title = trans('admin.ORDER').'-'.$order->order_id;
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ORDERS')] = array('admin.orders.index', array('status' => $status));;
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.INVOICE'));

           $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
                   $order_details= array();
            foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
           $order->order_detail = $order_details;

            if(empty($order->invoice_id)){
               
                
                
               

                $OrderObj = Order::where('id', '=', $id)->first();
                $OrderObj->invoice_id = INVOICE_START+$order->id;
                $order->invoice_id = INVOICE_START+$order->id;
                $OrderObj->save();

               
            }
                
              

             return view('admin.orders.invoice', compact('order', 'pageTitle', 'title', 'id', 'status', 'breadcrumb'));

    }   

    function invoicePrint($id){


       if (empty($id)) {
            return $this->InvalidUrl();
        }
       
         $order = \App\Order::with('user','order_detail')->where('id', $id)->first();

   if (empty($order)) {
            return $this->InvalidUrl();
        }
     
        $orderObj = $order;

           $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
                   $order_details= array();
            foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
           $order->order_detail = $order_details;

            if(empty($order->invoice_id)){
               
                $OrderObj = Order::where('id', '=', $id)->first();
                $OrderObj->invoice_id = INVOICE_START+$order->id;
                $order->invoice_id = INVOICE_START+$order->id;
                $OrderObj->save();

               
            }
                
            

             return view('admin.orders.invoice_print', compact('order','id'));

    }
    function invoiceSend($id,$status=''){


       if (empty($id)) {
            return $this->InvalidUrl();
        }
       
         $order = \App\Order::with('user','order_detail')->where('id', $id)->first();

   if (empty($order)) {
            return $this->InvalidUrl();
        }
     
        $orderObj = $order;

           $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
                   $order_details= array();
            foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
           $order->order_detail = $order_details;

            if(empty($order->invoice_id)){
               
                
                
                

                $OrderObj = Order::where('id', '=', $id)->first();
                $OrderObj->invoice_id = INVOICE_START+$order->id;
                $order->invoice_id = INVOICE_START+$order->id;
                $OrderObj->save();

               
            }
                
                    $html =  view('admin.orders.invoice_pdf', compact('id','order'))->render();
                
                  $html =  mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
                    
                   $file_name = 'invoice-'.$order->order_id;
                  
                   $pdf = PDF::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save(INVOICE_UPLOAD_DIRECTROY_PATH.$file_name.'.pdf');
                
              
                    $email_template = \App\EmailTemplate::where('slug', '=', 'invoice')->first();
            $email_type = $email_template->email_type;
            $subject = $email_template->subject;
            $body = $email_template->body;
            $user = $order->user;
            $to = $user->email;
           
            
            $body = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
                
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $order->order_id,
           
                
          
                    ), $body);    
   $subject = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
                
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $order->order_id,
           
                
          
                    ), $subject);    

   
    

             EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type,INVOICE_UPLOAD_DIRECTROY_PATH.$file_name.'.pdf');

            
                $OrderObj = Order::where('id', '=', $id)->first();
                $OrderObj->is_send_invoice = 1;
                $OrderObj->save();
                 $getCurrentPage['status'] = $status;
                 $getCurrentPage['id'] = $id;
             return redirect()->action('Admin\OrderController@invoice',$getCurrentPage)->with('alert-sucess', trans('admin.INVOICE_SEND_SUCCESSFULLY'));

    }




}