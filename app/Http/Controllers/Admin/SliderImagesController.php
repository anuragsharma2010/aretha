<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SliderImage;
use App\CategoryAttribute;
use App\Helpers\BasicFunction;
use Validator;
use Config;
use Input;

class SliderImagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $slider_imageslist = SliderImage::with('category')->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        

        $pageTitle = trans('admin.SLIDER_IMAGES');
        $title = trans('admin.SLIDER_IMAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.SLIDER_IMAGES'));
        setCurrentPage('admin.slider_images');

        return view('admin.slider_images.index', compact('slider_imageslist', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.ADD_SLIDER_IMAGES');
        $title = trans('admin.ADD_SLIDER_IMAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.SLIDER_IMAGES')] = 'admin.slider_images.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_SLIDER_IMAGES'));

        return view('admin.slider_images.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $slider_imagesObj = new SliderImage();


        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'description' => 'required|max:255',
                    'image' => 'required|image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size'),
             
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\SliderImagesController@create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();
        
         if ($request->hasFile('image')) {
            $name = BasicFunction::uploadImage(Input::file('image'), SLIDER_IMAGES_UPLOAD_DIRECTROY_PATH, 'slider_');
            $input['image'] = $name;
        }
        

        $slider_images = $slider_imagesObj->create($input);
        return redirect()->action('Admin\SliderImagesController@index', getCurrentPage('admin.slider_images'))->with('alert-sucess', trans('admin.SLIDER_IMAGES_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $slider_images = SliderImage::find($id);
        if (empty($slider_images)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_SLIDER_IMAGES');
        $title = trans('admin.EDIT_SLIDER_IMAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.SLIDER_IMAGES')] = 'admin.slider_images.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_SLIDER_IMAGES'));

        return view('admin.slider_images.edit', compact('slider_images', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'description' => 'required|max:255',
                    'image' => 'image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size'),
             
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\SliderImagesController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $input = $request->all();
        $slider_images = SliderImage::findOrFail($id);
        if ($request->hasFile('image')) {
            $name = BasicFunction::uploadImage(Input::file('image'), SLIDER_IMAGES_UPLOAD_DIRECTROY_PATH, 'slider_', true, $slider_images->image);
            $input['image'] = $name;
        } else {

            $input['image'] = $slider_images->image;
        }


       
        $slider_images->fill($input)->save();
        return redirect()->action('Admin\SliderImagesController@index', getCurrentPage('admin.slider_images'))->with('alert-sucess', trans('admin.SLIDER_IMAGES_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Function To chnage Status of slider_images pages
     *
     * @param  int  $id id of slider_images pages
     * @param  int  $status 1/0 (current status of slider_images page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {
       

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $slider_images = SliderImage::where('id', '=', $id)->first();
        $slider_images->status = $new_status;
        $slider_images->save();
        return redirect()->action('Admin\SliderImagesController@index', getCurrentPage('admin.slider_images'))->with('alert-sucess', trans('admin.SLIDER_IMAGES_CHANGE_STATUS_SUCCESSFULLY'));
    }

}
