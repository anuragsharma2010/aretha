<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Support\Facades\Auth;
use File;
use Validator;

class SettingsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $setting = Setting::find(1);
        $pageTitle = trans('admin.CONFIG_MANAGEMENT');
        $title = trans('admin.CONFIG_MANAGEMENT');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.CONFIG_MANAGEMENT'));

        return view('admin.settings.index', compact('setting', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


$validatorArry = array(  'site_title' => 'required|max:255',
                    'page_limit' => 'required|numeric',
                    'front_page_limit' => 'required|numeric',
                    'fromemail' => 'required|email|max:255',
                    'reply_to_email' => 'required|email|max:255',
                    'staff_mail' => 'required|email|max:255',
                    'from_name' => 'required|max:255',
                    'copyright' => 'required|max:255',
                    'meta_description' => 'required',
                    'meta_title' => 'required',
                    'meta_keywords' => 'required',
                    'email_signature' => 'required',
                    'facebook_client_id' => 'required',
                    'facebook_client_secret' => 'required',
                    'facebook_link' => 'required|url',
                    'twitter_link' => 'required|url',
                    'google_plus_link' => 'required|url',
                    'pinterest_link' => 'required|url',
                    'android_app_link' => 'required|url',
                    'ios_app_link' => 'required|url',
                    'phone_number' => 'required',
                    'address' => 'required',
                    'support_mail' => 'required|email',
                    'google_map' => 'required',
                    'related_product_limit' => 'required|numeric',
                    'return_and_delivery' => 'required',
                    'mail_port' => 'required|numeric',
                    'mail_host' => 'required',
                    'mail_username' => 'required',
                    'mail_password' => 'required',
                    'mail_driver' => 'required',
                    'sms_user_name' => 'required',
                    'sms_password' => 'required',
                    'sender_id' => 'required',
                    'google_client_id' => 'required',
                    'cod_charge' => 'required|numeric',
                    'cod_description' => 'required',
                    'mid' => 'required',
                    'website' => 'required',
                    'merchant_key' => 'required',
                    'channel_id' => 'required',
                    'industry_type_id' => 'required',
                    'merchant_id' => 'required',
                    'salt' => 'required',
                    'coupon_code'=>'required',
                    'coupon_description'=>'required',
                    'min_bill_amount'=>'required|numeric',
                    'max_cashback_amount'=>'required|numeric',
                    'min_total_amount_for_cod'=>'required|numeric',
                    'min_total_amount_for_shipping'=>'required|numeric',
                    'shipping_charges'=>'required|numeric',
                    'cashback_type'=>'required',
                    'delhivery_client'=>'required',
                    'delhivery_api'=>'required',
                    'delhivery_pickup'=>'required',
                    'pickup_address'=>'required',
                    'pickup_city'=>'required',
                    'pickup_phone'=>'required',
                    'pickup_pincode'=>'required',
                    'pickup_state'=>'required',
                    'payu_description'=>'required',
                    'payumoney_description'=>'required',
                    'paytm_description'=>'required',
                    );

        $input = $request->all();
        if(!empty($input['cashback_type'])) {
                    if($input['cashback_type']=='fixed'){

                    $validatorArry['amount'] = 'required|numeric';

                }else{

                $validatorArry['percentage'] = 'required|numeric';

                }
            }
            

        $validator = validator::make($request->all(), $validatorArry);
        if ($validator->fails()) {
            
            return redirect()->action('Admin\SettingsController@index')
                            ->withErrors($validator)
                            ->withInput()->with('alert-error', "Required field can't empty");
        }



        $setting = Setting::findOrFail($id);
        $input = $request->all();

        if(!isset($input['enable_sms'])){

            $input['enable_sms']  =   0;

        }     
        if(!isset($input['enable_cashback'])){

            $input['enable_cashback']  =   0;

        }     
        if(!isset($input['enable_paytm'])){

            $input['enable_paytm']  =   0;

        } 
        if(!isset($input['paytm_test_mode'])){

            $input['paytm_test_mode']  =   0;

        }
        if(!isset($input['payu_test_mode'])){

            $input['payu_test_mode']  =   0;

        }   
           if(!isset($input['enable_payu'])){

            $input['enable_payu']  =   0;

        }  

           if(!isset($input['enable_cod'])){

            $input['enable_cod']  =   0;

        }
     if(!isset($input['delhivery_test_mode'])){

            $input['delhivery_test_mode']  =   0;

        }






        $setting->fill($input)->save();
        $setting = Setting::find(1)->toArray();
        $filename = 'f' . gmdate('YmdHis');
        $path = base_path() . '/config/';
        File::put($path . $filename, '<?php ' . "\n");
        File::append($path . $filename, 'return [ ' . "\n");

        foreach ($setting as $key => $value) {
            $constant = "CONFIG_" . strtoupper($key);
            File:: append($path . $filename, '"' . $constant . '"   =>   "' . addslashes($value) . '",' . "\n");
        }
        File:: append($path . $filename, ' ];');
        @rename($path . $filename, $path . 'settings.php');





        return redirect()->action('Admin\SettingsController@index')->with('alert-sucess', 'Site configuration save successfully.');
        ;
    }

}
