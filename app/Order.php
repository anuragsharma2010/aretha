<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Order extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];

      public function order_detail()
        {
            return $this->hasMany('App\OrderDetail', 'order_id');
        }


     /*
      * get parent category  data
      *
      * */
     public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }     /*
      * get parent category  data
      *
      * */
     public function order_delivery()
    {
        return $this->hasOne('App\OrderDelivery', 'order_id');

    }
    
}
