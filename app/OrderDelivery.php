<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class OrderDelivery extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_delivery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];

       /*
      * get parent category  data
      *
      * */
     public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }


    
}
